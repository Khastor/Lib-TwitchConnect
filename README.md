# Network

Network services and utilities for C++ clients.

## Dependencies

* [Boost](https://www.boost.org/)
* [OpenSSL](https://www.openssl.org/)

## Build with CMake

```
$ mkdir build
$ cd build
$ cmake ..
$ cmake --build .
```

## Features

### Twitch OAuth2 Service

Connect a single user account to Twitch to start using Twitch IRC and Twitch API services:
* Get a Twitch OAuth2 app access token using the client credentials grant flow
* Get a Twitch OAuth2 user access token using the authorization code grant flow

Twitch OAuth2 access tokens are automatically validated and refreshed:
* On Twitch OAuth2 service startup
* On Twitch OAuth2 service hourly heartbeat
* On Twitch OAuth2 access token expiry date
* Upon reception of a Validate Access Tokens command

### Twitch IRC Service

Connect a single user account to a Twitch IRC channel:
* Receive messages from the Twitch IRC channel
* Send messages to the Twitch IRC channel

### Twitch API Service

Send requests to the Twitch API and receive asynchronous responses.

## Resources

* [Twitch Authentication](https://dev.twitch.tv/docs/authentication/)
* [Twitch IRC](https://dev.twitch.tv/docs/irc/)
* [Twitch API](https://dev.twitch.tv/docs/api/)
* [Twitch EventSub](https://dev.twitch.tv/docs/eventsub/)