#ifndef NETWORK_CORE_DISPATCHER_HPP_INCLUDED
#define NETWORK_CORE_DISPATCHER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher Interface                                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>
#include <Network/Core/Observer.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <condition_variable>
#include <cstdint>
#include <functional>
#include <memory>
#include <mutex>
#include <optional>
#include <thread>
#include <utility>
#include <vector>

namespace Network::Core
{

/**
 * @brief Message dispatcher.
 *
 * Dispatch messages asynchronously to the subscribed message handlers.
 * The message callbacks of the subscribed message handlers are all invoked from the same thread.
 */
class Dispatcher
{
private:
    /**
     * @brief Constructor.
     */
    Dispatcher();

public:
    /**
     * @brief Subscription to the message dispatcher.
     *
     * Hold a message handler's subscription to the message dispatcher.
     * The subscription must be revoked before the message handler is destroyed.
     */
    class Subscription
    {
        friend class Dispatcher;

    private:
        /**
         * @brief Constructor.
         *
         * Create a subscription, which must be revoked before the destruction of the message handler.
         * It is forbidden to create or revoke a subscription from a message callback invoked from the message dispatcher thread.
         *
         * @param pIdentifier The unique subscription identifier.
         */
        Subscription(
            std::uint64_t pIdentifier);

    public:
        /**
         * @brief Destructor.
         *
         * The subscription is revoked upon destruction.
         * The corresponding message handler will stop receiving messages from this subscription.
         * The subscription must be revoked before the destruction of the message handler.
         * It is forbidden to create or revoke a subscription from a message callback invoked from the message dispatcher thread.
         */
        ~Subscription();

        /**
         * @brief Deleted copy-constructor.
         */
        Subscription(const Subscription&) = delete;

        /**
         * @brief Move-constructor.
         */
        Subscription(Subscription&&);

        /**
         * @brief Deleted copy-assignment operator.
         */
        Subscription& operator=(const Subscription&) = delete;

        /**
         * @brief Move-assignment operator.
         */
        Subscription& operator=(Subscription&&);

        /**
         * @brief Revoke the subscription.
         *
         * The corresponding message handler will stop receiving messages from this subscription.
         * The subscription must be revoked before the destruction of the message handler.
         * It is forbidden to create or revoke a subscription from a message callback invoked from the message dispatcher thread.
         */
        void Revoke();

    private:
        /**
         * @brief The unique subscription identifier, or empty if the subscription has been revoked.
         */
        std::optional<std::uint64_t> mIdentifier;
    };

    /**
     * @brief Deleted copy-constructor.
     */
    Dispatcher(const Dispatcher&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Dispatcher(Dispatcher&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Dispatcher& operator==(const Dispatcher&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Dispatcher& operator==(Dispatcher&&) = delete;

    /**
     * @brief Get the unique message dispatcher instance.
     *
     * @return The unique message dispatcher instance.
     */
    static Dispatcher& Instance();

    /**
     * @brief Publish a message.
     *
     * The message will be dispatched asynchronously to all subscribed message handlers.
     *
     * @param pMessage The message to publish.
     */
    void Publish(
        std::unique_ptr<Message> pMessage);

    /**
     * @brief Subscribe a message observer to the message dispatcher.
     *
     * The message callback of the message observer will be invoked asynchronously from the message dispatcher thread when new messages are published.
     * It is forbidden to subscribe or revoke a subscription to the message dispatcher from a message callback invoked from the message dispatcher thread.
     *
     * @tparam tMessageT The type of messages that the message observer subscribes to.
     * @param pObserver The message observer to subscribe to the message dispatcher, will start receiving new messages of type tMessageT and derived.
     * @return The message handler subscription, which must be revoked before the destruction of the message observer.
     */
    template<MessageT tMessageT>
    Subscription Subscribe(
        Observer<tMessageT>* pObserver);

    /**
     * @brief Subscribe a message callback to the message dispatcher.
     *
     * The message callback will be invoked asynchronously from the message dispatcher thread when new messages are published.
     * It is forbidden to subscribe or revoke a subscription to the message dispatcher from a message callback invoked from the message dispatcher thread.
     *
     * @tparam tMessageT The type of messages that the message callback subscribes to.
     * @param pCallback The message callback to subscribe to the message dispatcher, will start receiving new messages of type tMessageT and derived.
     * @return The message handler subscription, which must be revoked before the destruction of the message callback resources.
     */
    template<MessageT tMessageT>
    Subscription Subscribe(
        std::function<void(const tMessageT&)> pCallback);

    /**
     * @brief Revoke a message handler's subscription to the message dispatcher.
     *
     * It is forbidden to subscribe or revoke a subscription to the message dispatcher from a message callback invoked from the message dispatcher thread.
     *
     * @param pSubscription The subscription to revoke, its corresponding message handler will stop receiving messages from this subscription.
     */
    void Revoke(
        Subscription& pSubscription);

private:
    /**
     * @brief Worker thread main loop.
     *
     * Dispatch the buffered messages to the registered message observers.
     * The main loop is started upon construction, and stopped upon destruction.
     *
     * @param pStopToken Signal sent upon destruction to request the main loop to stop.
     */
    void Loop(
        std::stop_token pStopToken);

    /**
     * @brief Message handler interface.
     */
    struct MessageHandlerConcept
    {
        /**
         * @brief Constructor.
         *
         * @param pSubscriptionId The unique identifier of the subscription.
         */
        MessageHandlerConcept(
            std::uint64_t pSubscriptionId);

        /**
         * @brief Destructor.
         */
        virtual ~MessageHandlerConcept() = default;

        /**
         * @brief Invoke the message callback of the underlying message handler.
         *
         * @param pMessage The message to handle.
         */
        virtual void OnMessage(
            Message* pMessage) const = 0;

        /**
         * @brief The unique identifier of the subscription.
         */
        const std::uint64_t Identifier;
    };

    /**
     * @brief Message handler concrete implementation.
     *
     * @tparam tMessageT The type of messages that the message observer subscribes to.
     */
    template<MessageT tMessageT>
    struct MessageHandlerObserver : public MessageHandlerConcept
    {
        /**
         * @brief Constructor.
         *
         * @param pSubscriptionId The unique identifier of the subscription.
         * @param pObserver The message observer.
         */
        MessageHandlerObserver(
            std::uint64_t pSubscriptionId,
            Observer<tMessageT>* pObserver);

        /**
         * @brief Invoke the message callback of the underlying message handler.
         *
         * @param pMessage The message to handle.
         */
        void OnMessage(
            Message* pMessage) const override;

        /**
         * @brief The underlying message handler.
         */
        Observer<tMessageT>* const Handler;
    };

    /**
     * @brief Message handler concrete implementation.
     *
     * @tparam tMessageT The type of messages that the message callback subscribes to.
     */
    template<MessageT tMessageT>
    struct MessageHandlerCallback : public MessageHandlerConcept
    {
        /**
         * @brief Constructor.
         *
         * @param pSubscriptionId The unique identifier of the subscription.
         * @param pCallback The message callback.
         */
        MessageHandlerCallback(
            std::uint64_t pSubscriptionId,
            std::function<void(const tMessageT&)> pCallback);

        /**
         * @brief Invoke the message callback of the underlying message handler.
         *
         * @param pMessage The message to handle.
         */
        void OnMessage(
            Message* pMessage) const override;

        /**
         * @brief The underlying message handler.
         */
        const std::function<void(const tMessageT&)> Handler;
    };

    /**
     * @brief Buffer of messages waiting to be dispatched.
     */
    std::vector<std::unique_ptr<Message>> mMessages;

    /**
     * @brief List of subscribed message handlers.
     */
    std::vector<std::unique_ptr<MessageHandlerConcept>> mHandlers;

    /**
     * @brief Unique identifier for the next message handler subscription.
     */
    std::uint64_t mNextSubscriptionId{ 0ULL };

    /**
     * @brief Mutual exclusion to protect the buffer of messages.
     */
    std::mutex mMutexUpdateMessages;

    /**
     * @brief Mutual exclusion to protect the list of subscribed message handlers.
     */
    std::mutex mMutexUpdateHandlers;

    /**
     * @brief Worker thread wake-up condition.
     *
     * Wake up the worker thread to dispatch the buffered messages to the subscribed message handlers.
     */
    std::condition_variable_any mWorkerWakeUp;

    /**
     * @brief Worker thread.
     *
     * Dispatch the buffered messages to the subscribed message handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;
};

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher Implementation                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Core
{

inline Dispatcher::Dispatcher() :
    mWorker{ [&](std::stop_token pStopToken) { Loop(pStopToken); }}
{}

inline Dispatcher& Dispatcher::Instance()
{
    static Dispatcher sInstance;
    return sInstance;
}

inline void Dispatcher::Publish(
    std::unique_ptr<Message> pMessage)
{
    {
        std::unique_lock<std::mutex> lLock{ mMutexUpdateMessages};
        mMessages.push_back(std::move(pMessage));
    }
    mWorkerWakeUp.notify_one();
}

template<MessageT tMessageT>
inline Dispatcher::Subscription Dispatcher::Subscribe(
    Observer<tMessageT>* pObserver)
{
    std::unique_lock<std::mutex> lLock{ mMutexUpdateHandlers };
    mNextSubscriptionId++;
    mHandlers.push_back(std::make_unique<Dispatcher::MessageHandlerObserver<tMessageT>>(
        mNextSubscriptionId, pObserver));
    // Move elision is guaranteed. The Subscription destructor is not called.
    return Subscription{ mNextSubscriptionId };
}

template<MessageT tMessageT>
inline Dispatcher::Subscription Dispatcher::Subscribe(
    std::function<void(const tMessageT&)> pCallback)
{
    std::unique_lock<std::mutex> lLock{ mMutexUpdateHandlers };
    mNextSubscriptionId++;
    mHandlers.push_back(std::make_unique<Dispatcher::MessageHandlerCallback<tMessageT>>(
        mNextSubscriptionId, std::move(pCallback)));
    // Move elision is guaranteed. The Subscription destructor is not called.
    return Subscription{ mNextSubscriptionId };
}

inline void Dispatcher::Revoke(
    Dispatcher::Subscription& pSubscription)
{
    std::unique_lock<std::mutex> lLock{ mMutexUpdateHandlers };
    if (pSubscription.mIdentifier)
    {
        std::erase_if(mHandlers,
            [&pSubscription](const auto& pHandler)
            {
                return pHandler->Identifier == pSubscription.mIdentifier.value();
            });
        pSubscription.mIdentifier.reset();
    }
}

inline void Dispatcher::Loop(
    std::stop_token pStopToken)
{
    while (!pStopToken.stop_requested())
    {
        std::vector<std::unique_ptr<Message>> lBatch;
        {
            std::unique_lock<std::mutex> lLock{ mMutexUpdateMessages };
            mWorkerWakeUp.wait(lLock, pStopToken, [&]() { return !mMessages.empty(); });
            std::swap(mMessages, lBatch);
        }

        std::unique_lock<std::mutex> lLock{ mMutexUpdateHandlers };
        for (std::unique_ptr<Message>& lMessage : lBatch)
        {
            for (std::unique_ptr<MessageHandlerConcept>& lHandler : mHandlers)
            {
                lHandler->OnMessage(lMessage.get());
            }
        }
    }
}

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher::Subscription Implementation                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Core
{

inline Dispatcher::Subscription::Subscription(
    std::uint64_t pIdentifier) :
    mIdentifier(pIdentifier)
{}

inline Dispatcher::Subscription::~Subscription()
{
    Dispatcher::Instance().Revoke(*this);
}

inline Dispatcher::Subscription::Subscription(
    Dispatcher::Subscription&& pOther)
{
    std::swap(mIdentifier, pOther.mIdentifier);
}

inline Dispatcher::Subscription& Dispatcher::Subscription::operator=(
    Dispatcher::Subscription&& pOther)
{
    std::swap(mIdentifier, pOther.mIdentifier);
    return *this;
}

inline void Dispatcher::Subscription::Revoke()
{
    Dispatcher::Instance().Revoke(*this);
}

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher::MessageHandlerConcept Implementation                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Core
{

inline Dispatcher::MessageHandlerConcept::MessageHandlerConcept(
    std::uint64_t pSubscriptionId) :
    Identifier(pSubscriptionId)
{}

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher::MessageHandlerObserver Implementation                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Core
{

template<MessageT tMessageT>
inline Dispatcher::MessageHandlerObserver<tMessageT>::MessageHandlerObserver(
    std::uint64_t pSubscriptionId,
    Observer<tMessageT>* pObserver) :
    MessageHandlerConcept(pSubscriptionId),
    Handler(pObserver)
{}

template<MessageT tMessageT>
inline void Dispatcher::MessageHandlerObserver<tMessageT>::OnMessage(
    Message* pMessage) const
{
    if (auto lMessage{ dynamic_cast<tMessageT*>(pMessage) })
    {
        Handler->OnMessage(*lMessage);
    }
}

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Dispatcher::MessageHandlerCallback Implementation                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Core
{

template<MessageT tMessageT>
inline Dispatcher::MessageHandlerCallback<tMessageT>::MessageHandlerCallback(
    std::uint64_t pSubscriptionId,
    std::function<void(const tMessageT&)> pCallback) :
    MessageHandlerConcept(pSubscriptionId),
    Handler(std::move(pCallback))
{}

template<MessageT tMessageT>
inline void Dispatcher::MessageHandlerCallback<tMessageT>::OnMessage(
    Message* pMessage) const
{
    if (auto lMessage{ dynamic_cast<tMessageT*>(pMessage) })
    {
        Handler(*lMessage);
    }
}

} // Network::Core

#endif // NETWORK_CORE_DISPATCHER_HPP_INCLUDED