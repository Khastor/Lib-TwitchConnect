#ifndef NETWORK_CORE_SETTINGS_HPP_INCLUDED
#define NETWORK_CORE_SETTINGS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Settings Interface                                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <mutex>
#include <string>
#include <unordered_map>
#include <unordered_set>

namespace Network::Core
{

/**
 * @brief Network settings and credentials.
 *
 * Thread-safe singleton instance to store application settings.
 */
class Settings
{
private:
    /**
     * @brief Constructor.
     */
    Settings();

public:
    /**
     * @brief Deleted copy-constructor.
     */
    Settings(const Settings&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Settings(Settings&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Settings& operator==(const Settings&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Settings& operator==(Settings&&) = delete;

    /**
     * @brief Get the unique settings instance.
     *
     * @return The unique settings instance.
     */
    static Settings& Instance();

    /**
     * @brief Load the settings from a file.
     *
     * @param pFilename The path to the settings file.
     */
    bool LoadFromFile(
        const std::string& pFilename);

    /**
     * @brief Save the settings back to the source file.
     */
    bool SaveToFile();

    /**
     * @brief Get the Twitch application client id.
     *
     * @return The Twitch application client id.
     */
    std::string GetTwitchClientId() const;

    /**
     * @brief Get the Twitch application client secret.
     *
     * @return The Twitch application client secret.
     */
    std::string GetTwitchClientSecret() const;

    /**
     * @brief Get the Twitch application access token.
     *
     * @return The Twitch application access token.
     */
    std::string GetTwitchAppAccessToken() const;

    /**
     * @brief Set the Twitch application access token.
     *
     * @param pTwitchAppAccessToken The Twitch application access token.
     */
    void SetTwitchAppAccessToken(
        std::string pTwitchAppAccessToken);

    /**
     * @brief Get the broadcaster's Twitch user access token.
     *
     * @return The broadcaster's Twitch user access token.
     */
    std::string GetTwitchUserAccessToken() const;

    /**
     * @brief Get the broadcaster's Twitch user refresh token.
     *
     * @return The broadcaster's Twitch user refresh token.
     */
    std::string GetTwitchUserRefreshToken() const;

    /**
     * @brief Set the broadcaster's Twitch user access and refresh tokens.
     *
     * @param pTwitchUserAccessToken The broadcaster's Twitch user access token.
     * @param pTwitchUserRefreshToken The broadcaster's Twitch user refresh token.
     */
    void SetTwitchUserAccessToken(
        std::string pTwitchUserAccessToken,
        std::string pTwitchUserRefreshToken);

    /**
     * @brief Get the broadcaster's Twitch user access scopes.
     *
     * @return The broadcaster's Twitch user access scopes.
     */
    std::unordered_set<std::string> GetTwitchUserAccessScopes() const;

    /**
     * @brief Add a scope to the broadcaster's Twitch user access scopes.
     *
     * @param pScope The user access scope.
     */
    void AddTwitchUserAccessScope(
        const std::string& pScope);

    /**
     * @brief Get the broadcaster's Twitch user login.
     *
     * @return The broadcaster's Twitch user login.
     */
    std::string GetTwitchUserLogin() const;

    /**
     * @brief Get the broadcaster's Twitch user id.
     *
     * @return The broadcaster's Twitch user id.
     */
    std::string GetTwitchUserId() const;

    /**
     * @brief Get the Twitch OAuth2 callback URI.
     *
     * @return The Twitch OAuth2 callback URI.
     */
    std::string GetTwitchOAuth2CallbackURI() const;

    /**
     * @brief Get the Twitch OAuth2 callback port.
     *
     * @return The Twitch OAuth2 callback port.
     */
    int GetTwitchOAuth2CallbackPort() const;

    /**
     * @brief Set the Twitch OAuth2 callback port.
     *
     * @param pPort The Twitch OAuth2 callback port.
     */
    void SetTwitchOAuth2CallbackPort(
        int pPort);

    /**
     * @brief Get the directory containing certificate authority files.
     *
     * Each file in the directory must contain a single certificate.
     * The files must be named using the subject name's hash and an extension of ".0".
     *
     * @return The directory containing certificate authority files.
     */
    std::string GetSSLVerifyPath() const;

    /**
     * @brief Get the path to the certification authority file for the given hostname.
     *
     * The certification authority file must be in PEM format and must contain the full certificate chain for the given hostname.
     *
     * @param pHostname The hostname to verify.
     * @return The path to the certification authority file for the given hostname.
     */
    std::string GetSSLVerifyFile(
        const std::string& pHostname) const;

private:
    /**
     * @brief The log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief Mutual exclusion for all settings.
     */
    mutable std::mutex mMutex;

    /**
     * @brief The path to the settings file.
     */
    std::string mFilename;

    /**
     * @brief The Twitch application client id.
     */
    std::string mTwitchClientId;

    /**
     * @brief The Twitch application client secret.
     */
    std::string mTwitchClientSecret;

    /**
     * @brief The Twitch application access token.
     */
    std::string mTwitchAppAccessToken;

    /**
     * @brief The broadcaster's Twitch user access token.
     */
    std::string mTwitchUserAccessToken;

    /**
     * @brief The broadcaster's Twitch user refresh token.
     */
    std::string mTwitchUserRefreshToken;

    /**
     * @brief The broadcaster's Twitch user access scopes.
     */
    std::unordered_set<std::string> mTwitchUserAccessScopes;

    /**
     * @brief The broadcaster's Twitch user login.
     */
    std::string mTwitchUserLogin;

    /**
     * @brief The broadcaster's Twitch user id.
     */
    std::string mTwitchUserId;

    /**
     * @brief The Twitch OAuth2 callback port.
     */
    int mTwitchOAuth2CallbackPort{ 3000 };

    /**
     * @brief The directory containing certificate authority files.
     */
    std::string mSSLVerifyPath;

    /**
     * @brief The path to the certification authority file for a given hostname.
     */
    std::unordered_map<std::string, std::string> mSSLVerifyFile;
};

} // Network::Core

#endif // NETWORK_CORE_SETTINGS_HPP_INCLUDED