#ifndef NETWORK_CORE_HTTP_CONSTANTS_HPP_INCLUDED
#define NETWORK_CORE_HTTP_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTP Constants Definitions                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Core
{

/**
 * @brief HTTP Authorization header prefix for bearer tokens.
 */
inline const std::string HTTP_AUTHORIZATION_BEARER{ "Bearer " };

/**
 * @brief HTTP Content-Type header enumerator for JSON data.
 */
inline const std::string HTTP_CONTENT_TYPE_APPLICATION_JSON{ "application/json"};

/**
 * @brief HTTP Content-Type header enumerator for URL-encoded data.
 */
inline const std::string HTTP_CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED{ "application/x-www-form-urlencoded" };

/**
 * @brief HTTP Content-Type header enumerator for text data.
 */
inline const std::string HTTP_CONTENT_TYPE_TEXT_HTML{ "text/html" };

} // Network::Core

#endif // NETWORK_CORE_HTTP_CONSTANTS_HPP_INCLUDED