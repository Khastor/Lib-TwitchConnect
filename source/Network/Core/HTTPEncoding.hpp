#ifndef NETWORK_CORE_HTTP_ENCODING_HPP_INCLUDED
#define NETWORK_CORE_HTTP_ENCODING_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTP Encoding Interface                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <optional>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace Network::Core
{

/**
 * @brief Build a URL-encoded parameter key or value.
 *
 * @param pText The parameter key or value to encode.
 * @return The URL-encoded parameter key or value.
 */
std::string URLEncoded(
    const std::string& pText);

/**
 * @brief Decode a URL-encoded parameter key or value.
 *
 * This function checks the validity of the URL-encoded text to decode.
 *
 * @param pText The URL-encoded parameter key or value to decode.
 * @return The decoded parameter key or value, or empty not valid.
 */
std::optional<std::string> URLDecoded(
    const std::string& pText);

/**
 * @brief Build a URL with a query string.
 *
 * The caller is responsible for URL-encoding the query parameter keys and values.
 *
 * @param pBaseURL The base URL.
 * @param pQueryParameters The URL-encoded query parameters.
 * @return The full URL containing the query string.
*/
std::string BuildURLWithQueryString(
    const std::string& pBaseURL,
    const std::vector<std::pair<std::string, std::string>>& pQueryParameters);

/**
 * @brief Build a query string.
 *
 * The caller is responsible for URL-encoding the query parameter keys and values.
 *
 * @param pQueryParameters The URL-encoded query parameters.
 * @return The query string.
 */
std::string BuildQueryString(
    const std::vector<std::pair<std::string, std::string>>& pQueryParameters);

/**
 * @brief Parse parameter keys and values from a query string.
 *
 * @param pURL The URL containing the query string, or just the query string.
 * @return The parameter keys and values as encoded in the query string.
 */
std::unordered_map<std::string, std::string> ParseQueryString(
    const std::string& pURL);

} // Network::Core

#endif // NETWORK_CORE_HTTP_ENCODING_HPP_INCLUDED