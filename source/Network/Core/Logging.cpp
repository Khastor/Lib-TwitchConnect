///////////////////////////////////////////////////////////////////////////////////////////////////
// Logging Implementation                                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/core/null_deleter.hpp>
#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/log/attributes.hpp>
#include <boost/log/expressions.hpp>
#include <boost/log/keywords/channel.hpp>
#include <boost/log/sinks/sync_frontend.hpp>
#include <boost/log/sinks/text_ostream_backend.hpp>
#include <boost/log/support/date_time.hpp>
#include <boost/log/utility/exception_handler.hpp>
#include <boost/log/utility/setup/common_attributes.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <array>
#include <fstream>
#include <iomanip>

BOOST_LOG_ATTRIBUTE_KEYWORD(channel, "Channel", std::string)
BOOST_LOG_ATTRIBUTE_KEYWORD(severity, "Severity", Network::Core::SeverityLevel)

namespace Network::Core
{

namespace
{

/**
 * @brief Common log record formatter.
 */
const boost::log::formatter gFormatter = boost::log::expressions::stream
    << boost::log::expressions::format_date_time<boost::posix_time::ptime>("TimeStamp", "[%Y-%m-%d %H:%M:%S.%f] ")
    << std::left << std::setw(9) << severity << " [" << channel << "] "
    << boost::log::expressions::smessage;

} // anonymous namespace

std::ostream& operator<<(
    std::ostream& pStream,
    Network::Core::SeverityLevel pLevel)
{
    static const std::array sSeverityLevelName{ "[trace]", "[info]", "[warning]", "[error]" };
    return pStream << sSeverityLevelName[pLevel];
}

Logger MakeLogger(
    const std::string& pChannel,
    const std::string& pFilename)
{
    using TextStreamSink = boost::log::sinks::synchronous_sink<boost::log::sinks::text_ostream_backend>;

    // Initialize the common logging configuration exactly once.
    struct Initializer
    {
        Initializer()
        {
            boost::log::add_common_attributes();

            boost::shared_ptr<TextStreamSink> lConsoleSink = boost::make_shared<TextStreamSink>();
            lConsoleSink->locked_backend()->add_stream(boost::shared_ptr<std::ostream>(&std::clog, boost::null_deleter()));
            lConsoleSink->locked_backend()->auto_flush();
            lConsoleSink->set_formatter(gFormatter);
            lConsoleSink->set_filter(severity >= Network::Core::SeverityLevel::Info);
            lConsoleSink->set_exception_handler(boost::log::make_exception_suppressor());
            boost::log::core::get()->add_sink(lConsoleSink);
        }
    };
    static const Initializer sInitializer;

    boost::shared_ptr<TextStreamSink> lChannelFileSink = boost::make_shared<TextStreamSink>();
    lChannelFileSink->locked_backend()->add_stream(boost::make_shared<std::ofstream>(pFilename));
    lChannelFileSink->locked_backend()->auto_flush();
    lChannelFileSink->set_formatter(gFormatter);
    lChannelFileSink->set_filter(channel == pChannel);
    lChannelFileSink->set_exception_handler(boost::log::make_exception_suppressor());
    boost::log::core::get()->add_sink(lChannelFileSink);

    return Logger{ boost::log::keywords::channel = pChannel };
}

} // Network::Core