#ifndef NETWORK_CORE_MESSAGE_HPP_INCLUDED
#define NETWORK_CORE_MESSAGE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Message Definition                                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <type_traits>

namespace Network::Core
{

/**
 * @brief Base class in the hierarchy of messages.
 */
struct Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Message() = default;
};

/**
 * @brief Message concept.
 */
template<typename T>
concept MessageT = std::is_base_of_v<Network::Core::Message, T>;

} // Network::Core

#endif // NETWORK_CORE_MESSAGE_HPP_INCLUDED