#ifndef NETWORK_CORE_OBSERVER_HPP_INCLUDED
#define NETWORK_CORE_OBSERVER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Observer Interface                                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Core
{

/**
 * @brief Message observer.
 *
 * Provide a callback for handling messages asynchronously when subscribed to the message dispatcher.
 * It is advised to subscribe and revoke subscriptions in the constructor and destructor of the most derived message observer class.
 * It is forbidden to create or revoke a subscription from a message callback invoked from the message dispatcher thread.
 *
 * @tparam tMessageT The type of messages that the message observer subscribes to.
 */
template<MessageT tMessageT>
class Observer
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~Observer() = default;

    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     *
     * @param pMessage The message to handle.
     */
    virtual void OnMessage(
        const tMessageT& pMessage) = 0;
};

} // Network::Core

#endif // NETWORK_CORE_OBSERVER_HPP_INCLUDED