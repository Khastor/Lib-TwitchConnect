///////////////////////////////////////////////////////////////////////////////////////////////////
// Settings Implementation                                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Settings.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/xml_parser.hpp>

namespace Network::Core
{

namespace
{

const std::string LOG_CHANNEL{ "Settings" };
const std::string LOG_FILENAME{ "SettingsLogs.log" };

const std::string SETTINGS_CREDENTIALS_TWITCH_CLIENT_ID{ "Settings.Credentials.Twitch.ClientId" };
const std::string SETTINGS_CREDENTIALS_TWITCH_CLIENT_SECRET{ "Settings.Credentials.Twitch.ClientSecret" };
const std::string SETTINGS_CREDENTIALS_TWITCH_APP_ACCESS_TOKEN{ "Settings.Credentials.Twitch.AppAccessToken" };
const std::string SETTINGS_CREDENTIALS_TWITCH_USER_ACCESS_TOKEN{ "Settings.Credentials.Twitch.UserAccessToken" };
const std::string SETTINGS_CREDENTIALS_TWITCH_USER_REFRESH_TOKEN{ "Settings.Credentials.Twitch.UserRefreshToken" };
const std::string SETTINGS_CREDENTIALS_TWITCH_USER_LOGIN{ "Settings.Credentials.Twitch.UserLogin" };
const std::string SETTINGS_CREDENTIALS_TWITCH_USER_ID{ "Settings.Credentials.Twitch.UserId" };
const std::string SETTINGS_NETWORK_SSL_VERIFY_PATH{ "Settings.Network.SSLVerifyPath" };
const std::string SETTINGS_NETWORK_SSL_VERIFY_FILE{ "Settings.Network.SSLVerifyFile" };
const std::string SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST{ "Settings.Network.SSLVerifyFile.Host" };
const std::string SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_HOSTNAME{ "Hostname" };
const std::string SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_FILENAME{ "Filename" };
const std::string SETTINGS_CALLBACK_URI_PREFIX{ "http://localhost:" };

template<typename tPropertyT>
bool ReadProperty(
    const boost::property_tree::ptree& pPropertyTree,
    const std::string& pKey,
    tPropertyT& pValue)
{
    if (const auto& lNodeOptional{ pPropertyTree.get_child_optional(pKey) })
    {
        const auto& lNode{ lNodeOptional.get() };
        if (!lNode.data().empty())
        {
            pValue = lNode.get_value<tPropertyT>();
            return true;
        }
    }
    return false;
}

template<typename tPropertyT>
void WriteProperty(
    boost::property_tree::ptree& pPropertyTree,
    const std::string& pKey,
    const tPropertyT& pValue)
{
    pPropertyTree.add<tPropertyT>(pKey, pValue);
}

} // anonymous namespace

Settings::Settings() :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME))
{}

Settings& Settings::Instance()
{
    static Settings sInstance;
    return sInstance;
}

bool Settings::LoadFromFile(
    const std::string& pFilename)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    boost::property_tree::ptree lSettingsXML;
    try
    {
        mFilename = pFilename;
        boost::property_tree::read_xml(mFilename, lSettingsXML);
        if (!ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_CLIENT_ID, mTwitchClientId))
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "cannot find property: " << SETTINGS_CREDENTIALS_TWITCH_CLIENT_ID;
        }
        if (!ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_CLIENT_SECRET, mTwitchClientSecret))
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "cannot find property: " << SETTINGS_CREDENTIALS_TWITCH_CLIENT_SECRET;
        }
        ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_APP_ACCESS_TOKEN, mTwitchAppAccessToken);
        ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_ACCESS_TOKEN, mTwitchUserAccessToken);
        ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_REFRESH_TOKEN, mTwitchUserRefreshToken);
        if (!ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_LOGIN, mTwitchUserLogin))
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "cannot find property: " << SETTINGS_CREDENTIALS_TWITCH_USER_LOGIN;
        }
        if (!ReadProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_ID, mTwitchUserId))
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "cannot find property: " << SETTINGS_CREDENTIALS_TWITCH_USER_ID;
        }
        ReadProperty(lSettingsXML, SETTINGS_NETWORK_SSL_VERIFY_PATH, mSSLVerifyPath);
        if (const auto& lVerifyFileNodeOptional{ lSettingsXML.get_child_optional(SETTINGS_NETWORK_SSL_VERIFY_FILE) })
        {
            for (const auto& lVerifyFileHostNode : lVerifyFileNodeOptional.get())
            {
                std::string lHostname, lFilename;
                ReadProperty(lVerifyFileHostNode.second, SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_HOSTNAME, lHostname);
                ReadProperty(lVerifyFileHostNode.second, SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_FILENAME, lFilename);
                mSSLVerifyFile[lHostname] = lFilename;
            }
        }
    }
    catch (boost::property_tree::xml_parser_error& pException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "failed to load settings from file: " << pException.what();
        return false;
    }
    return true;
}

bool Settings::SaveToFile()
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    boost::property_tree::ptree lSettingsXML;
    try
    {
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_CLIENT_ID, mTwitchClientId);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_CLIENT_SECRET, mTwitchClientSecret);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_APP_ACCESS_TOKEN, mTwitchAppAccessToken);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_ACCESS_TOKEN, mTwitchUserAccessToken);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_REFRESH_TOKEN, mTwitchUserRefreshToken);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_LOGIN, mTwitchUserLogin);
        WriteProperty(lSettingsXML, SETTINGS_CREDENTIALS_TWITCH_USER_ID, mTwitchUserId);
        WriteProperty(lSettingsXML, SETTINGS_NETWORK_SSL_VERIFY_PATH, mSSLVerifyPath);
        for (const auto& [lHostname, lFilename] : mSSLVerifyFile)
        {
            boost::property_tree::ptree lHostSettingsXML;
            WriteProperty(lHostSettingsXML, SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_HOSTNAME, lHostname);
            WriteProperty(lHostSettingsXML, SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST_FILENAME, lFilename);
            lSettingsXML.add_child(SETTINGS_NETWORK_SSL_VERIFY_FILE_HOST, lHostSettingsXML);
        }
        const auto lWriterSettings{ boost::property_tree::xml_writer_make_settings<std::string>(' ', 4) };
        boost::property_tree::write_xml(mFilename, lSettingsXML, std::locale(), lWriterSettings);
    }
    catch (boost::property_tree::xml_parser_error& pException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "failed to save settings to file: " << pException.what();
        return false;
    }
    return true;
}

std::string Settings::GetTwitchClientId() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchClientId;
}

std::string Settings::GetTwitchClientSecret() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchClientSecret;
}

std::string Settings::GetTwitchAppAccessToken() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchAppAccessToken;
}

void Settings::SetTwitchAppAccessToken(
    std::string pTwitchAppAccessToken)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    mTwitchAppAccessToken = pTwitchAppAccessToken;
}

std::string Settings::GetTwitchUserAccessToken() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchUserAccessToken;
}

std::string Settings::GetTwitchUserRefreshToken() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchUserRefreshToken;
}

void Settings::SetTwitchUserAccessToken(
    std::string pTwitchUserAccessToken,
    std::string pTwitchUserRefreshToken)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    mTwitchUserAccessToken = pTwitchUserAccessToken;
    mTwitchUserRefreshToken = pTwitchUserRefreshToken;
}

std::unordered_set<std::string> Settings::GetTwitchUserAccessScopes() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchUserAccessScopes;
}

void Settings::AddTwitchUserAccessScope(
    const std::string& pScope)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    mTwitchUserAccessScopes.emplace(pScope);
}

std::string Settings::GetTwitchUserLogin() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchUserLogin;
}

std::string Settings::GetTwitchUserId() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchUserId;
}

std::string Settings::GetTwitchOAuth2CallbackURI() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return SETTINGS_CALLBACK_URI_PREFIX + std::to_string(mTwitchOAuth2CallbackPort);
}

int Settings::GetTwitchOAuth2CallbackPort() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mTwitchOAuth2CallbackPort;
}

void Settings::SetTwitchOAuth2CallbackPort(
    int pPort)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    mTwitchOAuth2CallbackPort = pPort;
}

std::string Settings::GetSSLVerifyPath() const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    return mSSLVerifyPath;
}

std::string Settings::GetSSLVerifyFile(
    const std::string& pHostname) const
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    if (const auto& it{ mSSLVerifyFile.find(pHostname) }; it != mSSLVerifyFile.cend())
    {
        return it->second;
    }
    return {};
}

} // Network::Core