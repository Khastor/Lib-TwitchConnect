#ifndef NETWORK_CORE_HTTP_CALLBACK_SESSION_HPP_INCLUDED
#define NETWORK_CORE_HTTP_CALLBACK_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTPCallbackSession Interface                                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------------- Boost Headers
#include <Network/Core/Logging.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstddef>
#include <cstdint>
#include <memory>
#include <optional>
#include <string>

namespace Network::Core
{

/**
 * @brief HTTP callback session.
 */
class HTTPCallbackSession : public std::enable_shared_from_this<HTTPCallbackSession>
{
public:
    /**
     * @brief Constructor.
     *
     * Derived classes must be allocated dynamically with std::make_shared().
     *
     * @param pLogger The log source.
     * @param pSocket The TCP socket to communicate with the remote host.
     */
    HTTPCallbackSession(
        Network::Core::Logger& pLogger,
        boost::asio::ip::tcp::socket&& pSocket);

    /**
     * @brief Destructor.
     */
    virtual ~HTTPCallbackSession() = default;

    /**
     * @brief Start the callback session.
     *
     * Handlers will be invoked asynchronously from the execution context thread bound to the TCP socket.
     */
    void Start();

private:
    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the callback session fails to read the HTTP request from the remote host.
     *
     * @param pError The error message.
     */
    virtual void OnReadError(
        const std::string& pError) = 0;

    /**
     * @brief HTTP request handler.
     *
     * This handler is invoked when the callback session receives the HTTP request from the remote host.
     *
     * @param pHTTPRequest The HTTP request.
     * @return The optional HTTP response.
     */
    virtual std::optional<boost::beast::http::response<boost::beast::http::string_body>> OnHTTPRequest(
        const boost::beast::http::request<boost::beast::http::string_body>& pHTTPRequest) = 0;

    /**
     * @brief Read handler.
     *
     * This handler is invoked when the callback session read completes.
     * Invokes the read error handler on error, otherwise invokes the HTTP request handler.
     *
     * @param pError The error details.
     * @param pBytes The number of bytes read.
     */
    void OnReadHandler(
        boost::beast::error_code pError,
        std::size_t pBytes);

    /**
     * @brief Write handler.
     *
     * This handler is invoked when the callback session write completes.
     *
     * @param pError The error details.
     * @param pBytes The number of bytes written.
     */
    void OnWriteHandler(
        boost::beast::error_code pError,
        std::size_t pBytes);

    /**
     * @brief Shutdown the callback session.
     */
    void Shutdown();

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The TCP stream to communicate with the remote host.
     */
    boost::beast::tcp_stream mStream;

    /**
     * @brief The HTTP request buffer.
     */
    boost::beast::flat_buffer mBuffer;

    /**
     * @brief The HTTP request.
     */
    boost::beast::http::request<boost::beast::http::string_body> mHTTPRequest;

    /**
     * @brief The HTTP response.
     */
    std::optional<boost::beast::http::response<boost::beast::http::string_body>> mHTTPResponse;
};

} // Network::Core

#endif // NETWORK_CORE_HTTP_CALLBACK_SESSION_HPP_INCLUDED