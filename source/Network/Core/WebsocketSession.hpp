#ifndef NETWORK_CORE_WEBSOCKET_SESSION_HPP_INCLUDED
#define NETWORK_CORE_WEBSOCKET_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// WebsocketSession Interface                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/websocket.hpp>
#include <boost/beast/websocket/ssl.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <memory>
#include <optional>
#include <string>

namespace Network::Core
{

/**
 * @brief Secure websocket session over SSL.
 */
class WebsocketSession : public std::enable_shared_from_this<WebsocketSession>
{
public:
    /**
     * @brief Constructor.
     *
     * Derived classes must be allocated dynamically with std::make_shared().
     *
     * @param pLogger The log source.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     * @param pHost The remote host name.
     * @param pPort The remote host port.
     * @param pEndpoint The target endpoint.
     */
    WebsocketSession(
        Network::Core::Logger& pLogger,
        boost::asio::io_context& pIOContext,
        const std::string& pHost,
        const std::string& pPort,
        const std::string& pEndpoint);

    /**
     * @brief Destructor.
     */
    virtual ~WebsocketSession() = default;

    /**
     * @brief Start the session.
     *
     * Handlers will be invoked asynchronously from the execution context thread.
     */
    void Start();

    /**
     * @brief Set the idle timeout, in seconds.
     *
     * The idle timeout is optional, and is disabled by default.
     * This method is not thread-safe and must not be called after the session has started.
     *
     * @param pTimeout The idle timeout, in seconds.
     */
    void SetIdleTimeout(
        std::uint64_t pTimeout);

private:
    /**
     * @brief Resolve error handler.
     *
     * This handler is invoked when the session fails to find the remote host address.
     *
     * @param pError The error message.
     */
    virtual void OnResolveError(
        const std::string& pError) = 0;

    /**
     * @brief Connect error handler.
     *
     * This handler is invoked when the session fails to connect to the remote host.
     *
     * @param pError The error message.
     */
    virtual void OnConnectError(
        const std::string& pError) = 0;

    /**
     * @brief SSL handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate a secure connection with the remote host.
     * This can happen if the client cannot verify the SSL certificate chain of the remote host.
     *
     * @param pError The error message.
     */
    virtual void OnSSLHandshakeError(
        const std::string& pError) = 0;

    /**
     * @brief Websocket handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate the upgrade of the websocket connection with the remote host.
     *
     * @param pError The error message.
     */
    virtual void OnWebsocketHandshakeError(
        const std::string& pError) = 0;

    /**
     * @brief Websocket read error handler.
     *
     * This handler is invoked when the session fails to read a websocket message from the remote host.
     *
     * @param pError The error message.
     */
    virtual void OnWebsocketReadError(
        const std::string& pError) = 0;

    /**
     * @brief Websocket message handler.
     *
     * This handler is invoked when the session receives a websocket message from the remote host.
     *
     * @param The websocket message.
     * @return True to keep the websocket connection alive, false to close the connection.
     */
    virtual bool OnWebsocketMessage(
        const std::string& pMessage) = 0;

    /**
     * @brief Resolve handler.
     *
     * This handler is invoked when the session resolve completes.
     * Invokes the resolve error handler on error.
     *
     * @param pError The error details.
     * @param pEndpoints The resolved endpoints.
     */
    void OnResolveHandler(
        boost::beast::error_code pError,
        boost::asio::ip::tcp::resolver::results_type pEndpoints);

    /**
     * @brief Connect handler.
     *
     * This handler is invoked when the session connect completes.
     * Invokes the connect error handler on error.
     *
     * @param pError The error details.
     */
    void OnConnectHandler(
        boost::beast::error_code pError,
        boost::asio::ip::tcp::resolver::results_type::endpoint_type);

    /**
     * @brief SSL Handshake handler.
     *
     * This handler is invoked when the session SSL handshake completes.
     * Invokes the SSL handshake error handler on error.
     *
     * @param pError The error details.
     */
    void OnSSLHandshakeHandler(
        boost::beast::error_code pError);

    /**
     * @brief Websocket Handshake handler.
     *
     * This handler is invoked when the session websocket handshake completes.
     * Invokes the websocket handshake error handler on error.
     *
     * @param pError The error details.
     */
    void OnWebsocketHandshakeHandler(
        boost::beast::error_code pError);

    /**
     * @brief Read handler.
     *
     * This handler is invoked when a session read completes.
     * Invokes the read error handler on error, otherwise invokes the websocket message handler.
     *
     * @param pError The error details.
     * @param pBytes The number of bytes read.
     */
    void OnReadHandler(
        boost::beast::error_code pError,
        std::size_t pBytes);

    /**
     * @brief Close handler.
     *
     * This handler is invoked when the session shutdown completes.
     *
     * @param pError The error details.
     */
    void OnCloseHandler(
        boost::beast::error_code pError);

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The SSL context.
     */
    boost::asio::ssl::context mSSLContext;

    /**
     * @brief The idle timeout, in seconds.
     */
    std::optional<std::uint64_t> mIdleTimeout;

    /**
     * @brief The TCP resolver to find the address of the remote host.
     */
    boost::asio::ip::tcp::resolver mResolver;

    /**
     * @brief The websocket SSL stream to communicate with the remote host.
     */
    boost::beast::websocket::stream<boost::beast::ssl_stream<boost::beast::tcp_stream>> mStream;

    /**
     * @brief The websocket read buffer.
     */
    boost::beast::flat_buffer mBuffer;

    /**
     * @brief The remote host name.
     */
    std::string mHost;

    /**
     * @brief The remote host port.
     */
    std::string mPort;

    /**
     * @brief The target endpoint.
     */
    std::string mEndpoint;
};

} // Network::Core

#endif // NETWORK_CORE_WEBSOCKET_SESSION_HPP_INCLUDED