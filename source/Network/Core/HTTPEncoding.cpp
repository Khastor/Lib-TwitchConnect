///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTP Encoding Implementation                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPEncoding.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/regex.hpp>
#include <boost/url.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cctype>
#include <iomanip>
#include <sstream>

namespace Network::Core
{

constexpr char QUERY_STRING_SEP{ '?' };
constexpr char QUERY_STRING_AND{ '&' };
constexpr char QUERY_STRING_EQ{ '=' };

const boost::regex QUERY_STRING_PARAMETER_REGEXP{ "^[&\?]?([^=&\?]+)=([^=&\?]*)" };

std::string URLEncoded(
    const std::string& pText)
{
    const boost::urls::encoding_opts lEncodeWithSpaceAsPlus{ true, false, false };
    return boost::urls::encode(pText, boost::urls::unreserved_chars, lEncodeWithSpaceAsPlus);
}

std::optional<std::string> URLDecoded(
    const std::string& pText)
{
    if (const auto lValidatedText{ boost::urls::make_pct_string_view(pText) }; lValidatedText.has_value())
    {
        const boost::urls::encoding_opts lDecodeWithSpaceAsPlus{ true, false, false };
        return lValidatedText.value().decode(lDecodeWithSpaceAsPlus);
    }
    return {};
}

std::string BuildURLWithQueryString(
    const std::string& pBaseURL,
    const std::vector<std::pair<std::string, std::string>>& pQueryParameters)
{
    std::string lUrlWithQueryString{ pBaseURL + QUERY_STRING_SEP };
    for (const auto& [lKey, lValue] : pQueryParameters)
    {
        if (!lKey.empty())
        {
            lUrlWithQueryString += (lKey + QUERY_STRING_EQ + lValue + QUERY_STRING_AND);
        }
    }
    lUrlWithQueryString.pop_back();
    return lUrlWithQueryString;
}

std::string BuildQueryString(
    const std::vector<std::pair<std::string, std::string>>& pQueryParameters)
{
    std::string lQueryString;
    for (const auto& [lKey, lValue] : pQueryParameters)
    {
        if (!lKey.empty())
        {
            lQueryString += (lKey + QUERY_STRING_EQ + lValue + QUERY_STRING_AND);
        }
    }
    if (!lQueryString.empty())
    {
        lQueryString.pop_back();
    }
    return lQueryString;
}

std::unordered_map<std::string, std::string> ParseQueryString(
    const std::string& pURL)
{
    size_t lQueryStringPosition{ pURL.find(QUERY_STRING_SEP) };
    if (lQueryStringPosition == std::string::npos)
    {
        lQueryStringPosition = 0;
    }
    std::string::const_iterator lQueryStringStart{ pURL.begin() + lQueryStringPosition };
    std::string::const_iterator lQueryStringEnd{ pURL.end() };

    std::unordered_map<std::string, std::string> lQueryParameters;
    boost::match_results<std::string::const_iterator> lResult;
    while (boost::regex_search(lQueryStringStart, lQueryStringEnd, lResult, QUERY_STRING_PARAMETER_REGEXP))
    {
        if (lResult.size() == 3)
        {
            std::string lKey{ lResult[1].first, lResult[1].second };
            std::string lValue{ lResult[2].first, lResult[2].second };
            lQueryParameters[std::move(lKey)] = std::move(lValue);
        }
        lQueryStringStart = lResult[0].second;
    }
    return lQueryParameters;
}

} // Network::Core