///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTPSession Implementation                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPSession.hpp>
#include <Network/Core/Settings.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/ssl/host_name_verification.hpp>
#include <boost/bind/bind.hpp>
#include <boost/chrono/chrono.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <filesystem>
#include <string>
#include <utility>

namespace Network::Core
{

namespace
{

std::string GetHostName(
    const std::string& pHostname)
{
    return pHostname.substr(0, pHostname.find(':'));
}

std::string GetHostPort(
    const std::string& pHostname)
{
    const std::string::size_type lOffset{ pHostname.find(':') };
    if (lOffset != std::string::npos)
    {
        return pHostname.substr(lOffset + 1);
    }
    return {};
}

} // anonymous namespace

const std::string LOG_PREFIX{ "[HTTPSession] " };
constexpr int HTTP_SESSION_TIMEOUT_SECONDS{ 10 };

HTTPSession::HTTPSession(
    Network::Core::Logger& pLogger,
    boost::asio::io_context& pIOContext) :
    mLogger(pLogger),
    mIOContext(pIOContext),
    mSSLContext(boost::asio::ssl::context::sslv23),
    mRetryTimer(mIOContext),
    mResolver(mIOContext)
{}

void HTTPSession::Start()
{
    mRetryTimer.expires_after(boost::asio::chrono::seconds(std::exchange(mRetryDelay, 0ULL)));
    mRetryTimer.async_wait(
        boost::beast::bind_front_handler(
            &HTTPSession::OnStartHandler,
            shared_from_this()));
}

void HTTPSession::SetRetryLimit(
    std::uint64_t pCount)
{
    mRetryLimit = pCount;
}

void HTTPSession::SetRetryTimeout(
    std::uint64_t pTimeout)
{
    mRetryTimeout = std::chrono::steady_clock::now() + std::chrono::seconds(pTimeout);
}

void HTTPSession::SetRetryDelay(
    std::uint64_t pDelay)
{
    mRetryDelay = pDelay;
}

bool HTTPSession::HasRetryBudget() const
{
    if (mRetryTimeout && mRetryTimeout.value() <= std::chrono::steady_clock::now())
    {
        return false;
    }
    if (mRetryLimit && mRetryLimit.value() <= mRetryCount)
    {
        return false;
    }
    return true;
}

void HTTPSession::OnStartHandler(
    const boost::system::error_code& pError)
{
    if (pError)
    {
        OnStartError(pError.message(), Shutdown(true));
        return;
    }

    // Initialize the HTTP request. The content of the request may change after each restart.
    mHTTPRequest = BuildHTTPRequest();
    mHost = GetHostName(mHTTPRequest[boost::beast::http::field::host]);
    mPort = GetHostPort(mHTTPRequest[boost::beast::http::field::host]);

    // Initialize the SSL context and add an optional certification authority file or directory.
    boost::system::error_code lError;
    mSSLContext.set_default_verify_paths(lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mSSLContext.set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert, lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mSSLContext.set_verify_callback(boost::asio::ssl::host_name_verification(mHost), lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    const std::string lSSLVerifyFile{ Network::Core::Settings::Instance().GetSSLVerifyFile(mHost) };
    if (std::filesystem::is_regular_file(lSSLVerifyFile))
    {
        mSSLContext.load_verify_file(lSSLVerifyFile, lError);
        if (lError)
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
        }
    }
    else if (!lSSLVerifyFile.empty())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "Bad SSL verify file: " << lSSLVerifyFile;
    }
    else
    {
        const std::string lSSLVerifyPath{ Network::Core::Settings::Instance().GetSSLVerifyPath() };
        if (std::filesystem::is_directory(lSSLVerifyPath))
        {
            mSSLContext.add_verify_path(lSSLVerifyPath, lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
            }
        }
        else if (!lSSLVerifyPath.empty())
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "Bad SSL verify path: " << lSSLVerifyPath;
        }
    }

    // Initialize the SSL stream and set SNI Hostname (many hosts need this to handshake successfully).
    mStream = std::make_unique<boost::beast::ssl_stream<boost::beast::tcp_stream>>(mIOContext, mSSLContext);
    if (!SSL_set_tlsext_host_name(mStream->native_handle(), mHost.c_str()))
    {
        const boost::beast::error_code lError{ static_cast<int>(::ERR_get_error()), boost::beast::net::error::get_ssl_category() };
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mResolver.async_resolve(mHost, mPort,
        boost::beast::bind_front_handler(
            &HTTPSession::OnResolveHandler,
            shared_from_this()));
}

void HTTPSession::OnResolveHandler(
    boost::beast::error_code pError,
    boost::asio::ip::tcp::resolver::results_type pEndpoints)
{
    if (pError)
    {
        OnResolveError(pError.message(), Shutdown(true));
        return;
    }

    boost::beast::get_lowest_layer(*mStream).expires_after(std::chrono::seconds(HTTP_SESSION_TIMEOUT_SECONDS));
    boost::beast::get_lowest_layer(*mStream).async_connect(pEndpoints,
        boost::beast::bind_front_handler(
            &HTTPSession::OnConnectHandler,
            shared_from_this()));
}

void HTTPSession::OnConnectHandler(
    boost::beast::error_code pError,
    boost::asio::ip::tcp::resolver::results_type::endpoint_type)
{
    if (pError)
    {
        OnConnectError(pError.message(), Shutdown(true));
        return;
    }

    boost::beast::get_lowest_layer(*mStream).expires_after(std::chrono::seconds(HTTP_SESSION_TIMEOUT_SECONDS));
    mStream->async_handshake(boost::asio::ssl::stream_base::client,
        boost::beast::bind_front_handler(
            &HTTPSession::OnHandshakeHandler,
            shared_from_this()));
}

void HTTPSession::OnHandshakeHandler(
    boost::beast::error_code pError)
{
    if (pError)
    {
        OnHandshakeError(pError.message(), Shutdown(true));
        return;
    }

    boost::beast::get_lowest_layer(*mStream).expires_after(std::chrono::seconds(HTTP_SESSION_TIMEOUT_SECONDS));
    boost::beast::http::async_write(*mStream, mHTTPRequest,
        boost::beast::bind_front_handler(
            &HTTPSession::OnWriteHandler,
            shared_from_this()));
}

void HTTPSession::OnWriteHandler(
    boost::beast::error_code pError,
    std::size_t pBytes)
{
    if (pError)
    {
        OnWriteError(pError.message(), Shutdown(true));
        return;
    }

    mHTTPResponse = {};
    boost::beast::get_lowest_layer(*mStream).expires_after(std::chrono::seconds(HTTP_SESSION_TIMEOUT_SECONDS));
    boost::beast::http::async_read(*mStream, mBuffer, mHTTPResponse,
        boost::beast::bind_front_handler(
            &HTTPSession::OnReadHandler,
            shared_from_this()));
}

void HTTPSession::OnReadHandler(
    boost::beast::error_code pError,
    std::size_t pBytes)
{
    if (pError)
    {
        OnReadError(pError.message(), Shutdown(true));
        return;
    }

    Shutdown(!OnHTTPResponse(mHTTPResponse));
}

bool HTTPSession::Shutdown(
    bool pRetry)
{
    if (pRetry && !HasRetryBudget())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "retry limit exceeded";
    }

    const bool lShutdownThenRetry{ pRetry && HasRetryBudget() };
    boost::beast::get_lowest_layer(*mStream).expires_after(std::chrono::seconds(HTTP_SESSION_TIMEOUT_SECONDS));
    mStream->async_shutdown(
        boost::beast::bind_front_handler(
            &HTTPSession::OnShutdownHandler,
            shared_from_this(),
            lShutdownThenRetry));
    return !lShutdownThenRetry;
}

void HTTPSession::OnShutdownHandler(
    bool pRetry,
    boost::beast::error_code pError)
{
    // HTTPS servers will often misbehave when closing the connection and not negociate shutdown properly.
    // https://stackoverflow.com/questions/25587403/boost-asio-ssl-async-shutdown-always-finishes-with-an-error
    if (pError && pError != boost::asio::error::eof)
    {
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "shutdown error: " << pError.message();
        if (pError == boost::asio::ssl::error::stream_truncated)
        {
            NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "peer may have closed the connection without negociating shutdown";
        }
    }

    if (pRetry)
    {
        mRetryCount++;
        if (!mRetryDelay)
        {
            mRetryDelay = (1ULL << std::min(mRetryCount, 5ULL));
        }
        NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "retry delay: " << mRetryDelay << "s";
        Start();
    }
}

} // Network::Core