#ifndef NETWORK_CORE_LOGGING_HPP_INCLUDED
#define NETWORK_CORE_LOGGING_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Logging Interface                                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/log/sources/record_ostream.hpp>
#include <boost/log/sources/severity_channel_logger.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <iostream>
#include <string>

namespace Network::Core
{

/**
 * @brief Log record severity level.
 */
enum SeverityLevel
{
    Trace,
    Info,
    Warning,
    Error
};

std::ostream& operator<<(
    std::ostream& pStream,
    Network::Core::SeverityLevel pLevel);

/**
 * @brief Log source with severity level and channel attributes.
 */
using Logger = boost::log::sources::severity_channel_logger_mt<SeverityLevel, std::string>;

/**
 * @brief Create a log source bound to a channel name.
 *
 * The log source is associated with its own file sink.
 *
 * @param pChannel The channel name.
 * @param pFilename The sink filename.
 * @return The new log source bound to the channel name.
 */
Logger MakeLogger(
    const std::string& pChannel,
    const std::string& pFilename);

} // Network::Core

///////////////////////////////////////////////////////////////////////////////////////////////////
// Logging Macro Definitions                                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

#define NETWORK_CORE_LOG_TRACE(LOGGER)      BOOST_LOG_SEV(LOGGER, Network::Core::SeverityLevel::Trace)
#define NETWORK_CORE_LOG_INFO(LOGGER)       BOOST_LOG_SEV(LOGGER, Network::Core::SeverityLevel::Info)
#define NETWORK_CORE_LOG_WARNING(LOGGER)    BOOST_LOG_SEV(LOGGER, Network::Core::SeverityLevel::Warning)
#define NETWORK_CORE_LOG_ERROR(LOGGER)      BOOST_LOG_SEV(LOGGER, Network::Core::SeverityLevel::Error)

#endif // NETWORK_CORE_LOGGING_HPP_INCLUDED