///////////////////////////////////////////////////////////////////////////////////////////////////
// WebsocketSession Implementation                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Settings.hpp>
#include <Network/Core/WebsocketSession.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/ssl/host_name_verification.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <filesystem>

namespace Network::Core
{

const std::string LOG_PREFIX{ "[WebsocketSession] " };
constexpr int WEBSOCKET_SESSION_TIMEOUT_SECONDS{ 30 };

WebsocketSession::WebsocketSession(
    Network::Core::Logger& pLogger,
    boost::asio::io_context& pIOContext,
    const std::string& pHost,
    const std::string& pPort,
    const std::string& pEndpoint) :
    mLogger(pLogger),
    mSSLContext(boost::asio::ssl::context::sslv23),
    mResolver(pIOContext),
    mStream(pIOContext, mSSLContext),
    mHost(pHost),
    mPort(pPort),
    mEndpoint(pEndpoint)
{
    mStream.control_callback(
        [this](boost::beast::websocket::frame_type pKind, boost::beast::string_view pPayload)
        {
            switch (pKind)
            {
            case boost::beast::websocket::frame_type::close:
                NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close frame";
                break;
            case boost::beast::websocket::frame_type::ping:
                NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "ping frame";
                break;
            case boost::beast::websocket::frame_type::pong:
                NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "pong frame";
                break;
            }
        });
}

void WebsocketSession::Start()
{
    // Initialize the SSL context and add an optional certification authority file or directory.
    boost::system::error_code lError;
    mSSLContext.set_default_verify_paths(lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mSSLContext.set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert, lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mSSLContext.set_verify_callback(boost::asio::ssl::host_name_verification(mHost), lError);
    if (lError)
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    const std::string lSSLVerifyFile{ Network::Core::Settings::Instance().GetSSLVerifyFile(mHost) };
    if (std::filesystem::is_regular_file(lSSLVerifyFile))
    {
        mSSLContext.load_verify_file(lSSLVerifyFile, lError);
        if (lError)
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
        }
    }
    else if (!lSSLVerifyFile.empty())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "Bad SSL verify file: " << lSSLVerifyFile;
    }
    else
    {
        const std::string lSSLVerifyPath{ Network::Core::Settings::Instance().GetSSLVerifyPath() };
        if (std::filesystem::is_directory(lSSLVerifyPath))
        {
            mSSLContext.add_verify_path(lSSLVerifyPath, lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
            }
        }
        else if (!lSSLVerifyPath.empty())
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "Bad SSL verify path: " << lSSLVerifyPath;
        }
    }

    // Set SNI Hostname (many hosts need this to handshake successfully).
    if (!SSL_set_tlsext_host_name(mStream.next_layer().native_handle(), mHost.c_str()))
    {
        const boost::beast::error_code lError{ static_cast<int>(::ERR_get_error()), boost::beast::net::error::get_ssl_category() };
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "SSL context error: " << lError.message();
    }

    mResolver.async_resolve(mHost, mPort,
        boost::beast::bind_front_handler(
            &WebsocketSession::OnResolveHandler,
            shared_from_this()));
}

void WebsocketSession::SetIdleTimeout(
    std::uint64_t pTimeout)
{
    mIdleTimeout = pTimeout;
}

void WebsocketSession::OnResolveHandler(
    boost::beast::error_code pError,
    boost::asio::ip::tcp::resolver::results_type pEndpoints)
{
    if (pError)
    {
        OnResolveError(pError.message());
        return;
    }

    boost::beast::get_lowest_layer(mStream).expires_after(std::chrono::seconds(WEBSOCKET_SESSION_TIMEOUT_SECONDS));
    boost::beast::get_lowest_layer(mStream).async_connect(pEndpoints,
        boost::beast::bind_front_handler(
            &WebsocketSession::OnConnectHandler,
            shared_from_this()));
}

void WebsocketSession::OnConnectHandler(
    boost::beast::error_code pError,
    boost::asio::ip::tcp::resolver::results_type::endpoint_type)
{
    if (pError)
    {
        OnConnectError(pError.message());
        return;
    }

    boost::beast::get_lowest_layer(mStream).expires_after(std::chrono::seconds(WEBSOCKET_SESSION_TIMEOUT_SECONDS));
    mStream.next_layer().async_handshake(boost::asio::ssl::stream_base::client,
        boost::beast::bind_front_handler(
            &WebsocketSession::OnSSLHandshakeHandler,
            shared_from_this()));
}

void WebsocketSession::OnSSLHandshakeHandler(
    boost::beast::error_code pError)
{
    if (pError)
    {
        OnSSLHandshakeError(pError.message());
        return;
    }

    // Setup the websocket stream timeout and disable the TCP stream timeout.
    boost::beast::get_lowest_layer(mStream).expires_never();
    if (mIdleTimeout)
    {
        mStream.set_option(boost::beast::websocket::stream_base::timeout{
            .handshake_timeout{ std::chrono::seconds(WEBSOCKET_SESSION_TIMEOUT_SECONDS) },
            .idle_timeout{ std::chrono::seconds(mIdleTimeout.value()) },
            .keep_alive_pings{ false }
        });
    }
    else
    {
        mStream.set_option(boost::beast::websocket::stream_base::timeout{
            .handshake_timeout{ std::chrono::seconds(WEBSOCKET_SESSION_TIMEOUT_SECONDS) },
            .idle_timeout{ boost::beast::websocket::stream_base::none() },
            .keep_alive_pings{ false }
        });
    }

    mStream.async_handshake(mHost, mEndpoint,
        boost::beast::bind_front_handler(
            &WebsocketSession::OnWebsocketHandshakeHandler,
            shared_from_this()));
}

void WebsocketSession::OnWebsocketHandshakeHandler(
    boost::beast::error_code pError)
{
    if (pError)
    {
        OnWebsocketHandshakeError(pError.message());
        return;
    }

    mStream.async_read(mBuffer,
        boost::beast::bind_front_handler(
            &WebsocketSession::OnReadHandler,
            shared_from_this()));
}

void WebsocketSession::OnReadHandler(
    boost::beast::error_code pError,
    std::size_t pBytes)
{
    if (pError == boost::beast::websocket::error::closed)
    {
        // Websocket connection closed by the remote host.
        // The websocket implementation has already closed the stream.
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close: " << mStream.reason();
        return;
    }
    else if (pError == boost::beast::error::timeout)
    {
        // Websocket connection closed after the idle timeout expired.
        // The websocket implementation has already closed the stream.
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close: " << pError.message();
        return;
    }
    else if (pError == boost::asio::ssl::error::stream_truncated)
    {
        // Websocket connection closed by the remote host.
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close: peer may have closed the connection without negociating shutdown";
        return;
    }
    else if (pError)
    {
        OnWebsocketReadError(pError.message());
        mStream.async_close(boost::beast::websocket::close_code::normal,
            boost::beast::bind_front_handler(
                &WebsocketSession::OnCloseHandler,
                shared_from_this()));
        return;
    }

    const boost::asio::mutable_buffer& lBufferData{ mBuffer.data() };
    if (OnWebsocketMessage(std::string{ boost::asio::buffers_begin(lBufferData), boost::asio::buffers_end(lBufferData) }))
    {
        mBuffer.clear();
        mStream.async_read(mBuffer,
            boost::beast::bind_front_handler(
                &WebsocketSession::OnReadHandler,
                shared_from_this()));
    }
    else
    {
        mStream.async_close(boost::beast::websocket::close_code::normal,
            boost::beast::bind_front_handler(
                &WebsocketSession::OnCloseHandler,
                shared_from_this()));
    }
}

void WebsocketSession::OnCloseHandler(
    boost::beast::error_code pError)
{
    if (pError)
    {
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close error: " << pError;
    }
}

} // Network::Core