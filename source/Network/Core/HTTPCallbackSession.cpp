///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTPCallbackSession Implementation                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPCallbackSession.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/bind/bind.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <utility>

namespace Network::Core
{

const std::string LOG_PREFIX{ "[HTTPCallbackSession] " };
constexpr int HTTP_CALLBACK_SESSION_TIMEOUT_SECONDS{ 10 };

HTTPCallbackSession::HTTPCallbackSession(
    Network::Core::Logger& pLogger,
    boost::asio::ip::tcp::socket&& pSocket) :
    mLogger(pLogger),
    mStream(std::move(pSocket))
{}

void HTTPCallbackSession::Start()
{
    mHTTPRequest = {};
    mStream.expires_after(std::chrono::seconds(HTTP_CALLBACK_SESSION_TIMEOUT_SECONDS));
    boost::beast::http::async_read(mStream, mBuffer, mHTTPRequest,
        boost::beast::bind_front_handler(
            &HTTPCallbackSession::OnReadHandler,
            shared_from_this()));
}

void HTTPCallbackSession::OnReadHandler(
    boost::beast::error_code pError,
    std::size_t pBytes)
{
    if (pError)
    {
        OnReadError(pError.message());
        Shutdown();
        return;
    }

    if (mHTTPResponse = OnHTTPRequest(mHTTPRequest))
    {
        boost::beast::http::async_write(mStream, mHTTPResponse.value(),
            boost::beast::bind_front_handler(
                &HTTPCallbackSession::OnWriteHandler,
                shared_from_this()));
    }
    else
    {
        Shutdown();
    }
}

void HTTPCallbackSession::OnWriteHandler(
    boost::beast::error_code pError,
    std::size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "network write error: " << pError.message();
    }
    Shutdown();
}

void HTTPCallbackSession::Shutdown()
{
    boost::beast::error_code lError;
    mStream.socket().shutdown(boost::asio::ip::tcp::socket::shutdown_send, lError);
    if (lError)
    {
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "shutdown error: " << lError.message();
    }
    mStream.socket().close(lError);
    if (lError)
    {
        NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "close error: " << lError.message();
    }
}

} // Network::Core