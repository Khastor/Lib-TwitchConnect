#ifndef NETWORK_CORE_HTTP_SESSION_HPP_INCLUDED
#define NETWORK_CORE_HTTP_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// HTTPSession Interface                                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl/ssl_stream.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <chrono>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <optional>
#include <string>

namespace Network::Core
{

/**
 * @brief Secure HTTP session over SSL.
 */
class HTTPSession : public std::enable_shared_from_this<HTTPSession>
{
public:
    /**
     * @brief Constructor.
     *
     * Derived classes must be allocated dynamically with std::make_shared().
     *
     * @param pLogger The log source.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     */
    HTTPSession(
        Network::Core::Logger& pLogger,
        boost::asio::io_context& pIOContext);

    /**
     * @brief Destructor.
     */
    virtual ~HTTPSession() = default;

    /**
     * @brief Start the session.
     *
     * Handlers will be invoked asynchronously from the execution context thread.
     */
    void Start();

    /**
     * @brief Set the maximum number of retry attempts.
     *
     * The retry limit is optional, and is disabled by default.
     * This method is not thread-safe and must not be called after the session has started.
     *
     * @param pCount The maximum number of retry attempts.
     */
    void SetRetryLimit(
        std::uint64_t pCount);

    /**
     * @brief Set the retry timeout, in seconds.
     *
     * The retry timeout is optional, and is disabled by default.
     * This method is not thread-safe and must not be called after the session has started.
     *
     * @param pTimeout The number of seconds from now after which the session cannot retry anymore.
     */
    void SetRetryTimeout(
        std::uint64_t pTimeout);

protected:
    /**
     * @brief Set the delay in seconds before the next retry.
     *
     * If not set, the delay grows exponentially with the number of retries.
     * To override the default behaviour, the delay must be set before each retry.
     *
     * @param pDelay The delay in seconds before the next retry.
     */
    void SetRetryDelay(
        std::uint64_t pDelay);

    /**
     * @brief Check if the session can retry.
     *
     * The session cannot retry if:
     * - The retry limit has been set and the current retry count exceeds the retry limit.
     * - The retry timeout has been set and has expired.
     *
     * @return True if the session can retry.
     */
    bool HasRetryBudget() const;

private:
    /**
     * @brief Build the HTTP request.
     *
     * The HTTP request header must contain the host field with host name and port information.
     *
     * @return The HTTP request.
     */
    virtual boost::beast::http::request<boost::beast::http::string_body> BuildHTTPRequest() = 0;

    /**
     * @brief Start error handler.
     *
     * This handler is invoked when the session fails to start.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnStartError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief Resolve error handler.
     *
     * This handler is invoked when the session fails to find the remote host address.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnResolveError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief Connect error handler.
     *
     * This handler is invoked when the session fails to connect to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnConnectError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief Handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate a secure connection with the remote host.
     * This can happen if the client cannot verify the SSL certificate chain of the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnHandshakeError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief Write error handler.
     *
     * This handler is invoked when the session fails to send the HTTP request to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnWriteError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the session fails to read the HTTP response from the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    virtual void OnReadError(
        const std::string& pError,
        bool pFatal) = 0;

    /**
     * @brief HTTP response handler.
     *
     * This handler is invoked when the session receives the HTTP response from the remote host.
     * If the session has no retry budget, it will be terminated and ignore the retry request.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    virtual bool OnHTTPResponse(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse) = 0;

    /**
     * @brief Start handler.
     *
     * This handler is invoked when the session starts.
     * Invokes the start error handler on error.
     *
     * @param pError The error details.
     */
    void OnStartHandler(
        const boost::system::error_code& pError);

    /**
     * @brief Resolve handler.
     *
     * This handler is invoked when the session resolve completes.
     * Invokes the resolve error handler on error.
     *
     * @param pError The error details.
     * @param pEndpoints The resolved endpoints.
     */
    void OnResolveHandler(
        boost::beast::error_code pError,
        boost::asio::ip::tcp::resolver::results_type pEndpoints);

    /**
     * @brief Connect handler.
     *
     * This handler is invoked when the session connect completes.
     * Invokes the connect error handler on error.
     *
     * @param pError The error details.
     */
    void OnConnectHandler(
        boost::beast::error_code pError,
        boost::asio::ip::tcp::resolver::results_type::endpoint_type);

    /**
     * @brief Handshake handler.
     *
     * This handler is invoked when the session handshake completes.
     * Invokes the handshake error handler on error.
     *
     * @param pError The error details.
     */
    void OnHandshakeHandler(
        boost::beast::error_code pError);

    /**
     * @brief Write handler.
     *
     * This handler is invoked when the session write completes.
     * Invokes the write error handler on error.
     *
     * @param pError The error details.
     * @param pBytes The number of bytes written.
     */
    void OnWriteHandler(
        boost::beast::error_code pError,
        std::size_t pBytes);

    /**
     * @brief Read handler.
     *
     * This handler is invoked when the session read completes.
     * Invokes the read error handler on error, otherwise invokes the HTTP response handler.
     *
     * @param pError The error details.
     * @param pBytes The number of bytes read.
     */
    void OnReadHandler(
        boost::beast::error_code pError,
        std::size_t pBytes);

    /**
     * @brief Shutdown handler.
     *
     * This handler is invoked when the session shutdown completes.
     *
     * @param pRetry True if the session must retry after shutdown.
     * @param pError The error details.
     */
    void OnShutdownHandler(
        bool pRetry,
        boost::beast::error_code pError);

    /**
     * @brief Shutdown the session.
     *
     * @param pRetry True if the session should retry after shutdown.
     * @return True if the shutdown is definitive, false if the session will retry after shutdown.
     */
    bool Shutdown(
        bool pRetry);

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The IO context.
     */
    boost::asio::io_context& mIOContext;

    /**
     * @brief The SSL context.
     */
    boost::asio::ssl::context mSSLContext;

    /**
     * @brief The retry timer.
     *
     * The retry timer triggers the next retry attempt after waiting for the retry delay to expire.
     */
    boost::asio::steady_timer mRetryTimer;

    /**
     * @brief The delay before the next retry attempt, in seconds.
     */
    std::uint64_t mRetryDelay{ 0ULL };

    /**
     * @brief The current number of retry attempts.
     */
    std::uint64_t mRetryCount{ 0ULL };

    /**
     * @brief The maximum number of retry attempts.
     */
    std::optional<std::uint64_t> mRetryLimit;

    /**
     * @brief The retry timeout.
     *
     * The time point after which the session cannot retry anymore.
     */
    std::optional<std::chrono::steady_clock::time_point> mRetryTimeout;

    /**
     * @brief The TCP resolver to find the address of the remote host.
     */
    boost::asio::ip::tcp::resolver mResolver;

    /**
     * @brief The SSL stream to communicate with the remote host.
     */
    std::unique_ptr<boost::beast::ssl_stream<boost::beast::tcp_stream>> mStream;

    /**
     * @brief The HTTP request.
     *
     * The HTTP request header must contain the host field, with host name and port information.
     */
    boost::beast::http::request<boost::beast::http::string_body> mHTTPRequest;

    /**
     * @brief The remote host name.
     *
     * The remote host name is derived from the HTTP request header's host field.
     */
    std::string mHost;

    /**
     * @brief The remote host port.
     *
     * The remote host port is derived from the HTTP request header's host field.
     */
    std::string mPort;

    /**
     * @brief The HTTP response.
     */
    boost::beast::http::response<boost::beast::http::string_body> mHTTPResponse;

    /**
     * @brief The HTTP response buffer.
     */
    boost::beast::flat_buffer mBuffer;
};

} // Network::Core

#endif // NETWORK_CORE_HTTP_SESSION_HPP_INCLUDED