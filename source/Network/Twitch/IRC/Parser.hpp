#ifndef NETWORK_TWITCH_IRC_PARSER_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_PARSER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Parser Interface                                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/Model/Color.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <optional>
#include <string>
#include <vector>

namespace Network::Twitch::IRC
{

/**
 * @brief Twitch IRC message parser.
 */
class Parser
{
public:
    virtual ~Parser() = default;

    /**
     * @brief Parse and dispatch a Twitch IRC message.
     *
     * @param pBytes The Twitch IRC message bytes.
     * @return True if the message was successfully parsed and dispatched.
     */
    bool Dispatch(
        const std::string& pBytes);

protected:
    /**
     * @brief Twitch IRC CAP ACK subcommand.
     *
     * @see https://dev.twitch.tv/docs/irc/capabilities/
     */
    struct COMMAND_CAP_ACK
    {
        std::string Host;
        bool HasCommandsCap;                                                    // granted twitch.tv/commands
        bool HasMembershipCap;                                                  // granted twitch.tv/membership
        bool HasTagsCap;                                                        // granted twitch.tv/tags
    };

    /**
     * @brief Twitch IRC CAP ACK subcommand handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData);

    /**
     * @brief Twitch IRC CAP NAK subcommand.
     *
     * @see https://dev.twitch.tv/docs/irc/capabilities/
     */
    struct COMMAND_CAP_NAK
    {
        std::string Host;
        bool HasCommandsCap;                                                    // denied twitch.tv/commands
        bool HasMembershipCap;                                                  // denied twitch.tv/membership
        bool HasTagsCap;                                                        // denied twitch.tv/tags
    };

    /**
     * @brief Twitch IRC CAP NAK subcommand handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData);

    /**
     * @brief Twitch IRC CLEARCHAT command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#clearchat
     * @see https://dev.twitch.tv/docs/irc/tags/#clearchat-tags
     */
    struct COMMAND_CLEARCHAT
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> ChannelId;                                   // room-id tag
        std::optional<std::string> UserId;                                      // target-user-id tag
        std::optional<std::string> UserLogin;                                   // user
        std::optional<std::int64_t> Timeout;                                    // ban-duration tag
        std::optional<std::int64_t> Timestamp;                                  // tmi-sent-ts tag
    };

    /**
     * @brief Twitch IRC CLEARCHAT command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData);

    /**
     * @brief Twitch IRC CLEARMSG command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#clearmsg
     * @see https://dev.twitch.tv/docs/irc/tags/#clearmsg-tags
     */
    struct COMMAND_CLEARMSG
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> ChannelId;                                   // room-id tag
        std::optional<std::string> UserLogin;                                   // login tag
        std::string Message;                                                    // message
        std::optional<std::string> MessageId;                                   // target-msg-id tag
        std::optional<std::int64_t> Timestamp;                                  // tmi-sent-ts tag
    };

    /**
     * @brief Twitch IRC CLEARMSG command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData);

    /**
     * @brief Twitch IRC GLOBALUSERSTATE command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#globaluserstate
     * @see https://dev.twitch.tv/docs/irc/tags/#globaluserstate-tags
     */
    struct COMMAND_GLOBALUSERSTATE
    {
        std::string Host;
        std::optional<std::string> UserId;                                      // user-id tag
        std::optional<std::string> UserName;                                    // display-name tag
        std::optional<std::string> UserType;                                    // user-type tag
        std::optional<Network::Twitch::Model::Color> UserColor;                 // color tag
    };

    /**
     * @brief Twitch IRC GLOBALUSERSTATE command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData);

    /**
     * @brief Twitch IRC HOSTTARGET command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#hosttarget
     */
    struct COMMAND_HOSTTARGET
    {
        std::string Host;
        std::string HostedChannel;                                              // channel or empty
        std::string HostingChannel;                                             // hosting-channel
        std::int64_t ViewerCount;                                               // number-of-viewers
    };

    /**
     * @brief Twitch IRC HOSTTARGET command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData);

    /**
     * @brief Twitch IRC JOIN command.
     *
     * @see https://dev.twitch.tv/docs/irc/
     * @see https://dev.twitch.tv/docs/irc/join-chat-room/
     */
    struct COMMAND_JOIN
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::string UserLogin;                                                  // user
    };

    /**
     * @brief Twitch IRC JOIN command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData);

    /**
     * @brief Twitch IRC NOTICE command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#notice
     * @see https://dev.twitch.tv/docs/irc/tags/#notice-tags
     */
    struct COMMAND_NOTICE
    {
        std::string Host;
        std::string Channel;                                                    // channel or empty
        std::string Message;                                                    // message
        std::optional<std::string> NoticeType;                                  // msg-id tag
        std::optional<std::string> UserId;                                      // target-user-id tag
    };

    /**
     * @brief Twitch IRC NOTICE command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData);

    /**
     * @brief Twitch IRC PART command.
     *
     * @see https://dev.twitch.tv/docs/irc/
     */
    struct COMMAND_PART
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::string UserLogin;                                                  // user
    };

    /**
     * @brief Twitch IRC PART command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData);

    /**
     * @brief Twitch IRC PING command.
     *
     * @see https://dev.twitch.tv/docs/irc/#keepalive-messages
     */
    struct COMMAND_PING
    {
        std::string Payload;
    };

    /**
     * @brief Twitch IRC PING command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData);

    /**
     * @brief Twitch IRC PRIVMSG command.
     *
     * @see https://dev.twitch.tv/docs/irc/
     * @see https://dev.twitch.tv/docs/irc/send-receive-messages/
     * @see https://dev.twitch.tv/docs/irc/tags/#privmsg-tags
     */
    struct COMMAND_PRIVMSG
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> ChannelId;                                   // room-id tag
        std::optional<std::string> UserId;                                      // user-id tag
        std::string UserLogin;                                                  // user
        std::optional<std::string> UserName;                                    // display-name tag
        std::optional<std::string> UserType;                                    // user-type tag
        std::optional<bool> UserIsModerator;                                    // mod tag
        std::optional<bool> UserIsSubscriber;                                   // subscriber tag
        std::optional<bool> UserIsVIP;                                          // vip tag
        std::optional<Network::Twitch::Model::Color> UserColor;                 // color tag
        std::string Message;                                                    // message
        std::optional<std::string> MessageId;                                   // id tag
        std::optional<std::int64_t> Timestamp;                                  // tmi-sent-ts tag
        std::optional<std::string> ReplyParentUserId;                           // reply-parent-user-id tag
        std::optional<std::string> ReplyParentUserLogin;                        // reply-parent-user-login tag
        std::optional<std::string> ReplyParentUserName;                         // reply-parent-display-name tag
        std::optional<std::string> ReplyParentMessage;                          // reply-parent-msg-body tag
        std::optional<std::string> ReplyParentMessageId;                        // reply-parent-msg-id tag
        std::optional<std::int64_t> Bits;                                       // bits tag
    };

    /**
     * @brief Twitch IRC PRIVMSG command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData);

    /**
     * @brief Twitch IRC RECONNECT command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#reconnect
     */
    struct COMMAND_RECONNECT
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RECONNECT command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData);

    /**
     * @brief Twitch IRC ROOMSTATE command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#roomstate
     * @see https://dev.twitch.tv/docs/irc/tags/#roomstate-tags
     */
    struct COMMAND_ROOMSTATE
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> ChannelId;                                   // room-id tag
        std::optional<bool> EmoteOnly;                                          // emote-only tag
        std::optional<std::int64_t> FollowersOnly;                              // followers-only tag
        std::optional<bool> SubscribersOnly;                                    // subs-only tag
        std::optional<bool> UniqueMessages;                                     // r9k tag
        std::optional<std::int64_t> MessageCooldown;                            // slow tag
    };

    /**
     * @brief Twitch IRC ROOMSTATE command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData);

    /**
     * @brief Twitch IRC USERNOTICE command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#usernotice
     * @see https://dev.twitch.tv/docs/irc/tags/#usernotice-tags
     */
    struct COMMAND_USERNOTICE
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> ChannelId;                                   // room-id tag
        std::optional<std::string> UserId;                                      // user-id tag
        std::optional<std::string> UserLogin;                                   // login tag
        std::optional<std::string> UserName;                                    // display-name tag
        std::optional<std::string> UserType;                                    // user-type tag
        std::optional<bool> UserIsModerator;                                    // mod tag
        std::optional<bool> UserIsSubscriber;                                   // subscriber tag
        std::optional<Network::Twitch::Model::Color> UserColor;                 // color tag
        std::optional<std::string> Message;                                     // message
        std::optional<std::string> MessageId;                                   // id tag
        std::optional<std::string> NoticeType;                                  // msg-id tag
        std::optional<std::string> NoticeMessage;                               // system-msg tag
        std::optional<std::int64_t> Timestamp;                                  // tmi-sent-ts tag
        std::optional<std::int64_t> MessageParamCumulativeMonths;               // msg-param-cumulative-months tag
        std::optional<std::string> MessageParamUserName;                        // msg-param-displayName tag
        std::optional<std::string> MessageParamUserLogin;                       // msg-param-login tag
        std::optional<std::int64_t> MessageParamMonths;                         // msg-param-months tag
        std::optional<std::int64_t> MessageParamPromoGiftTotal;                 // msg-param-promo-gift-total tag
        std::optional<std::string> MessageParamPromoName;                       // msg-param-promo-name tag
        std::optional<std::string> MessageParamRecipientUserId;                 // msg-param-recipient-id tag
        std::optional<std::string> MessageParamRecipientUserLogin;              // msg-param-recipient-user-name tag
        std::optional<std::string> MessageParamRecipientUserName;               // msg-param-recipient-display-name tag
        std::optional<std::string> MessageParamSenderUserLogin;                 // msg-param-sender-login tag
        std::optional<std::string> MessageParamSenderUserName;                  // msg-param-sender-name tag
        std::optional<bool> MessageParamShouldShareStreak;                      // msg-param-should-share-streak tag
        std::optional<std::int64_t> MessageParamStreakMonths;                   // msg-param-streak-months tag
        std::optional<std::string> MessageParamSubPlan;                         // msg-param-sub-plan tag
        std::optional<std::string> MessageParamSubPlanName;                     // msg-param-sub-plan-name tag
        std::optional<std::int64_t> MessageParamViewerCount;                    // msg-param-viewerCount tag
        std::optional<std::string> MessageParamRitualName;                      // msg-param-ritual-name tag
        std::optional<std::int64_t> MessageParamThreshold;                      // msg-param-threshold tag
        std::optional<std::int64_t> MessageParamGiftMonths;                     // msg-param-gift-months tag
    };

    /**
     * @brief Twitch IRC USERNOTICE command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData);

    /**
     * @brief Twitch IRC USERSTATE command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#userstate
     * @see https://dev.twitch.tv/docs/irc/tags/#userstate-tags
     */
    struct COMMAND_USERSTATE
    {
        std::string Host;
        std::string Channel;                                                    // channel
        std::optional<std::string> UserName;                                    // display-name tag
        std::optional<Network::Twitch::Model::Color> UserColor;                 // color tag
        std::optional<std::string> UserType;                                    // user-type tag
        std::optional<bool> UserIsModerator;                                    // mod tag
        std::optional<bool> UserIsSubscriber;                                   // subscriber tag
        std::optional<std::string> MessageId;                                   // id tag
    };

    /**
     * @brief Twitch IRC USERSTATE command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData);

    /**
     * @brief Twitch IRC WHISPER command.
     *
     * @see https://dev.twitch.tv/docs/irc/commands/#whisper
     * @see https://dev.twitch.tv/docs/irc/tags/#whisper-tags
     */
    struct COMMAND_WHISPER
    {
        std::string Host;
        std::optional<std::string> UserId;                                      // user-id tag
        std::string UserLogin;                                                  // from-user
        std::optional<std::string> UserName;                                    // display-name tag
        std::optional<std::string> UserType;                                    // user-type tag
        std::optional<Network::Twitch::Model::Color> UserColor;                 // color tag
        std::string Message;                                                    // message
        std::optional<std::string> MessageId;                                   // message-id tag
        std::optional<std::string> ThreadId;                                    // thread-id tag
    };

    /**
     * @brief Twitch IRC WHISPER command handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData);

    /**
     * @brief Twitch IRC RPL_WELCOME numeric reply.
     *
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_WELCOME
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_WELCOME numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData);

    /**
     * @brief Twitch IRC RPL_YOURHOST numeric reply.
     *
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_YOURHOST
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_YOURHOST numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_YOURHOST& pMessageData);

    /**
     * @brief Twitch IRC RPL_CREATED numeric reply.
     *
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_CREATED
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_CREATED numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_CREATED& pMessageData);

    /**
     * @brief Twitch IRC RPL_MYINFO numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/authenticate-bot/
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_MYINFO
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_MYINFO numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MYINFO& pMessageData);

    /**
     * @brief Twitch IRC RPL_MOTDSTART numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/authenticate-bot/
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_MOTDSTART
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_MOTDSTART numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTDSTART& pMessageData);

    /**
     * @brief Twitch IRC RPL_MOTD numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/authenticate-bot/
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_MOTD
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_MOTD numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTD& pMessageData);

    /**
     * @brief Twitch IRC RPL_ENDOFMOTD numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/authenticate-bot/
     * @see https://www.rfc-editor.org/rfc/rfc2812
     */
    struct NUMERIC_RPL_ENDOFMOTD
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_ENDOFMOTD numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFMOTD& pMessageData);

    /**
     * @brief Twitch IRC RPL_NAMREPLY numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/join-chat-room/
     * @see https://www.rfc-editor.org/rfc/rfc2812#section-3.2.5
     */
    struct NUMERIC_RPL_NAMREPLY
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_NAMREPLY numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData);

    /**
     * @brief Twitch IRC RPL_ENDOFNAMES numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/join-chat-room/
     * @see https://www.rfc-editor.org/rfc/rfc2812#section-3.2.5
     */
    struct NUMERIC_RPL_ENDOFNAMES
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC RPL_ENDOFNAMES numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFNAMES& pMessageData);

    /**
     * @brief Twitch IRC ERR_UNKNOWNCOMMAND numeric reply.
     *
     * @see https://dev.twitch.tv/docs/irc/example-parser/
     * @see https://www.rfc-editor.org/rfc/rfc1459#section-6.1
     */
    struct NUMERIC_ERR_UNKNOWNCOMMAND
    {
        std::string Host;
    };

    /**
     * @brief Twitch IRC ERR_UNKNOWNCOMMAND numeric reply handler.
     *
     * @param pMessageData The parsed message data.
     */
    virtual void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_ERR_UNKNOWNCOMMAND& pMessageData);
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_PARSER_HPP_INCLUDED