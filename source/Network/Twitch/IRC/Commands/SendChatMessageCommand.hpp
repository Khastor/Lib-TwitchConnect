#ifndef NETWORK_TWITCH_IRC_COMMANDS_SEND_CHAT_MESSAGE_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_COMMANDS_SEND_CHAT_MESSAGE_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC SendChatMessageCommand Interface                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Command.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::IRC::Commands
{

/**
 * @brief Twitch IRC Send Chat Message command.
 *
 * Publish this command to call for the Twitch IRC service to send a message in the broadcaster's Twitch IRC channel.
 */
struct SendChatMessageCommand : public Network::Twitch::IRC::Command
{
    /**
     * @brief Constructor.
     */
    SendChatMessageCommand() = default;

    /**
     * @brief Constructor.
     *
     * @param pMessage The message content.
     * @param pParentId The id of the message to reply to, otherwise empty.
    */
    SendChatMessageCommand(
        const std::string& pMessage,
        const std::string& pParentId = std::string{});

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    void Dispatch(
        Network::Twitch::IRC::CommandHandler& pHandler) const override;

    /**
     * @brief The message content.
     */
    std::string Message;

    /**
     * @brief The id of the message to reply to, otherwise empty.
     */
    std::string ParentId;
};

} // Network::Twitch::IRC::Commands

#endif // NETWORK_TWITCH_IRC_COMMANDS_SEND_CHAT_MESSAGE_COMMAND_HPP_INCLUDED