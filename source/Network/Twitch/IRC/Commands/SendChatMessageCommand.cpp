///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC SendChatMessageCommand Implementation                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/CommandHandler.hpp>
#include <Network/Twitch/IRC/Commands/SendChatMessageCommand.hpp>

namespace Network::Twitch::IRC::Commands
{

SendChatMessageCommand::SendChatMessageCommand(
    const std::string& pMessage,
    const std::string& pParentId) :
    Message(pMessage),
    ParentId(pParentId)
{}

void SendChatMessageCommand::Dispatch(
    Network::Twitch::IRC::CommandHandler& pHandler) const
{
    pHandler.OnCommand(*this);
}

} // Network::Twitch::IRC::Commands