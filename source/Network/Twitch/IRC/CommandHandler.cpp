///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC CommandHandler Implementation                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Command.hpp>
#include <Network/Twitch/IRC/CommandHandler.hpp>

namespace Network::Twitch::IRC
{

void CommandHandler::OnMessage(
    const Network::Twitch::IRC::Command& pCommand)
{
    pCommand.Dispatch(*this);
}

void CommandHandler::OnCommand(
    const Network::Twitch::IRC::Commands::SendChatMessageCommand&)
{}

} // Network::Twitch::IRC