#ifndef NETWORK_TWITCH_IRC_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Command Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::IRC
{

class CommandHandler;

/**
 * @brief Base class in the hierarchy of Twitch IRC commands.
 */
struct Command : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Command() = default;

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    virtual void Dispatch(
        CommandHandler& pHandler) const = 0;
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_COMMAND_HPP_INCLUDED