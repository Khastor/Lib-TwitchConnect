///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Parser Implementation                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Parser.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cctype>
#include <cstddef>
#include <iostream>
#include <sstream>
#include <unordered_map>
#include <utility>

namespace Network::Twitch::IRC
{

namespace
{

const std::string COMMAND_CAP_ACK_NAME{ "CAP * ACK" };
const std::string COMMAND_CAP_NAK_NAME{ "CAP * NAK" };
const std::string COMMAND_CLEARCHAT_NAME{ "CLEARCHAT" };
const std::string COMMAND_CLEARMSG_NAME{ "CLEARMSG" };
const std::string COMMAND_GLOBALUSERSTATE_NAME{ "GLOBALUSERSTATE" };
const std::string COMMAND_HOSTTARGET_NAME{ "HOSTTARGET" };
const std::string COMMAND_JOIN_NAME{ "JOIN" };
const std::string COMMAND_NOTICE_NAME{ "NOTICE" };
const std::string COMMAND_PART_NAME{ "PART" };
const std::string COMMAND_PING_NAME{ "PING" };
const std::string COMMAND_PRIVMSG_NAME{ "PRIVMSG" };
const std::string COMMAND_RECONNECT_NAME{ "RECONNECT" };
const std::string COMMAND_ROOMSTATE_NAME{ "ROOMSTATE" };
const std::string COMMAND_USERNOTICE_NAME{ "USERNOTICE" };
const std::string COMMAND_USERSTATE_NAME{ "USERSTATE" };
const std::string COMMAND_WHISPER_NAME{ "WHISPER" };
const std::string NUMERIC_RPL_WELCOME_CODE{ "001" };
const std::string NUMERIC_RPL_YOURHOST_CODE{ "002" };
const std::string NUMERIC_RPL_CREATED_CODE{ "003" };
const std::string NUMERIC_RPL_MYINFO_CODE{ "004" };
const std::string NUMERIC_RPL_MOTDSTART_CODE{ "375" };
const std::string NUMERIC_RPL_MOTD_CODE{ "372" };
const std::string NUMERIC_RPL_ENDOFMOTD_CODE{ "376" };
const std::string NUMERIC_RPL_NAMREPLY_CODE{ "353" };
const std::string NUMERIC_RPL_ENDOFNAMES_CODE{ "366" };
const std::string NUMERIC_ERR_UNKNOWNCOMMAND_CODE{ "421" };

const std::string CAPABILITY_COMMANDS{ "twitch.tv/commands" };
const std::string CAPABILITY_MEMBERSHIP{ "twitch.tv/membership" };
const std::string CAPABILITY_TAGS{ "twitch.tv/tags" };

bool ReadNextMarker(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex);

bool ReadNextString(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex,
    std::string& pValue);

bool ReadNextInteger(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex,
    std::int64_t& pValue);

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<std::string>& pValue,
    bool pDecodeEscapedCharacters = false);

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<std::int64_t>& pValue);

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<bool>& pValue);

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<Network::Twitch::Model::Color>& pValue);

bool DecodeEscapedCharacters(
    std::string& pValue);

} // anonymous namespace

bool Parser::Dispatch(
    const std::string& pBytes)
{
    std::size_t lIndex{};

    std::unordered_map<std::string, std::string> lTags;
    if (ReadNextMarker("@", pBytes, lIndex))
    {
        std::string lMessageTags, lMessageTag;
        if (!ReadNextString(" ", pBytes, lIndex, lMessageTags))
        {
            return false;
        }

        std::stringstream lMessageTagsStream(lMessageTags);
        while (std::getline(lMessageTagsStream, lMessageTag, ';'))
        {
            std::size_t lMessageTagIndex{};
            std::string lMessageTagKey;
            if (!ReadNextString("=", lMessageTag, lMessageTagIndex, lMessageTagKey))
            {
                return false;
            }
            lTags[lMessageTagKey] = lMessageTag.substr(lMessageTagIndex);
        }
    }

    std::string lMessagePrefix, lMessagePrefixHost, lMessagePrefixNick;
    if (ReadNextMarker(":", pBytes, lIndex))
    {
        if (!ReadNextString(" ", pBytes, lIndex, lMessagePrefix))
        {
            return false;
        }

        std::size_t lMessagePrefixIndex{};
        if (ReadNextString("!", lMessagePrefix, lMessagePrefixIndex, lMessagePrefixNick))
        {
            if (!ReadNextString("@", lMessagePrefix, lMessagePrefixIndex, lMessagePrefixNick))
            {
                return false;
            }
            if (!ReadNextString(".", lMessagePrefix, lMessagePrefixIndex, lMessagePrefixNick))
            {
                return false;
            }
            lMessagePrefixHost = lMessagePrefix.substr(lMessagePrefixIndex);
        }
        else
        {
            lMessagePrefixHost = lMessagePrefix;
        }
    }

    if (ReadNextMarker(COMMAND_PING_NAME, pBytes, lIndex))
    {
        COMMAND_PING lMessageData{};
        if (!ReadNextMarker(" ", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Payload))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_CAP_ACK_NAME, pBytes, lIndex))
    {
        COMMAND_CAP_ACK lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" :", pBytes, lIndex))
        {
            return false;
        }
        std::string lCapabilities;
        if (!ReadNextString("\r\n", pBytes, lIndex, lCapabilities))
        {
            return false;
        }
        if (lCapabilities.find(CAPABILITY_COMMANDS) != std::string::npos)
        {
            lMessageData.HasCommandsCap = true;
        }
        if (lCapabilities.find(CAPABILITY_MEMBERSHIP) != std::string::npos)
        {
            lMessageData.HasMembershipCap = true;
        }
        if (lCapabilities.find(CAPABILITY_TAGS) != std::string::npos)
        {
            lMessageData.HasTagsCap = true;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_CAP_NAK_NAME, pBytes, lIndex))
    {
        COMMAND_CAP_NAK lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" :", pBytes, lIndex))
        {
            return false;
        }
        std::string lCapabilities;
        if (!ReadNextString("\r\n", pBytes, lIndex, lCapabilities))
        {
            return false;
        }
        if (lCapabilities.find(CAPABILITY_COMMANDS) != std::string::npos)
        {
            lMessageData.HasCommandsCap = true;
        }
        if (lCapabilities.find(CAPABILITY_MEMBERSHIP) != std::string::npos)
        {
            lMessageData.HasMembershipCap = true;
        }
        if (lCapabilities.find(CAPABILITY_TAGS) != std::string::npos)
        {
            lMessageData.HasTagsCap = true;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_CLEARCHAT_NAME, pBytes, lIndex))
    {
        COMMAND_CLEARCHAT lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.Channel))
        {
            if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
            {
                return false;
            }
        }
        else
        {
            std::string lUserLogin;
            if (!ReadNextString("\r\n", pBytes, lIndex, lUserLogin))
            {
                return false;
            }
            lMessageData.UserLogin = std::move(lUserLogin);
        }
        if (!ReadTagValue(lTags, "room-id", lMessageData.ChannelId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "target-user-id", lMessageData.UserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "ban-duration", lMessageData.Timeout))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "tmi-sent-ts", lMessageData.Timestamp))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_CLEARMSG_NAME, pBytes, lIndex))
    {
        COMMAND_CLEARMSG lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Message))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "room-id", lMessageData.ChannelId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "login", lMessageData.UserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "target-msg-id", lMessageData.MessageId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "tmi-sent-ts", lMessageData.Timestamp))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_GLOBALUSERSTATE_NAME, pBytes, lIndex))
    {
        COMMAND_GLOBALUSERSTATE lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadTagValue(lTags, "user-id", lMessageData.UserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "display-name", lMessageData.UserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-type", lMessageData.UserType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "color", lMessageData.UserColor))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_HOSTTARGET_NAME, pBytes, lIndex))
    {
        COMMAND_HOSTTARGET lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.HostingChannel))
        {
            return false;
        }
        if (!ReadNextString(" ", pBytes, lIndex, lMessageData.HostedChannel))
        {
            return false;
        }
        if (!ReadNextInteger("\r\n", pBytes, lIndex, lMessageData.ViewerCount))
        {
            return false;
        }
        if (lMessageData.HostedChannel == "-")
        {
            lMessageData.HostedChannel.clear();
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_JOIN_NAME, pBytes, lIndex))
    {
        COMMAND_JOIN lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        lMessageData.UserLogin = lMessagePrefixNick;
        if (lMessageData.UserLogin.empty())
        {
            return false;
        }
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_NOTICE_NAME, pBytes, lIndex))
    {
        COMMAND_NOTICE lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            if (!ReadNextMarker(" * :", pBytes, lIndex))
            {
                return false;
            }
            if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Message))
            {
                return false;
            }
        }
        else
        {
            if (!ReadNextString(" :", pBytes, lIndex, lMessageData.Channel))
            {
                return false;
            }
            if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Message))
            {
                return false;
            }
            if (!ReadTagValue(lTags, "msg-id", lMessageData.NoticeType))
            {
                return false;
            }
            if (!ReadTagValue(lTags, "target-user-id", lMessageData.UserId))
            {
                return false;
            }
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_PART_NAME, pBytes, lIndex))
    {
        COMMAND_PART lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        std::size_t lPrefixIndex{};
        if (!ReadNextMarker(":", pBytes, lPrefixIndex))
        {
            return false;
        }
        if (!ReadNextString("!", pBytes, lPrefixIndex, lMessageData.UserLogin))
        {
            return false;
        }
        if (!ReadNextString("@", pBytes, lPrefixIndex, lMessageData.UserLogin))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_PRIVMSG_NAME, pBytes, lIndex))
    {
        COMMAND_PRIVMSG lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        lMessageData.UserLogin = lMessagePrefixNick;
        if (lMessageData.UserLogin.empty())
        {
            return false;
        }
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Message))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "room-id", lMessageData.ChannelId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-id", lMessageData.UserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "display-name", lMessageData.UserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-type", lMessageData.UserType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "mod", lMessageData.UserIsModerator))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "subscriber", lMessageData.UserIsSubscriber))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "vip", lMessageData.UserIsVIP))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "color", lMessageData.UserColor))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "id", lMessageData.MessageId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "tmi-sent-ts", lMessageData.Timestamp))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "reply-parent-user-id", lMessageData.ReplyParentUserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "reply-parent-user-login", lMessageData.ReplyParentUserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "reply-parent-display-name", lMessageData.ReplyParentUserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "reply-parent-msg-body", lMessageData.ReplyParentMessage))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "reply-parent-msg-id", lMessageData.ReplyParentMessageId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "bits", lMessageData.Bits))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_RECONNECT_NAME, pBytes, lIndex))
    {
        COMMAND_RECONNECT lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_ROOMSTATE_NAME, pBytes, lIndex))
    {
        COMMAND_ROOMSTATE lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "room-id", lMessageData.ChannelId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "emote-only", lMessageData.EmoteOnly))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "followers-only", lMessageData.FollowersOnly))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "subs-only", lMessageData.SubscribersOnly))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "r9k", lMessageData.UniqueMessages))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "slow", lMessageData.MessageCooldown))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_USERNOTICE_NAME, pBytes, lIndex))
    {
        COMMAND_USERNOTICE lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.Channel))
        {
            if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
            {
                return false;
            }
        }
        else
        {
            std::string lMessage;
            if (!ReadNextString("\r\n", pBytes, lIndex, lMessage))
            {
                return false;
            }
            lMessageData.Message = std::move(lMessage);
        }
        if (!ReadTagValue(lTags, "room-id", lMessageData.ChannelId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-id", lMessageData.UserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "login", lMessageData.UserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "display-name", lMessageData.UserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-type", lMessageData.UserType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "mod", lMessageData.UserIsModerator))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "subscriber", lMessageData.UserIsSubscriber))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "color", lMessageData.UserColor))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "id", lMessageData.MessageId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-id", lMessageData.NoticeType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "system-msg", lMessageData.NoticeMessage, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "tmi-sent-ts", lMessageData.Timestamp))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-cumulative-months", lMessageData.MessageParamCumulativeMonths))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-displayName", lMessageData.MessageParamUserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-login", lMessageData.MessageParamUserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-months", lMessageData.MessageParamMonths))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-promo-gift-total", lMessageData.MessageParamPromoGiftTotal))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-promo-name", lMessageData.MessageParamPromoName))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-recipient-id", lMessageData.MessageParamRecipientUserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-recipient-user-name", lMessageData.MessageParamRecipientUserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-recipient-display-name", lMessageData.MessageParamRecipientUserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-sender-login", lMessageData.MessageParamSenderUserLogin))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-sender-name", lMessageData.MessageParamSenderUserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-should-share-streak", lMessageData.MessageParamShouldShareStreak))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-streak-months", lMessageData.MessageParamStreakMonths))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-sub-plan", lMessageData.MessageParamSubPlan))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-sub-plan-name", lMessageData.MessageParamSubPlanName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-viewerCount", lMessageData.MessageParamViewerCount))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-ritual-name", lMessageData.MessageParamRitualName))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-threshold", lMessageData.MessageParamThreshold))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "msg-param-gift-months", lMessageData.MessageParamGiftMonths))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_USERSTATE_NAME, pBytes, lIndex))
    {
        COMMAND_USERSTATE lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" #", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Channel))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "display-name", lMessageData.UserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "color", lMessageData.UserColor))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-type", lMessageData.UserType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "mod", lMessageData.UserIsModerator))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "subscriber", lMessageData.UserIsSubscriber))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "id", lMessageData.MessageId))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(COMMAND_WHISPER_NAME, pBytes, lIndex))
    {
        COMMAND_WHISPER lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        if (!ReadNextMarker(" ", pBytes, lIndex))
        {
            return false;
        }
        if (!ReadNextString(" :", pBytes, lIndex, lMessageData.UserLogin))
        {
            return false;
        }
        if (!ReadNextString("\r\n", pBytes, lIndex, lMessageData.Message))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-id", lMessageData.UserId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "display-name", lMessageData.UserName, true))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "user-type", lMessageData.UserType))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "color", lMessageData.UserColor))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "message-id", lMessageData.MessageId))
        {
            return false;
        }
        if (!ReadTagValue(lTags, "thread-id", lMessageData.ThreadId))
        {
            return false;
        }
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_WELCOME_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_WELCOME lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_YOURHOST_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_YOURHOST lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_CREATED_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_CREATED lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_MYINFO_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_MYINFO lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_MOTDSTART_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_MOTDSTART lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_MOTD_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_MOTD lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_ENDOFMOTD_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_ENDOFMOTD lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_NAMREPLY_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_NAMREPLY lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_RPL_ENDOFNAMES_CODE, pBytes, lIndex))
    {
        NUMERIC_RPL_ENDOFNAMES lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else if (ReadNextMarker(NUMERIC_ERR_UNKNOWNCOMMAND_CODE, pBytes, lIndex))
    {
        NUMERIC_ERR_UNKNOWNCOMMAND lMessageData{};
        lMessageData.Host = lMessagePrefixHost;
        OnParseMessage(lMessageData);
    }
    else
    {
        return false;
    }
    return true;
}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_YOURHOST& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_CREATED& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MYINFO& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTDSTART& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTD& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFMOTD& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFNAMES& pMessageData) {}

void Parser::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_ERR_UNKNOWNCOMMAND& pMessageData) {}

namespace
{

bool ReadNextMarker(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex)
{
    if (pBytes.find(pMarker, pIndex) == pIndex)
    {
        pIndex += pMarker.size();
        return true;
    }
    return false;
}

bool ReadNextString(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex,
    std::string& pValue)
{
    const std::size_t lEndIndex{ pBytes.find(pMarker, pIndex) };
    if (lEndIndex != std::string::npos)
    {
        pValue = pBytes.substr(pIndex, lEndIndex - pIndex);
        pIndex = lEndIndex + pMarker.size();
        return true;
    }
    pValue.clear();
    return false;
}

bool ReadNextInteger(
    const std::string& pMarker,
    const std::string& pBytes,
    std::size_t& pIndex,
    std::int64_t& pValue)
{
    const std::size_t lEndIndex{ pBytes.find(pMarker, pIndex) };
    if (lEndIndex != std::string::npos)
    {
        try
        {
            pValue = std::stoll(pBytes.substr(pIndex, lEndIndex - pIndex));
        }
        catch (...)
        {
            pValue = 0;
            return false;
        }
        pIndex = lEndIndex + pMarker.size();
        return true;
    }
    pValue = 0;
    return false;
}

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<std::string>& pValue,
    bool pDecodeEscapedCharacters)
{
    if (auto lIterator{ pTags.find(pKey) }; lIterator != pTags.cend())
    {
        if (lIterator->second.empty())
        {
            pValue.reset();
            return true;
        }
        if (pDecodeEscapedCharacters)
        {
            std::string lEncodedValue{ lIterator->second };
            if (!DecodeEscapedCharacters(lEncodedValue))
            {
                pValue.reset();
                return false;
            }
            pValue = lEncodedValue;
            return true;
        }
        pValue = lIterator->second;
        return true;
    }
    pValue.reset();
    return true;
}

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<std::int64_t>& pValue)
{
    if (auto lIterator{ pTags.find(pKey) }; lIterator != pTags.cend())
    {
        if (lIterator->second.empty())
        {
            pValue.reset();
            return true;
        }
        try
        {
            pValue = std::stoll(lIterator->second);
            return true;
        }
        catch (...)
        {
            pValue.reset();
            return false;
        }
    }
    pValue.reset();
    return true;
}

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<bool>& pValue)
{
    if (auto lIterator{ pTags.find(pKey) }; lIterator != pTags.cend())
    {
        if (lIterator->second.empty())
        {
            pValue = true;
            return true;
        }
        try
        {
            pValue = static_cast<bool>(std::stoi(lIterator->second));
            return true;
        }
        catch (...)
        {
            pValue.reset();
            return false;
        }
    }
    pValue.reset();
    return true;
}

bool ReadTagValue(
    const std::unordered_map<std::string, std::string>& pTags,
    const std::string& pKey,
    std::optional<Network::Twitch::Model::Color>& pValue)
{
    if (auto lIterator{ pTags.find(pKey) }; lIterator != pTags.cend())
    {
        if (lIterator->second.empty())
        {
            pValue.reset();
            return true;
        }
        if (lIterator->second.size() != 7)
        {
            pValue.reset();
            return false;
        }
        try
        {
            Network::Twitch::Model::Color lColorValue{};
            lColorValue.R = static_cast<std::uint8_t>(std::stoi(lIterator->second.substr(1, 2), nullptr, 16));
            lColorValue.G = static_cast<std::uint8_t>(std::stoi(lIterator->second.substr(3, 2), nullptr, 16));
            lColorValue.B = static_cast<std::uint8_t>(std::stoi(lIterator->second.substr(5, 2), nullptr, 16));
            pValue = lColorValue;
            return true;
        }
        catch (...)
        {
            pValue.reset();
            return false;
        }
    }
    pValue.reset();
    return true;
}

bool DecodeEscapedCharacters(
    std::string& pValue)
{
    std::string lUnescapedValue;
    bool lEscaped{ false };
    for (std::size_t lIndex{}; lIndex < pValue.size(); lIndex++)
    {
        if (lEscaped)
        {
            lEscaped = false;
            switch (pValue[lIndex])
            {
            case ':':
                lUnescapedValue += ';';
                break;
            case 's':
                lUnescapedValue += ' ';
                break;
            case '\\':
                lUnescapedValue += '\\';
                break;
            case 'r':
                lUnescapedValue += '\r';
                break;
            case 'n':
                lUnescapedValue += '\n';
                break;
            default:
                return false;
            }
        }
        else if (pValue[lIndex] == '\\')
        {
            lEscaped = true;
        }
        else
        {
            lUnescapedValue += pValue[lIndex];
        }
    }
    pValue = lUnescapedValue;
    return true;
}

} // anonymous namespace

} // Network::Twitch::IRC