///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC EventHandler Implementation                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/IRC/EventHandler.hpp>

namespace Network::Twitch::IRC
{

void EventHandler::OnMessage(
    const Network::Twitch::IRC::Event& pEvent)
{
    pEvent.Dispatch(*this);
}

void EventHandler::OnEvent(
    const Network::Twitch::IRC::Events::StatusChangeEvent& pEvent)
{}

void EventHandler::OnEvent(
    const Network::Twitch::IRC::Events::UserJoinEvent& pEvent)
{}

void EventHandler::OnEvent(
    const Network::Twitch::IRC::Events::UserPartEvent& pEvent)
{}

void EventHandler::OnEvent(
    const Network::Twitch::IRC::Events::UserMessageEvent& pEvent)
{}

void EventHandler::OnEvent(
    const Network::Twitch::IRC::Events::UserWhisperEvent& pEvent)
{}

} // Network::Twitch::IRC