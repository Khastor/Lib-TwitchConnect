///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserJoinEvent Implementation                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/UserJoinEvent.hpp>

namespace Network::Twitch::IRC::Events
{

UserJoinEvent::UserJoinEvent(
    const std::string& pChannel,
    const std::string& pUserLogin) :
    Channel(pChannel),
    UserLogin(pUserLogin)
{}

void UserJoinEvent::Dispatch(
    Network::Twitch::IRC::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::IRC::Events