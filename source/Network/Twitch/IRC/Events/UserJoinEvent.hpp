#ifndef NETWORK_TWITCH_IRC_EVENTS_USER_JOIN_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENTS_USER_JOIN_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserJoinEvent Interface                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::IRC::Events
{

/**
 * @brief Twitch IRC User Join event.
 *
 * This event is published when a user joins a Twitch IRC channel.
 */
struct UserJoinEvent : public Network::Twitch::IRC::Event
{
    /**
     * @brief Constructor.
     *
     * @param pChannel The Twitch IRC channel joined by the user.
     * @param pUserLogin The login of the user who joined the Twitch IRC channel.
     */
    UserJoinEvent(
        const std::string& pChannel,
        const std::string& pUserLogin);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::IRC::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch IRC channel joined by the user.
     */
    const std::string Channel;

    /**
     * @brief The login of the user who joined the Twitch IRC channel.
     */
    const std::string UserLogin;
};

} // Network::Twitch::IRC::Events

#endif // NETWORK_TWITCH_IRC_EVENTS_USER_JOIN_EVENT_HPP_INCLUDED