#ifndef NETWORK_TWITCH_IRC_EVENTS_USER_MESSAGE_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENTS_USER_MESSAGE_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserMessageEvent Interface                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/Model/Color.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <optional>
#include <string>

namespace Network::Twitch::IRC::Events
{

/**
 * @brief Twitch IRC User Message event.
 *
 * This event is published when a user sends a message in a Twitch IRC channel.
 */
struct UserMessageEvent : public Network::Twitch::IRC::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::IRC::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch IRC channel.
     */
    std::string Channel;

    /**
     * @brief The id of the user who sent the message.
     */
    std::string UserId;

    /**
     * @brief The login of the user who sent the message.
     */
    std::string UserLogin;

    /**
     * @brief The display name of the user who sent the message.
     */
    std::string UserName;

    /**
     * @brief The message content.
     */
    std::string Message;

    /**
     * @brief The message id.
     */
    std::string MessageId;

    /**
     * @brief The color of the user who sent the message.
     */
    std::optional<Network::Twitch::Model::Color> UserColor;

    /**
     * @brief True if the user who sent the message is a moderator.
     */
    bool UserIsModerator;

    /**
     * @brief True if the user who sent the message is a subscriber.
     */
    bool UserIsSubscriber;

    /**
     * @brief True if the user who sent the message is a vip.
     */
    bool UserIsVIP;
};

} // Network::Twitch::IRC::Events

#endif // NETWORK_TWITCH_IRC_EVENTS_USER_MESSAGE_EVENT_HPP_INCLUDED