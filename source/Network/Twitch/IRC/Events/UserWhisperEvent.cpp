///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserWhisperEvent Implementation                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/UserWhisperEvent.hpp>

namespace Network::Twitch::IRC::Events
{

void UserWhisperEvent::Dispatch(
    Network::Twitch::IRC::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::IRC::Events