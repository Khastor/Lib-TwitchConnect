#ifndef NETWORK_TWITCH_IRC_EVENTS_USER_PART_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENTS_USER_PART_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserPartEvent Interface                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::IRC::Events
{

/**
 * @brief Twitch IRC User Part event.
 *
 * This event is published when a user leaves or is banned from a Twitch IRC channel.
 */
struct UserPartEvent : public Network::Twitch::IRC::Event
{
    /**
     * @brief Constructor.
     *
     * @param pChannel The Twitch IRC channel left by the user.
     * @param pUserLogin The login of the user who left the Twitch IRC channel.
     */
    UserPartEvent(
        const std::string& pChannel,
        const std::string& pUserLogin);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::IRC::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch IRC channel left by the user.
     */
    const std::string Channel;

    /**
     * @brief The login of the user who left the Twitch IRC channel.
     */
    const std::string UserLogin;
};

} // Network::Twitch::IRC::Events

#endif // NETWORK_TWITCH_IRC_EVENTS_USER_PART_EVENT_HPP_INCLUDED