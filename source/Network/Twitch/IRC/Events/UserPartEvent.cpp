///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserPartEvent Implementation                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/UserPartEvent.hpp>

namespace Network::Twitch::IRC::Events
{

UserPartEvent::UserPartEvent(
    const std::string& pChannel,
    const std::string& pUserLogin) :
    Channel(pChannel),
    UserLogin(pUserLogin)
{}

void UserPartEvent::Dispatch(
    Network::Twitch::IRC::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::IRC::Events