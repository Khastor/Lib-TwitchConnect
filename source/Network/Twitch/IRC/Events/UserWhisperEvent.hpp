#ifndef NETWORK_TWITCH_IRC_EVENTS_USER_WHISPER_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENTS_USER_WHISPER_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserWhisperEvent Interface                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/Model/Color.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <optional>
#include <string>

namespace Network::Twitch::IRC::Events
{

/**
 * @brief Twitch IRC User Whisper event.
 *
 * This event is published when a user sends a whisper message to the broadcaster.
 */
struct UserWhisperEvent : public Network::Twitch::IRC::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::IRC::EventHandler& pHandler) const override;

    /**
     * @brief The id of the user who sent the whisper.
     */
    std::string UserId;

    /**
     * @brief The login of the user who sent the whisper.
     */
    std::string UserLogin;

    /**
     * @brief The display name of the user who sent the whisper.
     */
    std::string UserName;

    /**
     * @brief The whisper message content.
     */
    std::string Message;

    /**
     * @brief The whisper message id.
     */
    std::string MessageId;

    /**
     * @brief The color of the user who sent the whisper.
     */
    std::optional<Network::Twitch::Model::Color> UserColor;
};

} // Network::Twitch::IRC::Events

#endif // NETWORK_TWITCH_IRC_EVENTS_USER_WHISPER_EVENT_HPP_INCLUDED