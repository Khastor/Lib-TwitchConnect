///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC StatusChangeEvent Implementation                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/StatusChangeEvent.hpp>

namespace Network::Twitch::IRC::Events
{

StatusChangeEvent::StatusChangeEvent(
    Network::Twitch::IRC::Status pCode,
    const std::string& pChannel) :
    Code(pCode),
    Channel(pChannel)
{}

void StatusChangeEvent::Dispatch(
    Network::Twitch::IRC::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
};

} // Network::Twitch::IRC::Events