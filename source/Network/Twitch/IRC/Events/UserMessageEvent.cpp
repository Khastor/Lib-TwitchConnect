///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC UserMessageEvent Implementation                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/UserMessageEvent.hpp>

namespace Network::Twitch::IRC::Events
{

void UserMessageEvent::Dispatch(
    Network::Twitch::IRC::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::IRC::Events