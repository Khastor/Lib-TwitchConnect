#ifndef NETWORK_TWITCH_IRC_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC StatusChangeEvent Interface                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/IRC/Status.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::IRC::Events
{

/**
 * @brief Twitch IRC channel Status Change event.
 *
 * This event is published when a Twitch IRC channel connection status changes.
 */
struct StatusChangeEvent : public Network::Twitch::IRC::Event
{
    /**
     * @brief Constructor.
     *
     * @param pCode The Twitch IRC channel status code.
     * @param pChannel The Twitch IRC channel.
     */
    StatusChangeEvent(
        Network::Twitch::IRC::Status pCode,
        const std::string& pChannel);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::IRC::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch IRC channel status code.
     */
    const Network::Twitch::IRC::Status Code;

    /**
     * @brief The Twitch IRC channel.
     */
    const std::string Channel;
};

} // Network::Twitch::IRC::Events

#endif // NETWORK_TWITCH_IRC_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED