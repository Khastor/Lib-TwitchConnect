#ifndef NETWORK_TWITCH_IRC_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Event Interface                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::IRC
{

class EventHandler;

/**
 * @brief Base class in the hierarchy of Twitch IRC events.
 */
struct Event : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Event() = default;

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    virtual void Dispatch(
        EventHandler& pHandler) const = 0;
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_EVENT_HPP_INCLUDED