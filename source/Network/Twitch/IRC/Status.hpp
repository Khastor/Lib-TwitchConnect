#ifndef NETWORK_TWITCH_IRC_STATUS_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_STATUS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Service Status Definition                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Twitch::IRC
{

/**
 * @brief Twitch IRC service status.
 */
enum class Status
{
    /**
     * @brief The Twitch IRC service is connected to a Twitch IRC channel.
     */
    Connected,

    /**
     * @brief The Twitch IRC service is not connected to a Twitch IRC channel.
     */
    Disconnected
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_STATUS_HPP_INCLUDED