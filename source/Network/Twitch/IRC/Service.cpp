///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Service Implementation                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/IRC/Commands/SendChatMessageCommand.hpp>
#include <Network/Twitch/IRC/Constants.hpp>
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/IRC/Events/StatusChangeEvent.hpp>
#include <Network/Twitch/IRC/Events/UserJoinEvent.hpp>
#include <Network/Twitch/IRC/Events/UserMessageEvent.hpp>
#include <Network/Twitch/IRC/Events/UserPartEvent.hpp>
#include <Network/Twitch/IRC/Events/UserWhisperEvent.hpp>
#include <Network/Twitch/IRC/Parser.hpp>
#include <Network/Twitch/IRC/Service.hpp>
#include <Network/Twitch/OAuth2/Commands/ValidateAccessTokensCommand.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/bind/bind.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <cmath>
#include <filesystem>

namespace Network::Twitch::IRC
{

namespace
{

const std::string LOG_CHANNEL{ "Twitch IRC" };
const std::string LOG_FILENAME{ "TwitchIRCServiceLogs.log" };
constexpr std::uint32_t NETWORK_WRITE_TICK_IDLE{ 100ULL };
constexpr std::uint32_t NETWORK_WRITE_TICK_BUSY{ 10ULL };

} // anonymous namespace

Service::Service(
    std::uint32_t pRateLimitWindowCount,
    std::uint32_t pRateLimitWindowTime) :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME)),
    mRateLimitWindowCount(pRateLimitWindowCount),
    mRateLimitWindowTime(pRateLimitWindowTime),
    mRetryCount(0),
    mSSLContext(boost::asio::ssl::context::sslv23),
    mIOContext(std::make_unique<boost::asio::io_context>()),
    mSocket(std::make_unique<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>(*mIOContext, mSSLContext)),
    mSocketWriteTimer(std::make_unique<boost::asio::steady_timer>(*mIOContext)),
    mWorker([this](std::stop_token pStopToken) { Loop(pStopToken); }),
    mTwitchIRCCommandSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::IRC::Command>(this))
{}

Service::~Service()
{
    std::unique_lock<std::mutex> lLock(mMutex);
    mTwitchIRCCommandSubscription.Revoke();
    mWorker.request_stop();
    mIOContext->stop();
}

void Service::Initialize(
    std::uint32_t pRateLimitWindowCount,
    std::uint32_t pRateLimitWindowTime)
{
    static Service sInstance{ pRateLimitWindowCount, pRateLimitWindowTime };
}

void Service::Loop(
    std::stop_token pStopToken)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start";
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    boost::system::error_code lError;
    while (!pStopToken.stop_requested())
    {
        if (mRetryCount > 0)
        {
            const std::uint64_t lRetryDelay{ 1ULL << std::min(mRetryCount, 5ULL) };
            NETWORK_CORE_LOG_INFO(mLogger) << "connection retry #" << mRetryCount << " in " << lRetryDelay << "s";

            std::unique_lock<std::mutex> lLock{ mMutex };
            auto lStopCondition{ [pStopToken]() { return pStopToken.stop_requested(); } };
            if (mWaitCondition.wait_for(lLock, pStopToken, std::chrono::seconds(lRetryDelay), lStopCondition))
            {
                NETWORK_CORE_LOG_INFO(mLogger) << "connection aborted";
                continue;
            }
        }
        NETWORK_CORE_LOG_INFO(mLogger) << "connecting to " << Network::Twitch::IRC::Endpoints::HOSTNAME << ":" << Network::Twitch::IRC::Endpoints::PORT;
        mRetryCount++;

        // Initialize the SSL context and add an optional certification authority file or directory.
        mSSLContext.set_default_verify_paths(lError);
        if (lError)
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "SSL context error: " << lError.message();
            continue;
        }

        mSSLContext.set_verify_mode(boost::asio::ssl::verify_peer | boost::asio::ssl::verify_fail_if_no_peer_cert, lError);
        if (lError)
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "SSL context error: " << lError.message();
            continue;
        }

        mSSLContext.set_verify_callback(boost::asio::ssl::host_name_verification(Network::Twitch::IRC::Endpoints::HOSTNAME), lError);
        if (lError)
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "SSL context error: " << lError.message();
            continue;
        }

        const std::string lSSLVerifyFile{ lSettings.GetSSLVerifyFile(Network::Twitch::IRC::Endpoints::HOSTNAME) };
        if (std::filesystem::is_regular_file(lSSLVerifyFile))
        {
            mSSLContext.load_verify_file(lSSLVerifyFile, lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
            }
        }
        else if (!lSSLVerifyFile.empty())
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << "Bad SSL verify file: " << lSSLVerifyFile;
        }
        else
        {
            const std::string lSSLVerifyPath{ lSettings.GetSSLVerifyPath() };
            if (std::filesystem::is_directory(lSSLVerifyPath))
            {
                mSSLContext.add_verify_path(lSSLVerifyPath, lError);
                if (lError)
                {
                    NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
                }
            }
            else if (!lSSLVerifyPath.empty())
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << "Bad SSL verify path: " << lSSLVerifyPath;
            }
        }

        // Initialize the SSL stream and set SNI Hostname (many hosts need this to handshake successfully).
        if (!SSL_set_tlsext_host_name(mSocket->native_handle(), Network::Twitch::IRC::Endpoints::HOSTNAME.c_str()))
        {
            const boost::system::error_code lError{ static_cast<int>(::ERR_get_error()), boost::asio::error::get_ssl_category() };
            NETWORK_CORE_LOG_WARNING(mLogger) << "SSL context error: " << lError.message();
            continue;
        }

        boost::asio::ip::tcp::resolver lResolver{ *mIOContext };
        const auto lEndpoints{ lResolver.resolve(Network::Twitch::IRC::Endpoints::HOSTNAME, Network::Twitch::IRC::Endpoints::PORT, lError) };
        if (lError)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lError.message();
            continue;
        }

        boost::asio::async_connect(mSocket->lowest_layer(), lEndpoints,
            boost::bind(&Service::OnSocketConnect, this,
                boost::asio::placeholders::error,
                boost::asio::placeholders::endpoint));

        try
        {
            mIOContext->run();
        }
        catch (std::exception& lException)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lException.what();
        }

        const std::string lChannel{ lSettings.GetTwitchUserLogin() };
        Network::Core::Dispatcher::Instance().Publish(
            std::make_unique<Network::Twitch::IRC::Events::StatusChangeEvent>(Network::Twitch::IRC::Status::Disconnected, lChannel));

        {
            std::unique_lock<std::mutex> lLock(mMutex);

            // Cancel outstanding asynchronous operations and close the TCP socket.
            mSocket->lowest_layer().cancel(lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
            }
            mSocket->shutdown(lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
            }
            mSocket->lowest_layer().shutdown(boost::asio::ip::tcp::socket::shutdown_both, lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
            }
            mSocket->lowest_layer().close(lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << lError.message();
            }
            mSocket.reset();

            mSocketWriteTimer->cancel();
            mSocketWriteTimer.reset();

            // Hard reset the IO context to discard all waiting handlers.
            mIOContext = std::make_unique<boost::asio::io_context>();
            mSocket = std::make_unique<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>>(*mIOContext, mSSLContext);
            mSocketWriteTimer = std::make_unique<boost::asio::steady_timer>(*mIOContext);
            mSocketReadBuffer.clear();
        }
    }
    NETWORK_CORE_LOG_INFO(mLogger) << "stop";
}

void Service::PushSocketWrite(
    const std::string& pBytes)
{
    std::lock_guard<std::mutex> lLock{ mMutex };
    mSocketWriteBuffer.push(pBytes);
}

void Service::OnSocketConnect(
    const boost::system::error_code& pError,
    const boost::asio::ip::tcp::endpoint& pEndpoint)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket connect error: " << pError.message();
        mIOContext->stop();
        return;
    }

    mSocket->async_handshake(boost::asio::ssl::stream_base::client,
        boost::bind(&Service::OnSocketHandshake, this,
            boost::asio::placeholders::error));
}

void Service::OnSocketHandshake(
    const boost::system::error_code& pError)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket handshake error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::string lCommandMessage{ "CAP REQ :twitch.tv/commands twitch.tv/tags twitch.tv/membership\r\n" };
    std::shared_ptr<std::string> lCommandBuffer{ std::make_shared<std::string>(std::move(lCommandMessage)) };
    boost::asio::async_write(*mSocket, boost::asio::buffer(*lCommandBuffer),
        boost::bind(&Service::OnSocketWriteCapReq, this, lCommandBuffer,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketWriteCapReq(
    std::shared_ptr<std::string> pBuffer,
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::string lCommandMessage{ "PASS oauth:" + Network::Core::Settings::Instance().GetTwitchUserAccessToken() + "\r\n" };
    std::shared_ptr<std::string> lCommandBuffer{ std::make_shared<std::string>(std::move(lCommandMessage)) };
    boost::asio::async_write(*mSocket, boost::asio::buffer(*lCommandBuffer),
        boost::bind(&Service::OnSocketWritePass, this, lCommandBuffer,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketWritePass(
    std::shared_ptr<std::string> pBuffer,
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::string lCommandMessage{ "NICK " + Network::Core::Settings::Instance().GetTwitchUserLogin() + "\r\n" };
    std::shared_ptr<std::string> lCommandBuffer{ std::make_shared<std::string>(std::move(lCommandMessage)) };
    boost::asio::async_write(*mSocket, boost::asio::buffer(*lCommandBuffer),
        boost::bind(&Service::OnSocketWriteNick, this, lCommandBuffer,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketWriteNick(
    std::shared_ptr<std::string> pBuffer,
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::string lCommandMessage{ "JOIN #" + Network::Core::Settings::Instance().GetTwitchUserLogin() + "\r\n" };
    std::shared_ptr<std::string> lCommandBuffer{ std::make_shared<std::string>(std::move(lCommandMessage)) };
    boost::asio::async_write(*mSocket, boost::asio::buffer(*lCommandBuffer),
        boost::bind(&Service::OnSocketWriteJoin, this, lCommandBuffer,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketWriteJoin(
    std::shared_ptr<std::string> pBuffer,
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write error: " << pError.message();
        mIOContext->stop();
        return;
    }

    boost::asio::async_read_until(*mSocket, boost::asio::dynamic_buffer(mSocketReadBuffer), "\r\n",
        boost::bind(&Service::OnSocketRead, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketRead(
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket read error: " << pError.message();
        mIOContext->stop();
        return;
    }

    const std::string lLine{ mSocketReadBuffer.substr(0, pBytes) };
    mSocketReadBuffer.erase(0, pBytes);

    NETWORK_CORE_LOG_TRACE(mLogger) << "read: " << lLine.substr(0, lLine.size() - 2);
    if (!Dispatch(lLine))
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "parser error";
    }

    boost::asio::async_read_until(*mSocket, boost::asio::dynamic_buffer(mSocketReadBuffer), "\r\n",
        boost::bind(&Service::OnSocketRead, this,
            boost::asio::placeholders::error,
            boost::asio::placeholders::bytes_transferred));
}

void Service::OnSocketWrite(
    std::shared_ptr<std::string> pBuffer,
    const boost::system::error_code& pError,
    size_t pBytes)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write error: " << pError.message();
        mIOContext->stop();
        return;
    }

    NETWORK_CORE_LOG_TRACE(mLogger) << "write: " << pBuffer->substr(0, pBuffer->size() - 2);

    mSocketWriteTimer->expires_after(boost::asio::chrono::milliseconds(NETWORK_WRITE_TICK_BUSY));
    mSocketWriteTimer->async_wait(
        boost::bind(&Service::OnSocketWriteTick, this,
            boost::asio::placeholders::error));
}

void Service::OnSocketWriteTick(
    const boost::system::error_code& pError)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "socket write tick error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::lock_guard<std::mutex> lLock{ mMutex };

    const std::chrono::seconds lRateLimitWindow{ mRateLimitWindowTime };
    const std::chrono::steady_clock::time_point lNow{ std::chrono::steady_clock::now() };
    while (!mSocketWriteWindow.empty() && lNow - mSocketWriteWindow.front() > lRateLimitWindow)
    {
        mSocketWriteWindow.pop();
    }

    if (mSocketWriteWindow.size() == mRateLimitWindowCount)
    {
        mSocketWriteTimer->expires_at(mSocketWriteWindow.front() + lRateLimitWindow);
        mSocketWriteTimer->async_wait(
            boost::bind(&Service::OnSocketWriteTick, this,
                boost::asio::placeholders::error));
    }
    else if (!mSocketWriteBuffer.empty())
    {
        std::shared_ptr<std::string> lBuffer{ std::make_shared<std::string>(mSocketWriteBuffer.front()) };
        boost::asio::async_write(*mSocket, boost::asio::buffer(*lBuffer),
            boost::bind(&Service::OnSocketWrite, this, lBuffer,
                boost::asio::placeholders::error,
                boost::asio::placeholders::bytes_transferred));

        mSocketWriteWindow.push(boost::asio::chrono::steady_clock::now());
        mSocketWriteBuffer.pop();
    }
    else
    {
        mSocketWriteTimer->expires_after(std::chrono::milliseconds{ NETWORK_WRITE_TICK_IDLE });
        mSocketWriteTimer->async_wait(
            boost::bind(&Service::OnSocketWriteTick, this,
                boost::asio::placeholders::error));
    }
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessage)
{
    if (!pMessage.HasMembershipCap || !pMessage.HasCommandsCap || !pMessage.HasTagsCap)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "required Twitch IRC capabilities are not available";
        Network::Core::Dispatcher::Instance().Publish(
            std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
        mIOContext->stop();
        return;
    }
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessage)
{
    if (pMessage.HasMembershipCap || pMessage.HasCommandsCap || pMessage.HasTagsCap)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "required Twitch IRC capabilities are not available";
        Network::Core::Dispatcher::Instance().Publish(
            std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
        mIOContext->stop();
        return;
    }
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user join: " << pMessageData.UserLogin << "@" << pMessageData.Channel;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::IRC::Events::UserJoinEvent>(
            pMessageData.Channel,
            pMessageData.UserLogin));
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData)
{
    if (pMessageData.Channel.empty())
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << pMessageData.Message;
        Network::Core::Dispatcher::Instance().Publish(
            std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
        mIOContext->stop();
        return;
    }
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user part: " << pMessageData.UserLogin << "@" << pMessageData.Channel;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::IRC::Events::UserPartEvent>(
            pMessageData.Channel,
            pMessageData.UserLogin));
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData)
{
    PushSocketWrite("PONG " + pMessageData.Payload + "\r\n");
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData)
{
    const std::string lChannel{ Network::Core::Settings::Instance().GetTwitchUserLogin() };
    NETWORK_CORE_LOG_INFO(mLogger) << "read message: " << pMessageData.UserLogin << "@" << lChannel << ": " << pMessageData.Message;

    if (!pMessageData.UserId.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "PRIVMSG: empty UserId";
        return;
    }
    if (!pMessageData.UserName.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "PRIVMSG: empty UserName";
        return;
    }
    if (!pMessageData.MessageId.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "PRIVMSG: empty MessageId";
        return;
    }

    auto lUserMessageEvent{ std::make_unique<Network::Twitch::IRC::Events::UserMessageEvent>() };
    lUserMessageEvent->Channel = pMessageData.Channel;
    lUserMessageEvent->UserId = pMessageData.UserId.value();
    lUserMessageEvent->UserLogin = pMessageData.UserLogin;
    lUserMessageEvent->UserName = pMessageData.UserName.value();
    lUserMessageEvent->Message = pMessageData.Message;
    lUserMessageEvent->MessageId = pMessageData.MessageId.value();
    lUserMessageEvent->UserColor = pMessageData.UserColor;
    lUserMessageEvent->UserIsModerator = pMessageData.UserIsModerator.has_value() && pMessageData.UserIsModerator.value();
    lUserMessageEvent->UserIsSubscriber = pMessageData.UserIsSubscriber.has_value() && pMessageData.UserIsSubscriber.value();
    lUserMessageEvent->UserIsVIP = pMessageData.UserIsVIP.has_value() && pMessageData.UserIsVIP.value();
    Network::Core::Dispatcher::Instance().Publish(std::move(lUserMessageEvent));
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "connection reset";
    mIOContext->stop();
    return;
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData)
{
    const std::string lChannel{ Network::Core::Settings::Instance().GetTwitchUserLogin() };
    NETWORK_CORE_LOG_INFO(mLogger) << "read whisper: " << pMessageData.UserLogin << "@" << lChannel;

    if (!pMessageData.UserId.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "WHISPER: empty UserId";
        return;
    }
    if (!pMessageData.UserName.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "WHISPER: empty UserName";
        return;
    }
    if (!pMessageData.MessageId.has_value())
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << "WHISPER: empty MessageId";
        return;
    }

    auto lUserMessageEvent{ std::make_unique<Network::Twitch::IRC::Events::UserWhisperEvent>() };
    lUserMessageEvent->UserId = pMessageData.UserId.value();
    lUserMessageEvent->UserLogin = pMessageData.UserLogin;
    lUserMessageEvent->UserName = pMessageData.UserName.value();
    lUserMessageEvent->Message = pMessageData.Message;
    lUserMessageEvent->MessageId = pMessageData.MessageId.value();
    lUserMessageEvent->UserColor = pMessageData.UserColor;
    Network::Core::Dispatcher::Instance().Publish(std::move(lUserMessageEvent));
}

void Service::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData)
{
    if (mRetryCount > 0)
    {
        mRetryCount = 0;

        NETWORK_CORE_LOG_INFO(mLogger) << "connected";

        const std::string lChannel{ Network::Core::Settings::Instance().GetTwitchUserLogin() };
        Network::Core::Dispatcher::Instance().Publish(
            std::make_unique<Network::Twitch::IRC::Events::StatusChangeEvent>(Network::Twitch::IRC::Status::Connected, lChannel));

        mSocketWriteTimer->expires_after(std::chrono::milliseconds{ NETWORK_WRITE_TICK_IDLE });
        mSocketWriteTimer->async_wait(
            boost::bind(&Service::OnSocketWriteTick, this,
                boost::asio::placeholders::error));
    }
}

void Service::OnCommand(
    const Network::Twitch::IRC::Commands::SendChatMessageCommand& pCommand)
{
    const std::string lChannel{ Network::Core::Settings::Instance().GetTwitchUserLogin() };
    NETWORK_CORE_LOG_INFO(mLogger) << "write message: " << lChannel << "@" << lChannel << ": " << pCommand.Message;

    if (pCommand.ParentId.empty())
    {
        PushSocketWrite("PRIVMSG #" + lChannel + " :" + pCommand.Message + "\r\n");
    }
    else
    {
        PushSocketWrite("@reply-parent-msg-id=" + pCommand.ParentId + " PRIVMSG #" + lChannel + " :" + pCommand.Message + "\r\n");
    }
}

} // Network::Twitch::IRC