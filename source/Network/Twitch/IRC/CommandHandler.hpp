#ifndef NETWORK_TWITCH_IRC_COMMAND_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_COMMAND_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC CommandHandler Interface                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/IRC/Command.hpp>

namespace Network::Twitch::IRC
{

namespace Commands
{

struct SendChatMessageCommand;

} // Commands

/**
 * @brief Twitch IRC command handler.
 *
 * Provide Twitch IRC command handler methods that can be invoked asynchronously.
 */
class CommandHandler : public Network::Core::Observer<Network::Twitch::IRC::Command>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the command message to the appropriate command handler method.
     *
     * @param pCommand The command message to dispatch.
     */
    void OnMessage(
        const Network::Twitch::IRC::Command& pCommand) override;

public:
    /**
     * @brief Twitch IRC Send Chat Message command handler.
     *
     * @param pCommand The command to handle.
     */
    virtual void OnCommand(
        const Network::Twitch::IRC::Commands::SendChatMessageCommand& pCommand);
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_COMMAND_HANDLER_HPP_INCLUDED