#ifndef NETWORK_TWITCH_IRC_EVENT_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_EVENT_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC EventHandler Interface                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/IRC/Event.hpp>

namespace Network::Twitch::IRC
{

namespace Events
{

struct StatusChangeEvent;
struct UserJoinEvent;
struct UserPartEvent;
struct UserMessageEvent;
struct UserWhisperEvent;

} // Events

/**
 * @brief Twitch IRC event handler.
 *
 * Provide Twitch IRC event handler methods that can be invoked asynchronously.
 */
class EventHandler : public Network::Core::Observer<Network::Twitch::IRC::Event>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the event message to the appropriate event handler method.
     *
     * @param pEvent The event message to handle.
     */
    void OnMessage(
        const Network::Twitch::IRC::Event& pEvent) override;

public:
    /**
     * @brief Twitch IRC Status Change event handler.
     *
     * This handler is invoked when a Twitch IRC channel connection status changes.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::IRC::Events::StatusChangeEvent& pEvent);

    /**
     * @brief Twitch IRC User Join event handler.
     *
     * This handler is invoked when a user joins a Twitch IRC channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::IRC::Events::UserJoinEvent& pEvent);

    /**
     * @brief Twitch IRC User Part event handler.
     *
     * This handler is invoked when a user leaves or is banned from a Twitch IRC channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::IRC::Events::UserPartEvent& pEvent);

    /**
     * @brief Twitch IRC User Message event handler.
     *
     * This handler is invoked when a user sends a message in a Twitch IRC channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::IRC::Events::UserMessageEvent& pEvent);

    /**
     * @brief Twitch IRC User Whisper event handler.
     *
     * This handler is invoked when a user sends a whisper message.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::IRC::Events::UserWhisperEvent& pEvent);
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_EVENT_HANDLER_HPP_INCLUDED