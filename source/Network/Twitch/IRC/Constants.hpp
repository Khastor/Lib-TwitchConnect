#ifndef NETWORK_TWITCH_IRC_CONSTANTS_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Endpoints Definitions                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::IRC::Endpoints
{

/**
 * @brief Twitch IRC remote host name.
 */
inline const std::string HOSTNAME{ "irc.chat.twitch.tv" };

/**
 * @brief Twitch IRC remote host port.
 */
inline const std::string PORT{ "6697" };

} // Network::Twitch::IRC::Endpoints

#endif // NETWORK_TWITCH_IRC_CONSTANTS_HPP_INCLUDED