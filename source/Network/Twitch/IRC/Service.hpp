#ifndef NETWORK_TWITCH_IRC_SERVICE_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_SERVICE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch IRC Service Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Logging.hpp>
#include <Network/Twitch/IRC/CommandHandler.hpp>
#include <Network/Twitch/IRC/Event.hpp>
#include <Network/Twitch/IRC/Parser.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/asio/steady_timer.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <chrono>
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <queue>
#include <string>
#include <thread>

namespace Network::Twitch::IRC
{

/**
 * @brief Twitch IRC Service.
 *
 * Connect to a single Twitch IRC channel, send and receive chat messages.
 */
class Service : public Network::Twitch::IRC::CommandHandler, private Network::Twitch::IRC::Parser
{
private:
    /**
     * @brief Constructor.
     *
     * Start the Twitch IRC service, connect to the Twitch IRC channel.
     *
     * @param pRateLimitWindowCount The number of allowed messages to send to the Twitch IRC channel within the rate-limit sliding window.
     * @param pRateLimitWindowTime The duration of the rate-limit sliding window, in seconds.
     * @see https://dev.twitch.tv/docs/irc/#rate-limits.
     */
    Service(
        std::uint32_t pRateLimitWindowCount,
        std::uint32_t pRateLimitWindowTime);

public:
    /**
     * @brief Destructor.
     *
     * Stop the Twitch IRC service, disconnect from the Twitch IRC channel.
     */
    ~Service();

    /**
     * @brief Deleted copy-constructor.
     */
    Service(const Service&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Service(Service&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Service& operator==(const Service&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Service& operator==(Service&&) = delete;

    /**
     * @brief Initialize the Twitch IRC service.
     *
     * Start the Twitch IRC service, connect to the Twitch IRC channel.
     *
     * @param pRateLimitWindowCount The number of allowed messages to send to the Twitch IRC channel within the rate-limit sliding window.
     * @param pRateLimitWindowTime The duration of the rate-limit sliding window, in seconds.
     * @see https://dev.twitch.tv/docs/irc/#rate-limits.
     */
    static void Initialize(
        std::uint32_t pRateLimitWindowCount = 20,
        std::uint32_t pRateLimitWindowTime = 30);

private:
    /**
     * @brief Worker thread main loop.
     *
     * The main loop is started upon construction, and stopped upon destruction.
     *
     * @param pStopToken Signal sent upon destruction to request the main loop to stop.
     */
    void Loop(
        std::stop_token pStopToken);

    /**
     * @brief Push a new message to send to the Twitch IRC channel in the socket write queue.
     *
     * The message must be a well-formatted Twitch IRC message ending with CRLF.
     *
     * @param pBytes The data to send.
     */
    void PushSocketWrite(
        const std::string& pBytes);

    /**
     * @brief Twitch IRC TCP connection handler.
     *
     * This handler is called when the TCP connection completes.
     *
     * @param pError The network error, if any.
     * @param pEndpoint The TCP connection endpoint.
     */
    void OnSocketConnect(
        const boost::system::error_code& pError,
        const boost::asio::ip::tcp::endpoint& pEndpoint);

    /**
     * @brief Twitch IRC SSL handshake handler.
     *
     * This handler is called when the SSL handshake completes.
     *
     * @param pError The network error, if any.
     */
    void OnSocketHandshake(
        const boost::system::error_code& pError);

    /**
     * @brief Twitch IRC CAP REQ write handler.
     *
     * This handler is called when the IRC CAP REQ write completes.
     *
     * @param pBuffer The write buffer.
     * @param pError The network error, if any.
     * @param pBytes The number of bytes transferred.
     */
    void OnSocketWriteCapReq(
        std::shared_ptr<std::string> pBuffer,
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC PASS write handler.
     *
     * This handler is called when the IRC PASS write completes.
     *
     * @param pBuffer The write buffer.
     * @param pError The network error, if any.
     * @param pBytes The number of bytes transferred.
     */
    void OnSocketWritePass(
        std::shared_ptr<std::string> pBuffer,
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC NICK write handler.
     *
     * This handler is called when the IRC NICK write completes.
     *
     * @param pBuffer The write buffer.
     * @param pError The network error, if any.
     * @param pBytes The number of bytes transferred.
     */
    void OnSocketWriteNick(
        std::shared_ptr<std::string> pBuffer,
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC JOIN write handler.
     *
     * This handler is called when the IRC JOIN write completes.
     *
     * @param pBuffer The write buffer.
     * @param pError The network error, if any.
     * @param pBytes The number of bytes transferred.
     */
    void OnSocketWriteJoin(
        std::shared_ptr<std::string> pBuffer,
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC read handler.
     *
     * This handler is called when a Twitch IRC message read completes.
     *
     * @param pError The network error, if any.
     * @param pBytes The numbers of bytes to read in the buffer.
     */
    void OnSocketRead(
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC write handler.
     *
     * This handler is called when a Twitch IRC message write completes.
     *
     * @param pBuffer The write buffer.
     * @param pError The network error, if any.
     * @param pBytes The numbers of bytes transferred.
     */
    void OnSocketWrite(
        std::shared_ptr<std::string> pBuffer,
        const boost::system::error_code& pError,
        size_t pBytes);

    /**
     * @brief Twitch IRC write timer handler.
     *
     * This handler is called when the write timer is triggered to allow sending the next message.
     *
     * @param pError The system error, if any.
     */
    void OnSocketWriteTick(
        const boost::system::error_code& pError);

    /**
     * @brief Twitch IRC Send Chat Message command handler.
     *
     * Send a message in the broadcaster's Twitch IRC channel.
     *
     * @param pCommand The command to handle.
     */
    void OnCommand(
        const Network::Twitch::IRC::Commands::SendChatMessageCommand& pCommand) override;

    using Network::Twitch::IRC::Parser::OnParseMessage;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessage) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessage) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData) override;

    /**
     * @brief The Twitch IRC service log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief The SSL context.
     */
    boost::asio::ssl::context mSSLContext;

    /**
     * @brief The IO context.
     *
     * The IO context is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, a new instance of IO context can be constructed to discard all waiting handlers.
     */
    std::unique_ptr<boost::asio::io_context> mIOContext;

    /**
     * @brief The TCP socket to communicate with the remote host.
     *
     * The socket is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, the IO context executor is reset, so a new instance of the socket must be constructed.
     */
    std::unique_ptr<boost::asio::ssl::stream<boost::asio::ip::tcp::socket>> mSocket;

    /**
     * @brief The socket write timer, to enforce write rate limits.
     *
     * The socket write timer is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, the IO context executor is reset, so a new instance of the socket write timer must be constructed.
     *
     * @see https://dev.twitch.tv/docs/irc/#rate-limits.
     */
    std::unique_ptr<boost::asio::steady_timer> mSocketWriteTimer;

    /**
     * @brief The number of allowed messages to send to the Twitch IRC channel within the rate-limit sliding window.
     */
    std::uint32_t mRateLimitWindowCount;

    /**
     * @brief The duration of the rate-limit sliding window, in seconds.
     */
    std::uint32_t mRateLimitWindowTime;

    /**
     * @brief The current retry streak.
     */
    std::size_t mRetryCount{ 0ULL };

    /**
     * @brief The buffer of data received from the TCP socket.
     */
    std::string mSocketReadBuffer;

    /**
     * @brief The buffer of messages to send to the IRC channel.
     */
    std::queue<std::string> mSocketWriteBuffer;

    /**
     * @brief The time points of all messages sent to the IRC channel during the rate-limit sliding window.
     */
    std::queue<std::chrono::steady_clock::time_point> mSocketWriteWindow;

    /**
     * @brief Worker thread wait condition.
     *
     * Wake up the worker thread after waiting before a retry.
     */
    std::condition_variable_any mWaitCondition;

    /**
     * @brief Mutual exclusion to synchronize the worker thread, the message dispatcher thread and the service destruction.
     *
     * Protect the IO context pointer and the socket write buffer.
     */
    std::mutex mMutex;

    /**
     * @brief Worker thread.
     *
     * Invoke asynchronous Twitch IRC socket handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;

    /**
     * @brief Subscription to Twitch IRC Command messages.
     */
    Network::Core::Dispatcher::Subscription mTwitchIRCCommandSubscription;
};

} // Network::Twitch::IRC

#endif // NETWORK_TWITCH_IRC_SERVICE_HPP_INCLUDED