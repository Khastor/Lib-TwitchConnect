#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_SERVICE_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_SERVICE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Extensions Heat Service Interface                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>
#include <Network/Twitch/Extensions/Heat/Workflows/WebsocketWorkflow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <condition_variable>
#include <cstddef>
#include <memory>
#include <mutex>
#include <string>
#include <thread>

namespace Network::Twitch::Extensions::Heat
{

/**
 * @brief Twitch Heat Extension service.
 *
 * Receive Twitch Heat Extension websocket messages.
 * The Twitch Heat Extension must be active on the broadcaster's channel.
 *
 * @see https://heat.j38.net/about.
 */
class Service : public Network::Twitch::Extensions::Heat::Workflows::WebsocketWorkflow
{
private:
    /**
     * @brief Constructor.
     *
     * Start receiving Twitch Heat Extension websocket messages.
     */
    Service();

public:
    /**
     * @brief Destructor.
     *
     * Stop receiving Twitch Heat Extension websocket messages.
     */
    ~Service();

    /**
     * @brief Deleted copy-constructor.
     */
    Service(const Service&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Service(Service&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Service& operator==(const Service&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Service& operator==(Service&&) = delete;

    /**
     * @brief Initialize the Twitch Heat Extension service.
     *
     * Start receiving Twitch Heat Extension websocket messages.
     */
    static void Initialize();

private:
    /**
     * @brief Worker thread main loop.
     *
     * Invoke asynchronous Twitch Heat Extension websocket session handlers.
     * The main loop is started upon construction, and stopped upon destruction.
     *
     * @param pStopToken Signal sent upon destruction to request the main loop to stop.
     */
    void Loop(
        std::stop_token pStopToken);

    /**
     * @brief Twitch Heat Extension websocket session error handler.
     *
     * This handler is invoked when a websocket session fails due to a connection error.
     *
     * @param pError The error details.
     */
    void OnHeatWebsocketSessionError(
        const std::string& pError) override;

    /**
     * @brief Twitch Heat Extension websocket message error handler.
     *
     * This handler is invoked when a websocket session fails due to a websocket message error.
     *
     * @param pError The error details.
     */
    void OnHeatWebsocketMessageError(
        const std::string& pError) override;

    /**
     * @brief Twitch Heat Extension websocket welcome message handler.
     *
     * This handler is invoked when a websocket session receives a welcome message after connecting successfully.
     */
    void OnHeatWebsocketWelcomeMessage() override;

    /**
     * @brief Twitch Heat Extension websocket click message handler.
     *
     * This handler is invoked when a websocket session receives a click message.
     *
     * @param pUserId The user id or an opaque id.
     * @param pCursorX The normalized x coordinate of the cursor in the video player.
     * @param pCursorY The normalized y coordinate of the cursor in the video player.
     */
    void OnHeatWebsocketClickMessage(
        const std::string& pUserId,
        float pCursorX,
        float pCursorY) override;

    /**
     * @brief The Twitch Heat Extension service log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief The IO context.
     *
     * The IO context is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, a new instance of IO context can be constructed to discard all waiting handlers.
     */
    std::unique_ptr<boost::asio::io_context> mIOContext;

    /**
     * @brief The current retry streak.
     */
    std::size_t mRetryCount{ 0ULL };

    /**
     * @brief Worker thread wait condition.
     *
     * Wake up the worker thread after waiting before a retry.
     */
    std::condition_variable_any mWaitCondition;

    /**
     * @brief Mutual exclusion to synchronize the worker thread, the message dispatcher thread and the service destruction.
     *
     * Protect the IO context pointer and the wait condition.
     */
    std::mutex mMutex;

    /**
     * @brief Worker thread.
     *
     * Invoke asynchronous Twitch Heat Extension websocket session handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;
};

} // Network::Twitch::Extensions::Heat

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_SERVICE_HPP_INCLUDED