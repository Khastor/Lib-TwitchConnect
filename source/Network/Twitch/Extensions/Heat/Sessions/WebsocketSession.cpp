///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension WebsocketSession Implementation                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/Extensions/Heat/Constants.hpp>
#include <Network/Twitch/Extensions/Heat/Sessions/WebsocketSession.hpp>
#include <Network/Twitch/Extensions/Heat/Workflows/WebsocketWorkflow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace Network::Twitch::Extensions::Heat::Sessions
{

const std::string LOG_PREFIX{ "[WebsocketSession] " };

WebsocketSession::WebsocketSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::Extensions::Heat::Workflows::WebsocketWorkflow& pWorkflow,
    boost::asio::io_context& pIOContext) :
    Network::Core::WebsocketSession(pLogger, pIOContext,
        Network::Twitch::Extensions::Heat::Endpoints::HOSTNAME,
        Network::Twitch::Extensions::Heat::Endpoints::PORT,
        Network::Twitch::Extensions::Heat::Endpoints::CHANNEL + Core::Settings::Instance().GetTwitchUserId()),
    mLogger(pLogger),
    mWorkflow(pWorkflow)
{}

void WebsocketSession::OnResolveError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    mWorkflow.OnHeatWebsocketSessionError(Network::Twitch::Extensions::Heat::Errors::SESSION_RESOLVE);
}

void WebsocketSession::OnConnectError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    mWorkflow.OnHeatWebsocketSessionError(Network::Twitch::Extensions::Heat::Errors::SESSION_CONNECT);
}

void WebsocketSession::OnSSLHandshakeError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "SSL handshake error: " << pError;
    mWorkflow.OnHeatWebsocketSessionError(Network::Twitch::Extensions::Heat::Errors::SESSION_SSL_HANDSHAKE);
}

void WebsocketSession::OnWebsocketHandshakeError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "websocket handshake error: " << pError;
    mWorkflow.OnHeatWebsocketSessionError(Network::Twitch::Extensions::Heat::Errors::SESSION_WEBSOCKET_HANDSHAKE);
}

void WebsocketSession::OnWebsocketReadError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    mWorkflow.OnHeatWebsocketSessionError(Network::Twitch::Extensions::Heat::Errors::SESSION_READ);
}

bool WebsocketSession::OnWebsocketMessage(
    const std::string& pMessage)
{
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "message:\n" << pMessage;
    boost::property_tree::ptree lWebsocketMessageJSON;
    try
    {
        std::stringstream lWebsocketMessageBody{ pMessage };
        boost::property_tree::read_json(lWebsocketMessageBody, lWebsocketMessageJSON);

        const auto lMessageType{ lWebsocketMessageJSON.get_optional<std::string>(Network::Twitch::Extensions::Heat::Keys::MESSAGE_TYPE) };
        if (!lMessageType)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::Extensions::Heat::Keys::MESSAGE_TYPE;
            mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
            return true;
        }

        if (lMessageType.get() == Network::Twitch::Extensions::Heat::Keys::MESSAGE_TYPE_CLICK)
        {
            const auto lUserId{ lWebsocketMessageJSON.get_optional<std::string>(Network::Twitch::Extensions::Heat::Keys::MESSAGE_ID) };
            if (!lUserId)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::Extensions::Heat::Keys::MESSAGE_ID;
                mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
                return true;
            }

            const auto lCursorX{ lWebsocketMessageJSON.get_optional<float>(Network::Twitch::Extensions::Heat::Keys::MESSAGE_X) };
            if (!lCursorX)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::Extensions::Heat::Keys::MESSAGE_X;
                mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
                return true;
            }

            const auto lCursorY{ lWebsocketMessageJSON.get_optional<float>(Network::Twitch::Extensions::Heat::Keys::MESSAGE_Y) };
            if (!lCursorY)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::Extensions::Heat::Keys::MESSAGE_Y;
                mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
                return true;
            }

            mWorkflow.OnHeatWebsocketClickMessage(lUserId.get(), lCursorX.get(), lCursorY.get());
            return true;
        }

        if (lMessageType.get() == Network::Twitch::Extensions::Heat::Keys::MESSAGE_TYPE_SYSTEM)
        {
            const auto lMessage{ lWebsocketMessageJSON.get_optional<std::string>(Network::Twitch::Extensions::Heat::Keys::MESSAGE_MESSAGE) };
            if (!lMessage)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::Extensions::Heat::Keys::MESSAGE_MESSAGE;
                mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
                return true;
            }

            if (lMessage.get() == Network::Twitch::Extensions::Heat::Keys::MESSAGE_MESSAGE_WELCOME)
            {
                mWorkflow.OnHeatWebsocketWelcomeMessage();
                return true;
            }

            return true;
        }
    }
    catch (boost::property_tree::json_parser_error& lException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "bad content: " << lException.what();
        mWorkflow.OnHeatWebsocketMessageError(Network::Twitch::Extensions::Heat::Errors::BAD_CONTENT);
        return true;
    }
    return true;
}

} // Network::Twitch::Extensions::Heat::Sessions