///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension Service Implementation                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/Extensions/Heat/Constants.hpp>
#include <Network/Twitch/Extensions/Heat/Events/UserClickEvent.hpp>
#include <Network/Twitch/Extensions/Heat/Service.hpp>
#include <Network/Twitch/Extensions/Heat/Sessions/WebsocketSession.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <utility>

namespace Network::Twitch::Extensions::Heat
{

const std::string LOG_CHANNEL{ "Twitch Heat" };
const std::string LOG_FILENAME{ "TwitchHeatServiceLogs.log" };

Service::Service() :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME)),
    mIOContext(std::make_unique<boost::asio::io_context>()),
    mWorker([this](std::stop_token pStopToken) { Loop(pStopToken); })
{}

Service::~Service()
{
    std::unique_lock<std::mutex> lLock(mMutex);
    mWorker.request_stop();
    mIOContext->stop();
}

void Service::Initialize()
{
    static Service sInstance;
}

void Service::Loop(
    std::stop_token pStopToken)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start";
    while (!pStopToken.stop_requested())
    {
        if (mRetryCount > 0)
        {
            const std::uint64_t lRetryDelay{ 1ULL << std::min(mRetryCount, 5ULL) };
            NETWORK_CORE_LOG_INFO(mLogger) << "connection retry #" << mRetryCount << " in " << lRetryDelay << "s";

            std::unique_lock<std::mutex> lLock{ mMutex };
            auto lStopCondition{ [pStopToken]() { return pStopToken.stop_requested(); } };
            if (mWaitCondition.wait_for(lLock, pStopToken, std::chrono::seconds(lRetryDelay), lStopCondition))
            {
                NETWORK_CORE_LOG_INFO(mLogger) << "connection aborted";
                continue;
            }
        }
        NETWORK_CORE_LOG_INFO(mLogger) << "connecting to " << Network::Twitch::Extensions::Heat::Endpoints::HOSTNAME << ":" << Network::Twitch::Extensions::Heat::Endpoints::PORT;
        mRetryCount++;

        std::make_shared<Network::Twitch::Extensions::Heat::Sessions::WebsocketSession>(
            mLogger, *this, *mIOContext)->Start();

        try
        {
            mIOContext->run();
        }
        catch (std::exception& lException)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lException.what();
        }

        {
            std::unique_lock<std::mutex> lLock(mMutex);

            // Hard reset the IO context to discard all waiting handlers.
            mIOContext = std::make_unique<boost::asio::io_context>();
        }
    }
    NETWORK_CORE_LOG_INFO(mLogger) << "stop";
}

void Service::OnHeatWebsocketSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << pError;
    mIOContext->stop();
}

void Service::OnHeatWebsocketMessageError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_WARNING(mLogger) << "parser error: " << pError;
}

void Service::OnHeatWebsocketWelcomeMessage()
{
    NETWORK_CORE_LOG_INFO(mLogger) << "connected";
    mRetryCount = 0;
}

void Service::OnHeatWebsocketClickMessage(
    const std::string& pUserId,
    float pCursorX,
    float pCursorY)
{
    auto lEvent{ std::make_unique<Network::Twitch::Extensions::Heat::Events::UserClickEvent>() };
    lEvent->UserId = pUserId;
    lEvent->CursorX = pCursorX;
    lEvent->CursorY = pCursorY;
    Network::Core::Dispatcher::Instance().Publish(std::move(lEvent));
}

} // Network::Twitch::Extensions::Heat