#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_CONSTANTS_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension Endpoint Definitions                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::Extensions::Heat::Endpoints
{

/**
 * @brief Twitch Heat Extension remote host name.
 */
inline const std::string HOSTNAME{ "heat-api.j38.net" };

/**
 * @brief Twitch Heat Extension remote host port.
 */
inline const std::string PORT{ "443" };

/**
 * @brief Twitch Heat Extension channel endpoint.
 */
inline const std::string CHANNEL{ "/channel/" };

} // Network::Twitch::Extensions::Heat::Endpoints

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension Message Keys Definitions                                                //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::Extensions::Heat::Keys
{

/**
 * @brief Twitch Heat Extension message key "id".
 */
inline const std::string MESSAGE_ID{ "id" };

/**
 * @brief Twitch Heat Extension message key "message".
 */
inline const std::string MESSAGE_MESSAGE{ "message" };

/**
 * @brief Twitch Heat Extension message key "message" enumerator value for connected status.
 */
inline const std::string MESSAGE_MESSAGE_WELCOME{ "Connected to Heat API server." };

/**
 * @brief Twitch Heat Extension message key "type".
 */
inline const std::string MESSAGE_TYPE{ "type" };

/**
 * @brief Twitch Heat Extension message key "type" enumerator value "click".
 */
inline const std::string MESSAGE_TYPE_CLICK{ "click" };

/**
 * @brief Twitch Heat Extension message key "type" enumerator value "system".
 */
inline const std::string MESSAGE_TYPE_SYSTEM{ "system" };

/**
 * @brief Twitch Heat Extension message key "x".
 */
inline const std::string MESSAGE_X{ "x" };

/**
 * @brief Twitch Heat Extension message key "y".
 */
inline const std::string MESSAGE_Y{ "y" };

} // Network::Twitch::Extensions::Heat::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension Errors Definitions                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::Extensions::Heat::Errors
{

/**
 * @brief Twitch Heat Extension error message for hostname resolution errors.
 */
inline const std::string SESSION_RESOLVE{ "Hostname resolution error." };

/**
 * @brief Twitch Heat Extension error message for connection errors.
 */
inline const std::string SESSION_CONNECT{ "Connection error." };

/**
 * @brief Twitch Heat Extension error message for SSL handshake errors.
 */
inline const std::string SESSION_SSL_HANDSHAKE{ "SSL handshake error." };

/**
 * @brief Twitch Heat Extension error message for websocket handshake errors.
 */
inline const std::string SESSION_WEBSOCKET_HANDSHAKE{ "Websocket handshake error." };

/**
 * @brief Twitch Heat Extension error message for network read errors.
 */
inline const std::string SESSION_READ{ "Network read error." };

/**
 * @brief Twitch Heat Extension error message for websocket message parsing errors.
 */
inline const std::string BAD_CONTENT{ "Bad content." };

} // Network::Twitch::Extensions::Heat::Errors

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_CONSTANTS_HPP_INCLUDED