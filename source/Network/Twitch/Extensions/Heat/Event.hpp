#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension Event Interface                                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::Extensions::Heat
{

class EventHandler;

/**
 * @brief Base class in the hierarchy of Twitch Heat Extension events.
 */
struct Event : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Event() = default;

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    virtual void Dispatch(
        EventHandler& pHandler) const = 0;
};

} // Network::Twitch::Extensions::Heat

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HPP_INCLUDED