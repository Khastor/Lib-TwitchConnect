#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension WebsocketWorkflow Interface                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::Extensions::Heat::Workflows
{

/**
 * @brief Twitch Heat Extension workflow interface.
 *
 * @see https://github.com/scottgarner/Heat/wiki/Custom-Experiences.
 */
class WebsocketWorkflow
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~WebsocketWorkflow() = default;

    /**
     * @brief Twitch Heat Extension websocket session error handler.
     *
     * This handler is invoked when a websocket session fails due to a connection error.
     *
     * @param pError The error details.
     */
    virtual void OnHeatWebsocketSessionError(
        const std::string& pError) = 0;
    
    /**
     * @brief Twitch Heat Extension websocket message error handler.
     *
     * This handler is invoked when a websocket session fails due to a websocket message error.
     *
     * @param pError The error details.
     */
    virtual void OnHeatWebsocketMessageError(
        const std::string& pError) = 0;

    /**
     * @brief Twitch Heat Extension websocket welcome message handler.
     *
     * This handler is invoked when a websocket session receives a welcome message after connecting successfully.
     */
    virtual void OnHeatWebsocketWelcomeMessage() = 0;

    /**
     * @brief Twitch Heat Extension websocket click message handler.
     *
     * This handler is invoked when a websocket session receives a click message.
     *
     * @param pUserId The user id or an opaque id.
     * @param pCursorX The normalized x coordinate of the cursor in the video player.
     * @param pCursorY The normalized y coordinate of the cursor in the video player.
     */
    virtual void OnHeatWebsocketClickMessage(
        const std::string& pUserId,
        float pCursorX,
        float pCursorY) = 0;
};

} // Network::Twitch::Extensions::Heat::Workflows

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED