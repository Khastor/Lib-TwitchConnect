#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension EventHandler Interface                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/Extensions/Heat/Event.hpp>

namespace Network::Twitch::Extensions::Heat
{

namespace Events
{

struct UserClickEvent;

} // Events

/**
 * @brief Twitch Heat Extension event handler.
 *
 * Provide Twitch Heat Extension event handler methods that can be invoked asynchronously.
 */
class EventHandler : public Network::Core::Observer<Network::Twitch::Extensions::Heat::Event>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the event message to the appropriate event handler method.
     *
     * @param pEvent The event message to handle.
     */
    void OnMessage(
        const Network::Twitch::Extensions::Heat::Event& pEvent) override;

public:
    /**
     * @brief Twitch Heat Extension User Click event handler.
     *
     * This handler is invoked when a user clicks the video player on the broadcaster's channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::Extensions::Heat::Events::UserClickEvent& pEvent);
};

} // Network::Twitch::Extensions::Heat

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_EVENT_HANDLER_HPP_INCLUDED