///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension EventHandler Implementation                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/Extensions/Heat/Event.hpp>
#include <Network/Twitch/Extensions/Heat/EventHandler.hpp>

namespace Network::Twitch::Extensions::Heat
{

void EventHandler::OnMessage(
    const Network::Twitch::Extensions::Heat::Event& pEvent)
{
    pEvent.Dispatch(*this);
}

void EventHandler::OnEvent(
    const Network::Twitch::Extensions::Heat::Events::UserClickEvent&)
{}

} // Network::Twitch::Extensions::Heat