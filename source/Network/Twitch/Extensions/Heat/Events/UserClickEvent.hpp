#ifndef NETWORK_TWITCH_EXTENSIONS_HEAT_USER_CLICK_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EXTENSIONS_HEAT_USER_CLICK_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension UserClickEvent Interface                                                //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/Extensions/Heat/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::Extensions::Heat::Events
{

/**
 * @brief Twitch Heat Extension User Click event.
 *
 * This event is published when a user clicks the video player on the broadcaster's channel.
 */
struct UserClickEvent : public Network::Twitch::Extensions::Heat::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::Extensions::Heat::EventHandler& pHandler) const override;

    /**
     * @brief The user id or an opaque value.
     */
    std::string UserId;

    /**
     * @brief The normalized x coordinate of the cursor in the video player.
     */
    float CursorX;

    /**
     * @brief The normalized y coordinate of the cursor in the video player.
     */
    float CursorY;
};

} // Network::Twitch::Extensions::Heat::Events

#endif // NETWORK_TWITCH_EXTENSIONS_HEAT_USER_CLICK_EVENT_HPP_INCLUDED