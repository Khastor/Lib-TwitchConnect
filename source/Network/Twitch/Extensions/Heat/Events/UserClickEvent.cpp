///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch Heat Extension UserClickEvent Implementation                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/Extensions/Heat/EventHandler.hpp>
#include <Network/Twitch/Extensions/Heat/Events/UserClickEvent.hpp>

namespace Network::Twitch::Extensions::Heat::Events
{

void UserClickEvent::Dispatch(
    Network::Twitch::Extensions::Heat::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::Extensions::Heat::Events