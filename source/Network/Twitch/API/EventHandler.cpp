///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API EventHandler Implementation                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>
#include <Network/Twitch/API/EventHandler.hpp>

namespace Network::Twitch::API
{

void EventHandler::OnMessage(
    const Network::Twitch::API::Event& pEvent)
{
    pEvent.Dispatch(*this);
}

void EventHandler::OnEvent(
    const Network::Twitch::API::Events::SendChatAnnouncementEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::API::Events::SendChatAnnouncementError&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::API::Events::CreateEventSubSubscriptionEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::API::Events::CreateEventSubSubscriptionError&)
{}

} // Network::Twitch::API