#ifndef NETWORK_TWITCH_API_SERVICE_HPP_INCLUDED
#define NETWORK_TWITCH_API_SERVICE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Service Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Logging.hpp>
#include <Network/Twitch/API/CommandHandler.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <thread>

namespace Network::Twitch::API
{

/**
 * @brief Twitch API service.
 *
 * Process asynchronous Twitch API sessions upon reception of Twitch API commands.
 */
class Service : public Network::Twitch::API::CommandHandler
{
private:
    /**
     * @brief Constructor.
     *
     * Start receiving Twitch API commands and processing Twitch API sessions.
     */
    Service();

public:
    /**
     * @brief Destructor.
     *
     * Stop receiving Twitch API commands and processing Twitch API sessions.
     */
    ~Service();

    /**
     * @brief Deleted copy-constructor.
     */
    Service(const Service&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Service(Service&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Service& operator==(const Service&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Service& operator==(Service&&) = delete;

    /**
     * @brief Initialize the Twitch API service.
     *
     * Start receiving Twitch API commands and processing Twitch API sessions.
     */
    static void Initialize();

private:
    /**
     * @brief Worker thread main loop.
     *
     * Invoke asynchronous Twitch API HTTP session handlers.
     * The main loop is started upon construction, and stopped upon destruction.
     */
    void Loop();

    /**
     * @brief Twitch API Send Chat Announcement command handler.
     *
     * Start a Twitch API Send Chat Announcement session.
     *
     * @param pCommand The command to handle.
     */
    void OnCommand(
        const Network::Twitch::API::Commands::SendChatAnnouncementCommand& pCommand) override;

    /**
     * @brief Twitch API Create EventSub Subscription command handler.
     *
     * Start a Twitch API Create EventSub Subscription session.
     *
     * @param pCommand The command to handle.
     */
    void OnCommand(
        const Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand& pCommand) override;

    /**
     * @brief The Twitch API service log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief The IO context.
     */
    boost::asio::io_context mIOContext;

    /**
     * @brief The IO context work guard.
     *
     * Prevent the IO context from running out of work.
     */
    boost::asio::executor_work_guard<boost::asio::io_context::executor_type> mIOWorkGuard;

    /**
     * @brief Worker thread.
     *
     * Invoke asynchronous Twitch API HTTP session handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;

    /**
     * @brief Subscription to Twitch API Command messages.
     */
    Network::Core::Dispatcher::Subscription mTwitchAPICommandSubscription;
};

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_SERVICE_HPP_INCLUDED