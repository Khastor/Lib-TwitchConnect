///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionError Implementation                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionError.hpp>

namespace Network::Twitch::API::Events
{

CreateEventSubSubscriptionError::CreateEventSubSubscriptionError(
    std::uint64_t pCommandId,
    std::string pMessage,
    bool pFatal) :
    Error(pCommandId, std::move(pMessage), pFatal)
{}

void CreateEventSubSubscriptionError::Dispatch(
    Network::Twitch::API::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::API::Events