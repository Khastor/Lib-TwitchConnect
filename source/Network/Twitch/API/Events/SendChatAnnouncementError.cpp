///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementError Implementation                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/API/Events/SendChatAnnouncementError.hpp>

namespace Network::Twitch::API::Events
{

SendChatAnnouncementError::SendChatAnnouncementError(
    std::uint64_t pCommandId,
    std::string pMessage,
    bool pFatal) :
    Error(pCommandId, std::move(pMessage), pFatal)
{}

void SendChatAnnouncementError::Dispatch(
    Network::Twitch::API::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::API::Events