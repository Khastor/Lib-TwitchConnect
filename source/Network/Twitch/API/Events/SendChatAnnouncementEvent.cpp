///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementEvent Implementation                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/API/Events/SendChatAnnouncementEvent.hpp>

namespace Network::Twitch::API::Events
{

SendChatAnnouncementEvent::SendChatAnnouncementEvent(
    std::uint64_t pCommandId) :
    Event(pCommandId)
{}

void SendChatAnnouncementEvent::Dispatch(
    Network::Twitch::API::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::API::Events