#ifndef NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_ERROR_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_ERROR_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionError Interface                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API::Events
{

/**
 * @brief Twitch API Create EventSub Subscription error.
 *
 * This event is published upon failure of a Twitch API Create EventSub Subscription command.
 */
struct CreateEventSubSubscriptionError : public Network::Twitch::API::Error
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     * @param pMessage The error message.
     * @param pFatal True if the source command cannot continue because of a fatal error.
     */
    CreateEventSubSubscriptionError(
        std::uint64_t pCommandId,
        std::string pMessage,
        bool pFatal);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::API::EventHandler& pHandler) const override;
};

} // Network::Twitch::API::Events

#endif // NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_ERROR_HPP_INCLUDED