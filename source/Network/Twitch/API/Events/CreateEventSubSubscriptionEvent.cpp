///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionEvent Implementation                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionEvent.hpp>

namespace Network::Twitch::API::Events
{

CreateEventSubSubscriptionEvent::CreateEventSubSubscriptionEvent(
    std::uint64_t pCommandId) :
    Event(pCommandId)
{}

void CreateEventSubSubscriptionEvent::Dispatch(
    Network::Twitch::API::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::API::Events