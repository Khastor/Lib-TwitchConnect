#ifndef NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementEvent Interface                                                //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API::Events
{

/**
 * @brief Twitch API Send Chat Announcement event.
 *
 * This event is published upon successful completion of a Twitch API Send Chat Announcement command.
 */
struct SendChatAnnouncementEvent: public Network::Twitch::API::Event
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     */
    SendChatAnnouncementEvent(
        std::uint64_t pCommandId);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::API::EventHandler& pHandler) const override;
};

} // Network::Twitch::API::Events

#endif // NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_EVENT_HPP_INCLUDED