#ifndef NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_ERROR_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_ERROR_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementError Interface                                                //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API::Events
{

/**
 * @brief Twitch API Send Chat Announcement error.
 *
 * This event is published upon failure of a Twitch API Send Chat Announcement command.
 */
struct SendChatAnnouncementError: public Network::Twitch::API::Error
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     * @param pMessage The error message.
     * @param pFatal True if the source command cannot continue because of a fatal error.
     */
    SendChatAnnouncementError(
        std::uint64_t pCommandId,
        std::string pMessage,
        bool pFatal);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::API::EventHandler& pHandler) const override;
};

} // Network::Twitch::API::Events

#endif // NETWORK_TWITCH_API_EVENTS_SEND_CHAT_ANNOUNCEMENT_ERROR_HPP_INCLUDED