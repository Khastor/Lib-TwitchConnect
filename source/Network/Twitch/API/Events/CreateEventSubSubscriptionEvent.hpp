#ifndef NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionEvent Interface                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API::Events
{

/**
 * @brief Twitch API Create EventSub Subscription event.
 *
 * This event is published upon successful completion of a Twitch API Create EventSub Subscription command.
 */
struct CreateEventSubSubscriptionEvent: public Network::Twitch::API::Event
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     */
    CreateEventSubSubscriptionEvent(
        std::uint64_t pCommandId);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::API::EventHandler& pHandler) const override;
};

} // Network::Twitch::API::Events

#endif // NETWORK_TWITCH_API_EVENTS_CREATE_EVENT_SUB_SUBSCRIPTION_EVENT_HPP_INCLUDED