///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Service Implementation                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Command.hpp>
#include <Network/Twitch/API/Commands/CreateEventSubSubscriptionCommand.hpp>
#include <Network/Twitch/API/Commands/SendChatAnnouncementCommand.hpp>
#include <Network/Twitch/API/Service.hpp>
#include <Network/Twitch/API/Sessions/CreateEventSubSubscriptionSession.hpp>
#include <Network/Twitch/API/Sessions/SendChatAnnouncementSession.hpp>

namespace Network::Twitch::API
{

const std::string LOG_CHANNEL{ "Twitch API" };
const std::string LOG_FILENAME{ "TwitchAPIServiceLogs.log" };

Service::Service() :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME)),
    mIOWorkGuard(boost::asio::make_work_guard(mIOContext)),
    mWorker([this]() { Loop(); }),
    mTwitchAPICommandSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::API::Command>(this))
{}

Service::~Service()
{
    mTwitchAPICommandSubscription.Revoke();
    mIOWorkGuard.reset();
    mIOContext.stop();
}

void Service::Initialize()
{
    static Service sInstance;
}

void Service::Loop()
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start";
    while (mIOWorkGuard.owns_work())
    {
        try
        {
            mIOContext.run();
        }
        catch (std::exception& lException)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lException.what();
        }
    }
    NETWORK_CORE_LOG_INFO(mLogger) << "stop";
}

void Service::OnCommand(
    const Network::Twitch::API::Commands::SendChatAnnouncementCommand& pCommand)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start session: Send Chat Announcement";
    std::make_shared<Network::Twitch::API::Sessions::SendChatAnnouncementSession>(
        mLogger,
        mIOContext,
        pCommand.CommandId,
        pCommand.Message,
        pCommand.Color)->Start();
}

void Service::OnCommand(
    const Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand& pCommand)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start session: Create EventSub Subscription";
    std::make_shared<Network::Twitch::API::Sessions::CreateEventSubSubscriptionSession>(
        mLogger,
        mIOContext,
        pCommand.CommandId,
        pCommand.Timeout,
        pCommand.WebsocketSessionId,
        pCommand.SubscriptionType,
        pCommand.SubscriptionVersion,
        pCommand.SubscriptionCondition)->Start();
}

} // Network::Twitch::API