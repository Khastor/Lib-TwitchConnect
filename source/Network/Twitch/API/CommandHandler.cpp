///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CommandHandler Implementation                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Command.hpp>
#include <Network/Twitch/API/CommandHandler.hpp>

namespace Network::Twitch::API
{

void CommandHandler::OnMessage(
    const Network::Twitch::API::Command& pCommand)
{
    pCommand.Dispatch(*this);
}

void CommandHandler::OnCommand(
    const Network::Twitch::API::Commands::SendChatAnnouncementCommand&)
{}

void CommandHandler::OnCommand(
    const Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand&)
{}

} // Network::Twitch::API