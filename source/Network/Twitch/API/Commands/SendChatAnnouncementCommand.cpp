///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementCommand Implementation                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/CommandHandler.hpp>
#include <Network/Twitch/API/Commands/SendChatAnnouncementCommand.hpp>

namespace Network::Twitch::API::Commands
{

SendChatAnnouncementCommand::SendChatAnnouncementCommand(
    const std::string& pMessage,
    const std::string& pColor) :
    Message(pMessage),
    Color(pColor)
{}

void SendChatAnnouncementCommand::Dispatch(
    Network::Twitch::API::CommandHandler& pHandler) const
{
    pHandler.OnCommand(*this);
}

} // Network::Twitch::API::Commands