///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionCommand Implementation                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/CommandHandler.hpp>
#include <Network/Twitch/API/Commands/CreateEventSubSubscriptionCommand.hpp>

namespace Network::Twitch::API::Commands
{

CreateEventSubSubscriptionCommand::CreateEventSubSubscriptionCommand(
    const std::string& pWebsocketSessionId,
    const std::string& pSubscriptionType,
    const std::string& pSubscriptionVersion,
    const boost::json::value& pSubscriptionCondition,
    std::uint64_t pTimeout) :
    WebsocketSessionId(pWebsocketSessionId),
    SubscriptionType(pSubscriptionType),
    SubscriptionVersion(pSubscriptionVersion),
    SubscriptionCondition(pSubscriptionCondition),
    Timeout(pTimeout)
{}

void CreateEventSubSubscriptionCommand::Dispatch(
    Network::Twitch::API::CommandHandler& pHandler) const
{
    pHandler.OnCommand(*this);
}

} // Network::Twitch::API::Commands