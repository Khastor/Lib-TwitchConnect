#ifndef NETWORK_TWITCH_API_COMMANDS_SEND_CHAT_ANNOUNCEMENT_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_API_COMMANDS_SEND_CHAT_ANNOUNCEMENT_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementCommand Interface                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Command.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Commands
{

/**
 * @brief Twitch API Send Chat Announcement command.
 *
 * Publish this command to call for the Twitch API service to send a Twitch API Send Chat Announcement request.
 * The Twitch API service will publish a Twitch API Send Chat Announcement event or error upon completion.
 *
 * @see https://dev.twitch.tv/docs/api/reference/#send-chat-announcement.
 */
struct SendChatAnnouncementCommand : public Network::Twitch::API::Command
{
    /**
     * @brief Constructor.
     */
    SendChatAnnouncementCommand() = default;

    /**
     * @brief Constructor.
     *
     * @param pMessage The announcement message.
     * @param pColor The announcement highlight color.
     */
    SendChatAnnouncementCommand(
        const std::string& pMessage,
        const std::string& pColor);

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    void Dispatch(
        Network::Twitch::API::CommandHandler& pHandler) const override;

    /**
     * @brief The announcement message.
     */
    std::string Message;

    /**
     * @brief The announcement highlight color.
     */
    std::string Color;
};

} // Network::Twitch::API::Commands

#endif // NETWORK_TWITCH_API_COMMANDS_SEND_CHAT_ANNOUNCEMENT_COMMAND_HPP_INCLUDED