#ifndef NETWORK_TWITCH_API_COMMANDS_CREATE_EVENT_SUB_SUBSCRIPTION_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_API_COMMANDS_CREATE_EVENT_SUB_SUBSCRIPTION_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionCommand Interface                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Command.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/json.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Commands
{

/**
 * @brief Twitch API Create EventSub Subscription command.
 *
 * Publish this command to call for the Twitch API service to send a Twitch API Create EventSub Subscription request.
 * The Twitch API service will publish a Twitch API Create EventSub Subscription event or error upon completion.
 *
 * @see https://dev.twitch.tv/docs/api/reference/#create-eventsub-subscription.
 */
struct CreateEventSubSubscriptionCommand : public Network::Twitch::API::Command
{
    /**
     * @brief Constructor.
     */
    CreateEventSubSubscriptionCommand() = default;

    /**
     * @brief Constructor.
     *
     * @param pWebsocketSessionId The unique identifier of the websocket session that creates the subscription.
     * @param pSubscriptionType The type of subscription to create.
     * @param pSubscriptionVersion The version number of the subscription to create.
     * @param pSubscriptionCondition The parameter values that are specific to the subscription type.
     * @param pTimeout The command timeout, in seconds.
     */
    CreateEventSubSubscriptionCommand(
        const std::string& pWebsocketSessionId,
        const std::string& pSubscriptionType,
        const std::string& pSubscriptionVersion,
        const boost::json::value& pSubscriptionCondition,
        std::uint64_t pTimeout);

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    void Dispatch(
        Network::Twitch::API::CommandHandler& pHandler) const override;

    /**
     * @brief The unique identifier of the websocket session that creates the subscription.
     */
    std::string WebsocketSessionId;

    /**
     * @brief The type of subscription to create.
     */
    std::string SubscriptionType;

    /**
     * @brief The version number of the subscription to create.
     */
    std::string SubscriptionVersion;

    /**
     * @brief The parameter values that are specific to the subscription type.
     */
    boost::json::value SubscriptionCondition;

    /**
     * @brief The command timeout, in seconds.
     */
    std::uint64_t Timeout;
};

} // Network::Twitch::API::Commands

#endif // NETWORK_TWITCH_API_COMMANDS_CREATE_EVENT_SUB_SUBSCRIPTION_COMMAND_HPP_INCLUDED