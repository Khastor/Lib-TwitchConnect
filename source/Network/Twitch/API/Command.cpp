///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Command Implementation                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Command.hpp>

namespace Network::Twitch::API
{

namespace
{

std::uint64_t sNextCommandId{ 0ULL };

} // anonymous namespace

Command::Command() :
    CommandId(sNextCommandId++)
{}

} // Network::Twitch::API