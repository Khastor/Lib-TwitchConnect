///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Event Implementation                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////
 
// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API
{

Event::Event(
    std::uint64_t pCommandId) :
    CommandId(pCommandId)
{}

Error::Error(
    std::uint64_t pCommandId,
    std::string pMessage,
    bool pFatal) :
    Event(pCommandId),
    Message(std::move(pMessage)),
    Fatal(pFatal)
{}

} // Network::Twitch::API