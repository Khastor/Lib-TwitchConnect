#ifndef NETWORK_TWITCH_API_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_API_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Command Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>

namespace Network::Twitch::API
{

class CommandHandler;

/**
 * @brief Base class in the hierarchy of Twitch API commands.
 */
struct Command : public Network::Core::Message
{
    /**
     * @brief Constructor.
     */
    Command();

    /**
     * @brief Destructor.
     */
    virtual ~Command() = default;

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    virtual void Dispatch(
        CommandHandler& pHandler) const = 0;

    /**
     * @brief The unique identifier of the command.
     */
    const std::uint64_t CommandId;
};

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_COMMAND_HPP_INCLUDED