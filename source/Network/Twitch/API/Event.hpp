#ifndef NETWORK_TWITCH_API_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Event Interface                                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::API
{

class EventHandler;

/**
 * @brief Base class in the hierarchy of Twitch API events.
 */
struct Event : public Network::Core::Message
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     */
    Event(
        std::uint64_t pCommandId);

    /**
     * @brief Destructor.
     */
    virtual ~Event() = default;

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    virtual void Dispatch(
        EventHandler& pHandler) const = 0;

    /**
     * @brief The unique identifier of the source command.
     */
    const std::uint64_t CommandId;
};

/**
 * @brief Base class in the hierarchy of Twitch API errors.
 */
struct Error : public Event
{
    /**
     * @brief Constructor.
     *
     * @param pCommandId The unique identifier of the source command.
     * @param pMessage The error message.
     * @param pFatal True if the source command cannot continue because of a fatal error.
     */
    Error(
        std::uint64_t pCommandId,
        std::string pMessage,
        bool pFatal);

    /**
     * @brief The error message.
     */
    const std::string Message;

    /**
     * @brief True if the source command cannot continue because of a fatal error.
     */
    const bool Fatal;
};

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_EVENT_HPP_INCLUDED