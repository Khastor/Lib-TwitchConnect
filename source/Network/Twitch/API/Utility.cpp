///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Utility Implementation                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/API/Constants.hpp>
#include <Network/Twitch/API/Utility.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <chrono>
#include <sstream>

namespace Network::Twitch::API
{

std::optional<std::uint64_t> GetRetryDelay(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    try
    {
        const std::chrono::seconds lRateLimitResetTimestamp{ std::stoull(pHTTPResponse[Network::Twitch::API::Keys::HTTP_HEADER_RATELIMIT_RESET]) };
        const std::chrono::system_clock::time_point lRateLimitResetTime{ lRateLimitResetTimestamp };
        return std::max(0LL, std::chrono::duration_cast<std::chrono::seconds>(lRateLimitResetTime - std::chrono::system_clock::now()).count());
    }
    catch (...)
    {
        return {};
    }
}

std::optional<std::string> GetErrorMessage(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    boost::property_tree::ptree lHTTPResponseJSON;
    try
    {
        std::stringstream lHTTPResponseBody(pHTTPResponse.body());
        boost::property_tree::read_json(lHTTPResponseBody, lHTTPResponseJSON);
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::API::Keys::HTTP_ERROR_CONTENT_MESSAGE) })
        {
            return lErrorMessage.value();
        }
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::API::Keys::HTTP_ERROR_CONTENT_REASON) })
        {
            return lErrorMessage.value();
        }
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::API::Keys::HTTP_ERROR_CONTENT_STATUS) })
        {
            return lErrorMessage.value();
        }
        return {};
    }
    catch (...)
    {
        return {};
    }
}

} // Network::Twitch::API