#ifndef NETWORK_TWITCH_API_COMMAND_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_API_COMMAND_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CommandHandler Interface                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/API/Command.hpp>

namespace Network::Twitch::API
{

namespace Commands
{

struct CreateEventSubSubscriptionCommand;
struct SendChatAnnouncementCommand;

} // Commands

/**
 * @brief Twitch API command handler.
 *
 * Provide Twitch API command handler methods that can be invoked asynchronously.
 */
class CommandHandler : public Network::Core::Observer<Network::Twitch::API::Command>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the command message to the appropriate command handler method.
     *
     * @param pCommand The command message to dispatch.
     */
    void OnMessage(
        const Network::Twitch::API::Command& pCommand) override;

public:
    /**
     * @brief Twitch API Send Chat Announcement command handler.
     *
     * @param pCommand The command to handle.
     */
    virtual void OnCommand(
        const Network::Twitch::API::Commands::SendChatAnnouncementCommand& pCommand);

    /**
     * @brief Twitch API Create EventSub Subscription command handler.
     *
     * @param pCommand The command to handle.
     */
    virtual void OnCommand(
        const Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand& pCommand);
};

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_COMMAND_HANDLER_HPP_INCLUDED