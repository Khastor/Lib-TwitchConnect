#ifndef NETWORK_TWITCH_API_UTILITY_HPP_INCLUDED
#define NETWORK_TWITCH_API_UTILITY_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Utility Interface                                                                  //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/beast/http.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <optional>
#include <string>

namespace Network::Twitch::API
{

/**
 * @brief Get the retry delay in seconds after an HTTP error response.
 *
 * @param pHTTPResponse The HTTP error response.
 * @return The retry delay in seconds, or empty if the retry delay is not specified in the response.
 */
std::optional<std::uint64_t> GetRetryDelay(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

/**
 * @brief Get the detailed error message after an HTTP error response.
 *
 * @param pHTTPResponse The HTTP error response.
 * @return The detailed error message, or empty if the error message is not specified in the response.
 */
std::optional<std::string> GetErrorMessage(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_UTILITY_HPP_INCLUDED