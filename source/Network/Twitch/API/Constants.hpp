#ifndef NETWORK_TWITCH_API_CONSTANTS_HPP_INCLUDED
#define NETWORK_TWITCH_API_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API Endpoints Definitions                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Endpoints
{

/**
 * @brief Twitch API remote host name and port.
 */
inline const std::string HOSTNAME{ "api.twitch.tv:443" };

/**
 * @brief Twitch API announcements endpoint.
 */
inline const std::string ANNOUNCEMENTS{ "/helix/chat/announcements" };

/**
 * @brief Twitch API EventSub subscriptions endpoint.
 */
inline const std::string EVENTSUB_SUBSCRIPTIONS{ "/helix/eventsub/subscriptions" };

} // Network::Twitch::API::Endpoints

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API HTTP Header Keys Definitions                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Keys
{

/**
 * @brief Twitch API HTTP header key "Client-Id".
 */
inline const std::string HTTP_HEADER_CLIENT_ID{ "Client-Id" };

/**
 * @brief Twitch API HTTP header key "Ratelimit-Limit".
 *
 * @see https://dev.twitch.tv/docs/api/guide/#twitch-rate-limits.
 */
inline const std::string HTTP_HEADER_RATELIMIT_LIMIT{ "Ratelimit-Limit" };

/**
 * @brief Twitch API HTTP header key "Ratelimit-Remaining".
 *
 * @see https://dev.twitch.tv/docs/api/guide/#twitch-rate-limits.
 */
inline const std::string HTTP_HEADER_RATELIMIT_REMAINING{ "Ratelimit-Remaining" };

/**
 * @brief Twitch API HTTP header key "Ratelimit-Reset".
 *
 * @see https://dev.twitch.tv/docs/api/guide/#twitch-rate-limits.
 */
inline const std::string HTTP_HEADER_RATELIMIT_RESET{ "Ratelimit-Reset" };

} // Network::Twitch::API::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API HTTP Query Parameter Keys Definitions                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Keys
{

/**
 * @brief Twitch API HTTP query parameter key "broadcaster_id".
 */
inline const std::string HTTP_PARAMETER_BROADCASTER_ID{ "broadcaster_id" };

/**
 * @brief Twitch API HTTP query parameter key "moderator_id".
 */
inline const std::string HTTP_PARAMETER_MODERATOR_ID{ "moderator_id" };

} // Network::Twitch::API::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API HTTP Content Keys Definitions                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::API::Keys
{

/**
 * @brief Twitch API HTTP content key "color".
 */
inline const std::string HTTP_CONTENT_COLOR{ "color" };

/**
 * @brief Twitch API HTTP content key "condition".
 */
inline const std::string HTTP_CONTENT_CONDITION{ "condition" };

/**
 * @brief Twitch API HTTP content key "condition.broadcaster_user_id".
 */
inline const std::string HTTP_CONTENT_CONDITION_BROADCASTER_USER_ID{ "broadcaster_user_id" };

/**
 * @brief Twitch API HTTP content key "condition.from_broadcaster_user_id".
 */
inline const std::string HTTP_CONTENT_CONDITION_FROM_BROADCASTER_USER_ID{ "from_broadcaster_user_id" };

/**
 * @brief Twitch API HTTP content key "condition.reward_id".
 */
inline const std::string HTTP_CONTENT_CONDITION_REWARD_ID{ "reward_id" };

/**
 * @brief Twitch API HTTP content key "condition.to_broadcaster_user_id".
 */
inline const std::string HTTP_CONTENT_CONDITION_TO_BROADCASTER_USER_ID{ "to_broadcaster_user_id" };

/**
 * @brief Twitch API HTTP content key "message".
 */
inline const std::string HTTP_CONTENT_MESSAGE{ "message" };

/**
 * @brief Twitch API HTTP content key "transport".
 */
inline const std::string HTTP_CONTENT_TRANSPORT{ "transport" };

/**
 * @brief Twitch API HTTP content key "transport.method".
 */
inline const std::string HTTP_CONTENT_TRANSPORT_METHOD{ "method" };

/**
 * @brief Twitch API HTTP content key "transport.method" enumerator value "websocket".
 */
inline const std::string HTTP_CONTENT_TRANSPORT_METHOD_WEBSOCKET{ "websocket" };

/**
 * @brief Twitch API HTTP content key "transport.session_id".
 */
inline const std::string HTTP_CONTENT_TRANSPORT_SESSION_ID{ "session_id" };

/**
 * @brief Twitch API HTTP content key "type".
 */
inline const std::string HTTP_CONTENT_TYPE{ "type" };

/**
 * @brief Twitch API HTTP content key "version".
 */
inline const std::string HTTP_CONTENT_VERSION{ "version" };

/**
 * @brief Twitch API HTTP error content key "message".
 *
 * The detailed HTTP error message.
 */
inline const std::string HTTP_ERROR_CONTENT_MESSAGE{ "message" };

/**
 * @brief Twitch API HTTP error content key "error".
 *
 * The generic HTTP error reason.
 */
inline const std::string HTTP_ERROR_CONTENT_REASON{ "error" };

/**
 * @brief Twitch API HTTP error content key "status".
 *
 * The generic HTTP error status.
 */
inline const std::string HTTP_ERROR_CONTENT_STATUS{ "status" };

} // Network::Twitch::API::Keys

#endif // NETWORK_TWITCH_API_CONSTANTS_HPP_INCLUDED