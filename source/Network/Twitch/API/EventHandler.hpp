#ifndef NETWORK_TWITCH_API_EVENT_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_API_EVENT_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API EventHandler Interface                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/API/Event.hpp>

namespace Network::Twitch::API
{

namespace Events
{

struct CreateEventSubSubscriptionEvent;
struct CreateEventSubSubscriptionError;
struct SendChatAnnouncementError;
struct SendChatAnnouncementEvent;

} // Events

/**
 * @brief Twitch API event handler.
 *
 * Provide Twitch API event handler methods that can be invoked asynchronously.
 */
class EventHandler : public Network::Core::Observer<Network::Twitch::API::Event>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the event message to the appropriate event handler method.
     *
     * @param pEvent The event message to handle.
     */
    void OnMessage(
        const Network::Twitch::API::Event& pEvent) override;

public:
    /**
     * @brief Twitch API Send Chat Announcement event handler.
     *
     * This handler is invoked when the Twitch API Send Chat Announcement command terminates successfully.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::API::Events::SendChatAnnouncementEvent& pEvent);

    /**
     * @brief Twitch API Send Chat Announcement error handler.
     *
     * This handler is invoked when the Twitch API Send Chat Announcement command produces an error.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::API::Events::SendChatAnnouncementError& pEvent);

    /**
     * @brief Twitch API Create EventSub Subscription event handler.
     *
     * This handler is invoked when the Twitch API Create EventSub Subscription command terminates successfully.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::API::Events::CreateEventSubSubscriptionEvent& pEvent);

    /**
     * @brief Twitch API Create EventSub Subscription error handler.
     *
     * This handler is invoked when the Twitch API Create EventSub Subscription command produces an error.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::API::Events::CreateEventSubSubscriptionError& pEvent);
};

} // Network::Twitch::API

#endif // NETWORK_TWITCH_API_EVENT_HANDLER_HPP_INCLUDED