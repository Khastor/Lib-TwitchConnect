///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionSession Implementation                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/HTTPConstants.hpp>
#include <Network/Core/HTTPEncoding.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Constants.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionError.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionEvent.hpp>
#include <Network/Twitch/API/Sessions/CreateEventSubSubscriptionSession.hpp>
#include <Network/Twitch/API/Utility.hpp>
#include <Network/Twitch/OAuth2/Commands/ValidateAccessTokensCommand.hpp>

namespace Network::Twitch::API::Sessions
{

const std::string LOG_PREFIX{ "[CreateEventSubSubscriptionSession] " };

CreateEventSubSubscriptionSession::CreateEventSubSubscriptionSession(
    Network::Core::Logger& pLogger,
    boost::asio::io_context& pIOContext,
    std::uint64_t pCommandId,
    std::uint64_t pTimeout,
    const std::string& pWebsocketSessionId,
    const std::string& pSubscriptionType,
    const std::string& pSubscriptionVersion,
    const boost::json::value& pSubscriptionCondition) :
    Network::Core::HTTPSession(pLogger, pIOContext),
    mLogger(pLogger),
    mCommandId(pCommandId),
    mWebsocketSessionId(pWebsocketSessionId),
    mSubscriptionType(pSubscriptionType),
    mSubscriptionVersion(pSubscriptionVersion),
    mSubscriptionCondition(pSubscriptionCondition)
{
    SetRetryTimeout(pTimeout);
}

boost::beast::http::request<boost::beast::http::string_body> CreateEventSubSubscriptionSession::BuildHTTPRequest()
{
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    const boost::json::value lHTTPRequestBody{
        { Network::Twitch::API::Keys::HTTP_CONTENT_TYPE, mSubscriptionType },
        { Network::Twitch::API::Keys::HTTP_CONTENT_VERSION, mSubscriptionVersion },
        { Network::Twitch::API::Keys::HTTP_CONTENT_CONDITION, mSubscriptionCondition },
        { Network::Twitch::API::Keys::HTTP_CONTENT_TRANSPORT,
            {
                { Network::Twitch::API::Keys::HTTP_CONTENT_TRANSPORT_METHOD, Network::Twitch::API::Keys::HTTP_CONTENT_TRANSPORT_METHOD_WEBSOCKET },
                { Network::Twitch::API::Keys::HTTP_CONTENT_TRANSPORT_SESSION_ID, mWebsocketSessionId }
            }
        }
    };

    boost::beast::http::request<boost::beast::http::string_body> lHTTPRequest = {};
    lHTTPRequest.method(boost::beast::http::verb::post);
    lHTTPRequest.target(Network::Twitch::API::Endpoints::EVENTSUB_SUBSCRIPTIONS);
    lHTTPRequest.set(boost::beast::http::field::host, Network::Twitch::API::Endpoints::HOSTNAME);
    lHTTPRequest.set(boost::beast::http::field::authorization, Network::Core::HTTP_AUTHORIZATION_BEARER + lSettings.GetTwitchUserAccessToken());
    lHTTPRequest.set(boost::beast::http::field::content_type, Network::Core::HTTP_CONTENT_TYPE_APPLICATION_JSON);
    lHTTPRequest.set(Network::Twitch::API::Keys::HTTP_HEADER_CLIENT_ID, lSettings.GetTwitchClientId());
    lHTTPRequest.body() = boost::json::serialize(lHTTPRequestBody);
    lHTTPRequest.prepare_payload();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "request:\n" << lHTTPRequest;
    return lHTTPRequest;
}

void CreateEventSubSubscriptionSession::OnStartError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "aborted: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

void CreateEventSubSubscriptionSession::OnResolveError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

void CreateEventSubSubscriptionSession::OnConnectError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

void CreateEventSubSubscriptionSession::OnHandshakeError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "handshake error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

void CreateEventSubSubscriptionSession::OnWriteError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network write error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

void CreateEventSubSubscriptionSession::OnReadError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(mCommandId, pError, pFatal));
}

bool CreateEventSubSubscriptionSession::OnHTTPResponse(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "response: " << pHTTPResponse.result();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "response:\n" << pHTTPResponse;
    switch (pHTTPResponse.result())
    {
    case boost::beast::http::status::accepted:
        return OnHTTPResponseAccepted(pHTTPResponse);
    case boost::beast::http::status::bad_request:
        return OnHTTPResponseBadRequest(pHTTPResponse);
    case boost::beast::http::status::unauthorized:
        return OnHTTPResponseUnauthorized(pHTTPResponse);
    case boost::beast::http::status::forbidden:
        return OnHTTPResponseForbidden(pHTTPResponse);
    case boost::beast::http::status::conflict:
        return OnHTTPResponseConflict(pHTTPResponse);
    case boost::beast::http::status::too_many_requests:
        return OnHTTPResponseTooManyRequests(pHTTPResponse);
    }
    return OnHTTPResponseError(pHTTPResponse);
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseAccepted(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionEvent>(mCommandId));
    return true;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseBadRequest(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    const bool lFatalError{ true };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseUnauthorized(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the authentication service to update the tokens and retry.
    const bool lFatalError{ !HasRetryBudget() };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseForbidden(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the authentication service to update the tokens and retry.
    const bool lFatalError{ !HasRetryBudget() };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseConflict(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    const bool lFatalError{ true };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseTooManyRequests(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the rate-limit bucket to be reset and retry.
    const bool lFatalError{ !HasRetryBudget() };
    if (const std::optional<std::uint64_t>& lRetryDelay{ Network::Twitch::API::GetRetryDelay(pHTTPResponse) })
    {
        SetRetryDelay(lRetryDelay.value());
    }
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool CreateEventSubSubscriptionSession::OnHTTPResponseError(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the problem to magically disappear and retry.
    const bool lFatalError{ !HasRetryBudget() };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::CreateEventSubSubscriptionError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

} // Network::Twitch::API::Sessions