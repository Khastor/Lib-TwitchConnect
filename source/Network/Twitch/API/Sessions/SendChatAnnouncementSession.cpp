///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementSession Implementation                                         //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/HTTPConstants.hpp>
#include <Network/Core/HTTPEncoding.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Constants.hpp>
#include <Network/Twitch/API/Events/SendChatAnnouncementError.hpp>
#include <Network/Twitch/API/Events/SendChatAnnouncementEvent.hpp>
#include <Network/Twitch/API/Sessions/SendChatAnnouncementSession.hpp>
#include <Network/Twitch/API/Utility.hpp>
#include <Network/Twitch/OAuth2/Commands/ValidateAccessTokensCommand.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/json.hpp>

namespace Network::Twitch::API::Sessions
{

const std::string LOG_PREFIX{ "[SendChatAnnouncementSession] " };
constexpr std::uint64_t RETRY_TIMEOUT_SECONDS{ 60ULL };

SendChatAnnouncementSession::SendChatAnnouncementSession(
    Network::Core::Logger& pLogger,
    boost::asio::io_context& pIOContext,
    std::uint64_t pCommandId,
    const std::string& pMessage,
    const std::string& pColor) :
    Network::Core::HTTPSession(pLogger, pIOContext),
    mLogger(pLogger),
    mCommandId(pCommandId),
    mMessage(pMessage),
    mColor(pColor)
{
    SetRetryTimeout(RETRY_TIMEOUT_SECONDS);
}

boost::beast::http::request<boost::beast::http::string_body> SendChatAnnouncementSession::BuildHTTPRequest()
{
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    const std::vector<std::pair<std::string, std::string>> lHTTPRequestQuery{
        { Network::Twitch::API::Keys::HTTP_PARAMETER_BROADCASTER_ID, lSettings.GetTwitchUserId() },
        { Network::Twitch::API::Keys::HTTP_PARAMETER_MODERATOR_ID, lSettings.GetTwitchUserId() }
    };

    const boost::json::value lHTTPRequestBody{
        { Network::Twitch::API::Keys::HTTP_CONTENT_MESSAGE, mMessage },
        { Network::Twitch::API::Keys::HTTP_CONTENT_COLOR, mColor }
    };

    boost::beast::http::request<boost::beast::http::string_body> lHTTPRequest = {};
    lHTTPRequest.method(boost::beast::http::verb::post);
    lHTTPRequest.target(Network::Core::BuildURLWithQueryString(Network::Twitch::API::Endpoints::ANNOUNCEMENTS, lHTTPRequestQuery));
    lHTTPRequest.set(boost::beast::http::field::host, Network::Twitch::API::Endpoints::HOSTNAME);
    lHTTPRequest.set(boost::beast::http::field::authorization, Network::Core::HTTP_AUTHORIZATION_BEARER + lSettings.GetTwitchUserAccessToken());
    lHTTPRequest.set(boost::beast::http::field::content_type, Network::Core::HTTP_CONTENT_TYPE_APPLICATION_JSON);
    lHTTPRequest.set(Network::Twitch::API::Keys::HTTP_HEADER_CLIENT_ID, lSettings.GetTwitchClientId());
    lHTTPRequest.body() = boost::json::serialize(lHTTPRequestBody);
    lHTTPRequest.prepare_payload();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "request:\n" << lHTTPRequest;
    return lHTTPRequest;
}

void SendChatAnnouncementSession::OnStartError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "aborted: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

void SendChatAnnouncementSession::OnResolveError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

void SendChatAnnouncementSession::OnConnectError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

void SendChatAnnouncementSession::OnHandshakeError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "handshake error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

void SendChatAnnouncementSession::OnWriteError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network write error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

void SendChatAnnouncementSession::OnReadError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(mCommandId, pError, pFatal));
}

bool SendChatAnnouncementSession::OnHTTPResponse(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "response: " << pHTTPResponse.result();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "response:\n" << pHTTPResponse;
    switch (pHTTPResponse.result())
    {
    case boost::beast::http::status::no_content:
        return OnHTTPResponseNoContent(pHTTPResponse);
    case boost::beast::http::status::bad_request:
        return OnHTTPResponseBadRequest(pHTTPResponse);
    case boost::beast::http::status::unauthorized:
        return OnHTTPResponseUnauthorized(pHTTPResponse);
    case boost::beast::http::status::too_many_requests:
        return OnHTTPResponseTooManyRequests(pHTTPResponse);
    }
    return OnHTTPResponseError(pHTTPResponse);
}

bool SendChatAnnouncementSession::OnHTTPResponseNoContent(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementEvent>(mCommandId));
    return true;
}

bool SendChatAnnouncementSession::OnHTTPResponseBadRequest(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    const bool lFatalError{ true };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool SendChatAnnouncementSession::OnHTTPResponseUnauthorized(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the authentication service to update the tokens and retry.
    const bool lFatalError{ !HasRetryBudget() };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool SendChatAnnouncementSession::OnHTTPResponseTooManyRequests(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the rate-limit bucket to be reset and retry.
    const bool lFatalError{ !HasRetryBudget() };
    if (const std::optional<std::uint64_t>& lRetryDelay{ Network::Twitch::API::GetRetryDelay(pHTTPResponse) })
    {
        SetRetryDelay(lRetryDelay.value());
    }
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

bool SendChatAnnouncementSession::OnHTTPResponseError(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the problem to magically disappear and retry.
    const bool lFatalError{ !HasRetryBudget() };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::API::Events::SendChatAnnouncementError>(
            mCommandId, Network::Twitch::API::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()), lFatalError));
    return lFatalError;
}

} // Network::Twitch::API::Sessions