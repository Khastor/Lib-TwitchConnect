#ifndef NETWORK_TWITCH_API_SESSIONS_CREATE_EVENT_SUB_SUBSCRIPTION_SESSION_HPP_INCLUDED
#define NETWORK_TWITCH_API_SESSIONS_CREATE_EVENT_SUB_SUBSCRIPTION_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API CreateEventSubSubscriptionSession Interface                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPSession.hpp>
#include <Network/Core/Logging.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/json.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::API::Sessions
{

/**
 * @brief Twitch API Create EventSub Subscription HTTP session.
 *
 * Creates an EventSub subscription.
 *
 * @see https://dev.twitch.tv/docs/api/reference/#create-eventsub-subscription.
 */
class CreateEventSubSubscriptionSession : public Network::Core::HTTPSession
{
public:
    /**
     * @brief Constructor.
     *
     * This class must be allocated dynamically using std::make_shared().
     *
     * @param pLogger The log source.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     * @param pCommandId The unique identifier of the source command.
     * @param pTimeout The command timeout, in seconds.
     * @param pWebsocketSessionId The unique identifier of the websocket session that creates the subscription.
     * @param pSubscriptionType The type of subscription to create.
     * @param pSubscriptionVersion The version number of the subscription to create.
     * @param pSubscriptionCondition The parameter values that are specific to the subscription type.
     */
    CreateEventSubSubscriptionSession(
        Network::Core::Logger& pLogger,
        boost::asio::io_context& pIOContext,
        std::uint64_t pCommandId,
        std::uint64_t pTimeout,
        const std::string& pWebsocketSessionId,
        const std::string& pSubscriptionType,
        const std::string& pSubscriptionVersion,
        const boost::json::value& pSubscriptionCondition);

private:
    /**
     * @brief Build the HTTP request.
     *
     * The HTTP request header must contain the host field with host name and port information.
     *
     * @return The HTTP request.
     */
    boost::beast::http::request<boost::beast::http::string_body> BuildHTTPRequest() override;

    /**
     * @brief Start error handler.
     *
     * This handler is invoked when the session fails to start.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnStartError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Resolve error handler.
     *
     * This handler is invoked when the session fails to find the remote host address.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnResolveError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Connect error handler.
     *
     * This handler is invoked when the session fails to connect to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnConnectError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate a secure connection with the remote host.
     * This can happen if the client cannot verify the SSL certificate chain of the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnHandshakeError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Write error handler.
     *
     * This handler is invoked when the session fails to send the HTTP request to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnWriteError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the session fails to read the HTTP response from the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnReadError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief HTTP response handler.
     *
     * This handler is invoked when the session receives the HTTP response from the remote host.
     * If the session has no retry budget, it will be terminated and ignore the retry request.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponse(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse) override;

    /**
     * @brief HTTP response handler for status Accepted.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseAccepted(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Bad Request.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseBadRequest(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Unauthorized.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseUnauthorized(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Forbidden.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseForbidden(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Conflict.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseConflict(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Too Many Requests.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseTooManyRequests(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for unexpected status.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseError(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The unique identifier of the source command.
     */
    const std::uint64_t mCommandId;

    /**
     * @brief The unique identifier of the websocket session that creates the subscription.
     */
    const std::string mWebsocketSessionId;

    /**
     * @brief The type of subscription to create.
     */
    const std::string mSubscriptionType;

    /**
     * @brief The version number of the subscription to create.
     */
    const std::string mSubscriptionVersion;

    /**
     * @brief The parameter values that are specific to the subscription type.
     */
    const boost::json::value mSubscriptionCondition;
};

} // Network::Twitch::API::Sessions

#endif // NETWORK_TWITCH_API_SESSIONS_CREATE_EVENT_SUB_SUBSCRIPTION_SESSION_HPP_INCLUDED