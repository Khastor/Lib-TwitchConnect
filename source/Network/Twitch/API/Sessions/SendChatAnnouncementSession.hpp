#ifndef NETWORK_TWITCH_API_SESSIONS_SEND_CHAT_ANNOUNCEMENT_SESSION_HPP_INCLUDED
#define NETWORK_TWITCH_API_SESSIONS_SEND_CHAT_ANNOUNCEMENT_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch API SendChatAnnouncementSession Interface                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPSession.hpp>
#include <Network/Core/Logging.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::API::Sessions
{

/**
 * @brief Twitch API Send Chat Announcement HTTP session.
 *
 * Sends an announcement to the broadcaster's chat room.
 *
 * @see https://dev.twitch.tv/docs/api/reference/#send-chat-announcement.
 */
class SendChatAnnouncementSession: public Network::Core::HTTPSession
{
public:
    /**
     * @brief Constructor.
     *
     * This class must be allocated dynamically using std::make_shared().
     *
     * @param pLogger The log source.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     * @param pCommandId The unique identifier of the source command.
     * @param pMessage The announcement message.
     * @param pColor The announcement color.
     */
    SendChatAnnouncementSession(
        Network::Core::Logger& pLogger,
        boost::asio::io_context& pIOContext,
        std::uint64_t pCommandId,
        const std::string& pMessage,
        const std::string& pColor);

private:
    /**
     * @brief Build the HTTP request.
     *
     * The HTTP request header must contain the host field with host name and port information.
     *
     * @return The HTTP request.
     */
    boost::beast::http::request<boost::beast::http::string_body> BuildHTTPRequest() override;

    /**
     * @brief Start error handler.
     *
     * This handler is invoked when the session fails to start.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnStartError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Resolve error handler.
     *
     * This handler is invoked when the session fails to find the remote host address.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnResolveError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Connect error handler.
     *
     * This handler is invoked when the session fails to connect to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnConnectError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate a secure connection with the remote host.
     * This can happen if the client cannot verify the SSL certificate chain of the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnHandshakeError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Write error handler.
     *
     * This handler is invoked when the session fails to send the HTTP request to the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnWriteError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the session fails to read the HTTP response from the remote host.
     *
     * @param pError The error message.
     * @param pFatal True if the session cannot retry.
     */
    void OnReadError(
        const std::string& pError,
        bool pFatal) override;

    /**
     * @brief HTTP response handler.
     *
     * This handler is invoked when the session receives the HTTP response from the remote host.
     * If the session has no retry budget, it will be terminated and ignore the retry request.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponse(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse) override;

    /**
     * @brief HTTP response handler for status No Content.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseNoContent(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Bad Request.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseBadRequest(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Unauthorized.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseUnauthorized(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for status Too Many Requests.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseTooManyRequests(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief HTTP response handler for unexpected status.
     *
     * @param pHTTPResponse The HTTP response.
     * @return True to terminate the session, false to request a retry.
     */
    bool OnHTTPResponseError(
        const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The unique identifier of the source command.
     */
    const std::uint64_t mCommandId;

    /**
     * @brief The announcement message.
    */
    const std::string mMessage;

    /**
     * @brief The announcement color.
    */
    const std::string mColor;
};

} // Network::Twitch::API::Sessions

#endif // NETWORK_TWITCH_API_SESSIONS_SEND_CHAT_ANNOUNCEMENT_SESSION_HPP_INCLUDED