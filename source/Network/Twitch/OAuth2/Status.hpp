#ifndef NETWORK_TWITCH_OAUTH2_STATUS_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_STATUS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Service Status Definition                                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

namespace Network::Twitch::OAuth2
{

/**
 * @brief Twitch OAuth2 service status.
 */
enum class Status
{
    /**
     * @brief The Twitch OAuth2 service has been granted access tokens.
     */
    Authorized,

    /**
     * @brief The Twitch OAuth2 service has not been granted access tokens.
     */
    Unauthorized,

    /**
     * @brief The Twitch OAuth2 service is processing an access token grant flow.
     */
    Busy
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_STATUS_HPP_INCLUDED