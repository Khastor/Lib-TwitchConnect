#ifndef NETWORK_TWITCH_OAUTH2_CONSTANTS_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Endpoints Definitions                                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Endpoints
{

/**
 * @brief Twitch OAuth2 remote host name and port.
 */
inline const std::string HOSTNAME{ "id.twitch.tv:443" };

/**
 * @brief Twitch OAuth2 authorize endpoint full URL.
 */
inline const std::string AUTHORIZE{ "https://id.twitch.tv/oauth2/authorize" };

/**
 * @brief Twitch OAuth2 token endpoint.
 */
inline const std::string TOKEN{ "/oauth2/token" };

/**
 * @brief Twitch OAuth2 validate endpoint.
 */
inline const std::string VALIDATE{ "/oauth2/validate" };

} // Network::Twitch::OAuth2::Endpoints

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 HTTP Query Parameter Keys Definitions                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Keys
{

/**
 * @brief Twitch OAuth2 HTTP query parameter key "client_id".
 */
inline const std::string HTTP_PARAMETER_CLIENT_ID{ "client_id" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "client_secret".
 */
inline const std::string HTTP_PARAMETER_CLIENT_SECRET{ "client_secret" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "code".
 */
inline const std::string HTTP_PARAMETER_CODE{ "code" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "error_description".
 */
inline const std::string HTTP_PARAMETER_ERROR_DESCRIPTION{ "error_description" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "grant_type".
 */
inline const std::string HTTP_PARAMETER_GRANT_TYPE{ "grant_type" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "grant_type" enumerator value "authorization_code".
 */
inline const std::string HTTP_PARAMETER_GRANT_TYPE_AUTHORIZATION_CODE{ "authorization_code" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "grant_type" enumerator value "client_credentials".
 */
inline const std::string HTTP_PARAMETER_GRANT_TYPE_CLIENT_CREDENTIALS{ "client_credentials" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "grant_type" enumerator value "refresh_token".
 */
inline const std::string HTTP_PARAMETER_GRANT_TYPE_REFRESH_TOKEN{ "refresh_token" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "redirect_uri".
 */
inline const std::string HTTP_PARAMETER_REDIRECT_URI{ "redirect_uri" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "refresh_token".
 */
inline const std::string HTTP_PARAMETER_REFRESH_TOKEN{ "refresh_token" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "response_type".
 */
inline const std::string HTTP_PARAMETER_RESPONSE_TYPE{ "response_type" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "response_type" enumerator value "code".
 */
inline const std::string HTTP_PARAMETER_RESPONSE_TYPE_CODE{ "code" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "scope".
 */
inline const std::string HTTP_PARAMETER_SCOPE{ "scope" };

/**
 * @brief Twitch OAuth2 HTTP query parameter key "state".
 */
inline const std::string HTTP_PARAMETER_STATE{ "state" };

} // Network::Twitch::OAuth2::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 HTTP Content Keys Definitions                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Keys
{

/**
 * @brief Twitch OAuth2 HTTP content key "access_token".
 */
inline const std::string HTTP_CONTENT_ACCESS_TOKEN{ "access_token" };

/**
 * @brief Twitch OAuth2 HTTP content key "client_id".
 */
inline const std::string HTTP_CONTENT_CLIENT_ID{ "client_id" };

/**
 * @brief Twitch OAuth2 HTTP content key "expires_in".
 */
inline const std::string HTTP_CONTENT_EXPIRES_IN{ "expires_in" };

/**
 * @brief Twitch OAuth2 HTTP content key "login".
 */
inline const std::string HTTP_CONTENT_LOGIN{ "login" };

/**
 * @brief Twitch OAuth2 HTTP content key "refresh_token".
 */
inline const std::string HTTP_CONTENT_REFRESH_TOKEN{ "refresh_token" };

/**
 * @brief Twitch OAuth2 HTTP content key "scope".
 */
inline const std::string HTTP_CONTENT_SCOPE{ "scope" };

/**
 * @brief Twitch OAuth2 HTTP content key "scopes".
 */
inline const std::string HTTP_CONTENT_SCOPES{ "scopes" };

/**
 * @brief Twitch OAuth2 HTTP content key "user_id".
 */
inline const std::string HTTP_CONTENT_USER_ID{ "user_id" };

/**
 * @brief Twitch OAuth2 HTTP error content key "message".
 *
 * The detailed HTTP error message.
 */
inline const std::string HTTP_ERROR_CONTENT_MESSAGE{ "message" };

/**
 * @brief Twitch OAuth2 HTTP error content key "error".
 *
 * The generic HTTP error reason.
 */
inline const std::string HTTP_ERROR_CONTENT_REASON{ "error" };

/**
 * @brief Twitch OAuth2 HTTP error content key "status".
 *
 * The generic HTTP error status.
 */
inline const std::string HTTP_ERROR_CONTENT_STATUS{ "status" };

} // Network::Twitch::OAuth2::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Scopes Definitions                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Scopes
{

/**
 * @brief Twitch OAuth2 scope "channel:moderate".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string CHANNEL_MODERATE{ "channel:moderate" };

/**
 * @brief Twitch OAuth2 scope "channel:read:ads".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string CHANNEL_READ_ADS{ "channel:read:ads" };

/**
 * @brief Twitch OAuth2 scope "channel:read:redemptions".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string CHANNEL_READ_REDEMPTIONS{ "channel:read:redemptions" };

/**
 * @brief Twitch OAuth2 scope "chat:edit"
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string CHAT_EDIT{ "chat:edit" };

/**
 * @brief Twitch OAuth2 scope "chat:read".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string CHAT_READ{ "chat:read" };

/**
 * @brief Twitch OAuth2 scope "moderator:manage:announcements".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string MODERATOR_MANAGE_ANNOUNCEMENTS{ "moderator:manage:announcements" };

/**
 * @brief Twitch OAuth2 scope "user:read:chat".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string USER_READ_CHAT{ "user:read:chat" };

/**
 * @brief Twitch OAuth2 scope "user:write:chat".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string USER_WRITE_CHAT{ "user:write:chat" };

/**
 * @brief Twitch OAuth2 scope "whispers:edit".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string WHISPERS_EDIT{ "whispers:edit" };

/**
 * @brief Twitch OAuth2 scope "whispers:read".
 *
 * @see https://dev.twitch.tv/docs/authentication/scopes/.
 */
inline const std::string WHISPERS_READ{ "whispers:read" };

} // Network::Twitch::OAuth2::Scopes

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Errors Definitions                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Errors
{

/**
 * @brief Twitch OAuth2 error message for abort errors.
 */
inline const std::string SESSION_ABORTED{ "Aborted." };

/**
 * @brief Twitch OAuth2 error message for hostname resolution errors.
 */
inline const std::string SESSION_RESOLVE{ "Hostname resolution error." };

/**
 * @brief Twitch OAuth2 error message for connection errors.
 */
inline const std::string SESSION_CONNECT{ "Connection error." };

/**
 * @brief Twitch OAuth2 error message for handshake errors.
 */
inline const std::string SESSION_HANDSHAKE{ "Handshake error." };

/**
 * @brief Twitch OAuth2 error message for network write errors.
 */
inline const std::string SESSION_WRITE{ "Network write error." };

/**
 * @brief Twitch OAuth2 error message for network read errors.
 */
inline const std::string SESSION_READ{ "Network read error." };

/**
 * @brief Twitch OAuth2 error message for session response parsing errors.
 */
inline const std::string BAD_CONTENT{ "Bad content." };

/**
 * @brief Twitch OAuth2 error message for a user access token that does not contain the required access scopes.
 */
inline const std::string BAD_ACCESS_SCOPES{ "User access token does not contain the required access scopes." };

/**
 * @brief Twitch OAuth2 error message for an access token that does not match the expected client id.
 */
inline const std::string BAD_CLIENT_ID{ "Access token does not match the expected client id." };

/**
 * @brief Twitch OAuth2 error message for a user access token that does not match the expected user id.
 */
inline const std::string BAD_USER_ID{ "User access token does not match the expected user id." };

/**
 * @brief Twitch OAuth2 error message for a user access token that does not match the expected user login.
 */
inline const std::string BAD_USER_LOGIN{ "User access token does not match the expected user login." };

} // Network::Twitch::OAuth2::Errors

#endif // NETWORK_TWITCH_OAUTH2_CONSTANTS_HPP_INCLUDED