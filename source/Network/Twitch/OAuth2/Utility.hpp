#ifndef NETWORK_TWITCH_OAUTH2_UTILITY_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_UTILITY_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Utility Interface                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/beast/http.hpp>
#include <boost/property_tree/ptree.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <optional>
#include <set>
#include <string>

namespace Network::Twitch::OAuth2
{

/**
 * @brief Get the detailed error message after an HTTP error response.
 *
 * @param pHTTPResponse The HTTP error response.
 * @return The detailed error message, or empty if the error message is not specified in the response.
 */
std::optional<std::string> GetErrorMessage(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse);

/**
 * @brief Get all the values under a property tree node.
 *
 * @param pNode The property tree node.
 * @return All the values under the property tree node.
 */
std::set<std::string> GetAllValues(
    const boost::property_tree::ptree& pNode);

/**
 * @brief Generate a random 64-bit hexadecimal nonce.
 *
 * @return The random 64-bit hexadecimal nonce.
 */
std::string GenerateNonce();

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_UTILITY_HPP_INCLUDED