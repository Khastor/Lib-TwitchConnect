///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 EventHandler Implementation                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Event.hpp>
#include <Network/Twitch/OAuth2/EventHandler.hpp>

namespace Network::Twitch::OAuth2
{

void EventHandler::OnMessage(
    const Network::Twitch::OAuth2::Event& pEvent)
{
    pEvent.Dispatch(*this);
}

void EventHandler::OnEvent(
    const Network::Twitch::OAuth2::Events::AuthorizationRequestEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::OAuth2::Events::StatusChangeEvent&)
{}

} // Network::Twitch::OAuth2