///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Service Implementation                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPEncoding.hpp>
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Event.hpp>
#include <Network/Twitch/OAuth2/Events/AuthorizationRequestEvent.hpp>
#include <Network/Twitch/OAuth2/Events/StatusChangeEvent.hpp>
#include <Network/Twitch/OAuth2/Service.hpp>
#include <Network/Twitch/OAuth2/Utility.hpp>
#include <Network/Twitch/OAuth2/Sessions/GetAppAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Sessions/GetAuthorizationCodeCallbackSession.hpp>
#include <Network/Twitch/OAuth2/Sessions/GetUserAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Sessions/RefreshUserAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Sessions/ValidateAppAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Sessions/ValidateUserAccessTokenSession.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/algorithm/string.hpp>
#include <boost/bind/bind.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>

namespace Network::Twitch::OAuth2
{

const std::string LOG_CHANNEL{ "Twitch OAuth2" };
const std::string LOG_FILENAME{ "TwitchOAuth2ServiceLogs.log" };
constexpr std::uint64_t HEARTBEAT_DEFAULT_TIMEOUT_SECONDS{ 3600ULL };
constexpr std::uint64_t HEARTBEAT_RETRY_TIMEOUT_SECONDS{ 60ULL };

Service::Service() :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME)),
    mIOContext(std::make_unique<boost::asio::io_context>()),
    mCallbackListener(std::make_unique<boost::asio::ip::tcp::acceptor>(*mIOContext)),
    mHeartbeatTimer(std::make_unique<boost::asio::steady_timer>(*mIOContext)),
    mWorker([this](std::stop_token pStopToken) { Loop(pStopToken); }),
    mTwitchOAuth2CommandSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::OAuth2::Command>(this))
{}

Service::~Service()
{
    std::unique_lock<std::mutex> lLock(mMutex);
    mTwitchOAuth2CommandSubscription.Revoke();
    mWorker.request_stop();
    mIOContext->stop();
}

void Service::Initialize()
{
    static Service sInstance;
}

void Service::Loop(
    std::stop_token pStopToken)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start";

    boost::asio::ip::tcp::endpoint lLocalOAuth2Endpoint(
        boost::asio::ip::make_address("127.0.0.1"),
        Network::Core::Settings::Instance().GetTwitchOAuth2CallbackPort());

    while (!pStopToken.stop_requested())
    {
        SetStatus(Network::Twitch::OAuth2::Status::Busy);
        if (mRestartCount > 0)
        {
            const uint64_t lDelay{ 1ULL << std::min(mRestartCount, 5ULL) };
            NETWORK_CORE_LOG_INFO(mLogger) << "restart #" << mRestartCount << " in " << lDelay << "s";
            std::this_thread::sleep_for(std::chrono::seconds(lDelay));
        }
        mRestartCount++;

        boost::beast::error_code lError;
        mCallbackListener->open(lLocalOAuth2Endpoint.protocol(), lError);
        if (lError)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "callback acceptor open error: " << lError.message();
            mCallbackListener->close(lError);
            continue;
        }

        mCallbackListener->set_option(boost::asio::socket_base::reuse_address(true), lError);
        if (lError)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "callback acceptor option error: " << lError.message();
            mCallbackListener->close(lError);
            continue;
        }

        mCallbackListener->bind(lLocalOAuth2Endpoint, lError);
        if (lError)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "callback acceptor bind error: " << lError.message();
            mCallbackListener->close(lError);
            continue;
        }

        mCallbackListener->listen(boost::asio::socket_base::max_listen_connections, lError);
        if (lError)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "callback acceptor listen error: " << lError.message();
            mCallbackListener->close(lError);
            continue;
        }

        mCallbackListener->async_accept(*mIOContext,
            boost::beast::bind_front_handler(&Service::OnCallbackConnectHandler, this));

        {
            std::unique_lock<std::mutex> lLock(mMutex);
            mHeartbeatTimer->expires_after(std::chrono::seconds(HEARTBEAT_DEFAULT_TIMEOUT_SECONDS));
            mHeartbeatTimer->async_wait(boost::beast::bind_front_handler(
                &Service::OnHeartbeatTimeout, this));
        }

        std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateAppAccessTokenSession>(
            mLogger, *this, *mIOContext)->Start();

        try
        {
            mRestartCount = 0;
            mIOContext->run();
        }
        catch (std::exception& lException)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lException.what();
        }

        {
            std::unique_lock<std::mutex> lLock(mMutex);

            // Cancel outstanding asynchronous operations.
            mCallbackListener->close(lError);
            if (lError)
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << "callback acceptor close error: " << lError.message();
            }
            mCallbackListener.reset();

            mHeartbeatTimer->cancel();
            mHeartbeatTimer.reset();

            // Hard reset the IO context to discard all waiting handlers.
            mIOContext = std::make_unique<boost::asio::io_context>();
            mCallbackListener = std::make_unique<boost::asio::ip::tcp::acceptor>(*mIOContext);
            mHeartbeatTimer = std::make_unique<boost::asio::steady_timer>(*mIOContext);
        }
    }

    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized);
    NETWORK_CORE_LOG_INFO(mLogger) << "stop";
}

void Service::SetStatus(
    Network::Twitch::OAuth2::Status pStatus,
    const std::string& pMessage)
{
    std::unique_lock<std::mutex> lLock(mMutex);
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Events::StatusChangeEvent>(mStatus = pStatus, pMessage));
}

Twitch::OAuth2::Status Service::GetStatus()
{
    std::unique_lock<std::mutex> lLock(mMutex);
    return mStatus;
}

void Service::SetNextHeartbeatTimeout(
    std::uint64_t pTimeout)
{
    std::unique_lock<std::mutex> lLock(mMutex);
    const auto lCurrentTimePoint{ std::chrono::steady_clock::now() };
    if (mHeartbeatTimer->expiry() <= lCurrentTimePoint ||
        mHeartbeatTimer->expiry() >= lCurrentTimePoint + std::chrono::seconds(pTimeout))
    {
        // Set the new closest timeout and try to cancel the timer before it expires.
        if (mHeartbeatTimer->expires_after(std::chrono::seconds(pTimeout)) > 0)
        {
            mHeartbeatTimer->async_wait(
                boost::beast::bind_front_handler(&Service::OnHeartbeatTimeout, this));
        }
    }
}

void Service::RequestUserAuthorization()
{
    mNonce = Network::Twitch::OAuth2::GenerateNonce();

    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    const std::vector<std::pair<std::string, std::string>> lHTTPRequestQuery{
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_RESPONSE_TYPE, Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_RESPONSE_TYPE_CODE },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_CLIENT_ID, lSettings.GetTwitchClientId() },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_REDIRECT_URI, lSettings.GetTwitchOAuth2CallbackURI() },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_SCOPE, Network::Core::URLEncoded(boost::algorithm::join(lSettings.GetTwitchUserAccessScopes(), " ")) },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_STATE, mNonce }
    };

    const std::string lAuthorizationURL{ Network::Core::BuildURLWithQueryString(Network::Twitch::OAuth2::Endpoints::AUTHORIZE, lHTTPRequestQuery) };
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Events::AuthorizationRequestEvent>(lAuthorizationURL));

    NETWORK_CORE_LOG_INFO(mLogger) << "paste this link into your web browser to authorize the app:\n" << lAuthorizationURL;
}

void Service::OnCallbackConnectHandler(
    boost::beast::error_code pError,
    boost::asio::ip::tcp::socket pSocket)
{
    if (pError)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "accept error: " << pError.message();
        mIOContext->stop();
        return;
    }

    std::make_shared<Network::Twitch::OAuth2::Sessions::GetAuthorizationCodeCallbackSession>(
        mLogger, *this, std::move(pSocket))->Start();

    mCallbackListener->async_accept(*mIOContext,
        boost::beast::bind_front_handler(&Service::OnCallbackConnectHandler, this));
}

void Service::OnHeartbeatTimeout(
    boost::system::error_code pError)
{
    if (!pError)
    {
        NETWORK_CORE_LOG_INFO(mLogger) << "token validation heartbeat";
        {
            std::unique_lock<std::mutex> lLock(mMutex);
            const auto lCurrentTimePoint{ std::chrono::steady_clock::now() };
            if (mHeartbeatTimer->expiry() <= lCurrentTimePoint ||
                mHeartbeatTimer->expiry() >= lCurrentTimePoint + std::chrono::seconds(HEARTBEAT_DEFAULT_TIMEOUT_SECONDS))
            {
                mHeartbeatTimer->expires_after(std::chrono::seconds(HEARTBEAT_DEFAULT_TIMEOUT_SECONDS));
            }
            mHeartbeatTimer->async_wait(boost::beast::bind_front_handler(
                &Service::OnHeartbeatTimeout, this));
        }

        switch (GetStatus())
        {
        case Network::Twitch::OAuth2::Status::Authorized:
        case Network::Twitch::OAuth2::Status::Unauthorized:
            SetStatus(Network::Twitch::OAuth2::Status::Busy);
            std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateAppAccessTokenSession>(
                mLogger, *this, *mIOContext)->Start();
            break;
        case Network::Twitch::OAuth2::Status::Busy:
            NETWORK_CORE_LOG_WARNING(mLogger) << "skipped token validation heartbeat: service is already busy";
            break;
        }
    }
    else if (pError != boost::asio::error::operation_aborted)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "token validation heartbeat error: " << pError.message();
        mIOContext->stop();
        return;
    }
}

void Service::OnValidateAppAccessTokenSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error validating the app access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateAppAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnValidateAppAccessTokenResponseError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error validating the app access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnValidateAppAccessTokenUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "error validating the app access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::GetAppAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnValidateAppAccessTokenSuccess(
    std::int64_t pAppAccessTokenTimeout)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "app access token expires in " << pAppAccessTokenTimeout << "s";
    SetNextHeartbeatTimeout(pAppAccessTokenTimeout);
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnGetAppAccessTokenSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting app access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateAppAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnGetAppAccessTokenResponseError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting app access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetAppAccessTokenUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting app access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetAppAccessTokenSuccess(
    const std::string& pAppAccessToken,
    std::int64_t pAppAccessTokenTimeout)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "app access token expires in " << pAppAccessTokenTimeout << "s";
    Network::Core::Settings::Instance().SetTwitchAppAccessToken(pAppAccessToken);
    Network::Core::Settings::Instance().SaveToFile();
    SetNextHeartbeatTimeout(pAppAccessTokenTimeout);
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnValidateUserAccessTokenSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error validating user access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnValidateUserAccessTokenResponseError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error validating user access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnValidateUserAccessTokenUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "error validating user access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::RefreshUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnValidateUserAccessTokenSuccess(
    std::int64_t pUserAccessTokenTimeout)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user access token expires in " << pUserAccessTokenTimeout << "s";
    SetNextHeartbeatTimeout(pUserAccessTokenTimeout);
    SetStatus(Network::Twitch::OAuth2::Status::Authorized);
}

void Service::OnRefreshUserAccessTokenSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error refreshing user access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnRefreshUserAccessTokenResponseError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error refreshing user access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnRefreshUserAccessTokenUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "error refreshing user access token: " << pError;
    RequestUserAuthorization();
}

void Service::OnRefreshUserAccessTokenSuccess(
    const std::string& pUserAccessToken,
    const std::string& pUserRefreshToken,
    std::uint64_t pUserAccessTokenTimeout)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user access token expires in " << pUserAccessTokenTimeout << "s";
    Network::Core::Settings::Instance().SetTwitchUserAccessToken(pUserAccessToken, pUserRefreshToken);
    Network::Core::Settings::Instance().SaveToFile();
    SetNextHeartbeatTimeout(pUserAccessTokenTimeout);
    SetStatus(Network::Twitch::OAuth2::Status::Authorized);
}

bool Service::OnGetAuthorizationCodeCheckState(
    const std::string& pState)
{
    if (!mNonce.empty() && pState == mNonce)
    {
        mNonce.clear();
        return true;
    }
    return false;
}

void Service::OnGetAuthorizationCodeSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error waiting for user authorization: " << pError;
    mNonce.clear();
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnGetAuthorizationCodeRequestError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error waiting for user authorization: " << pError;
    mNonce.clear();
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetAuthorizationCodeUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error waiting for user authorization: " << pError;
    mNonce.clear();
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetAuthorizationCodeSuccess(
    const std::string& pAuthorizationCode)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user authorization granted";
    mNonce.clear();
    std::make_shared<Network::Twitch::OAuth2::Sessions::GetUserAccessTokenSession>(
        mLogger, *this, *mIOContext, pAuthorizationCode)->Start();
}

void Service::OnGetUserAccessTokenSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting user access token: " << pError;
    std::make_shared<Network::Twitch::OAuth2::Sessions::ValidateUserAccessTokenSession>(
        mLogger, *this, *mIOContext)->Start();
}

void Service::OnGetUserAccessTokenResponseError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting user access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetUserAccessTokenUnauthorized(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << "error getting user access token: " << pError;
    SetNextHeartbeatTimeout(HEARTBEAT_RETRY_TIMEOUT_SECONDS);
    SetStatus(Network::Twitch::OAuth2::Status::Unauthorized, pError);
}

void Service::OnGetUserAccessTokenSuccess(
    const std::string& pUserAccessToken,
    const std::string& pUserRefreshToken,
    std::uint64_t pUserAccessTokenTimeout)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "user access token expires in " << pUserAccessTokenTimeout << "s";
    Network::Core::Settings::Instance().SetTwitchUserAccessToken(pUserAccessToken, pUserRefreshToken);
    Network::Core::Settings::Instance().SaveToFile();
    SetNextHeartbeatTimeout(pUserAccessTokenTimeout);
    SetStatus(Network::Twitch::OAuth2::Status::Authorized);
}

void Service::OnCommand(
    const Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand& pCommand)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "token validation request";
    switch (GetStatus())
    {
    case Network::Twitch::OAuth2::Status::Authorized:
    case Network::Twitch::OAuth2::Status::Unauthorized:
        SetNextHeartbeatTimeout(0ULL);
        break;
    case Network::Twitch::OAuth2::Status::Busy:
        NETWORK_CORE_LOG_WARNING(mLogger) << "skipped token validation request: service is already busy";
        break;
    }
}

} // Network::Twitch::OAuth2