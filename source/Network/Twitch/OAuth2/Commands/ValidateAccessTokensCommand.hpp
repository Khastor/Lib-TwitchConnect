#ifndef NETWORK_TWITCH_OAUTH2_COMMANDS_VALIDATE_ACCESS_TOKENS_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_COMMANDS_VALIDATE_ACCESS_TOKENS_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 ValidateAccessTokensCommand Interface                                           //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Command.hpp>

namespace Network::Twitch::OAuth2::Commands
{

/**
 * @brief Twitch OAuth2 Validate Access Tokens command.
 *
 * Publish this command to call for the Twitch OAuth2 service to validate and refresh application and user access tokens.
 */
struct ValidateAccessTokensCommand : public Network::Twitch::OAuth2::Command
{
    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    void Dispatch(
        Network::Twitch::OAuth2::CommandHandler& pHandler) const override;
};

} // Network::Twitch::OAuth2::Commands

#endif // NETWORK_TWITCH_OAUTH2_COMMANDS_VALIDATE_ACCESS_TOKENS_COMMAND_HPP_INCLUDED