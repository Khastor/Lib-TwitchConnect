///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 ValidateAccessTokensCommand Implementation                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Commands/ValidateAccessTokensCommand.hpp>
#include <Network/Twitch/OAuth2/CommandHandler.hpp>

namespace Network::Twitch::OAuth2::Commands
{

void ValidateAccessTokensCommand::Dispatch(
    Network::Twitch::OAuth2::CommandHandler& pHandler) const
{
    pHandler.OnCommand(*this);
}

} // Network::Twitch::OAuth2::Commands