///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 StatusChangeEvent Implementation                                                //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Events/StatusChangeEvent.hpp>
#include <Network/Twitch/OAuth2/EventHandler.hpp>

namespace Network::Twitch::OAuth2::Events
{

StatusChangeEvent::StatusChangeEvent(
    Network::Twitch::OAuth2::Status pCode,
    const std::string& pMessage) :
    Code(pCode),
    Message(pMessage)
{}

void StatusChangeEvent::Dispatch(
    Network::Twitch::OAuth2::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
};

} // Network::Twitch::OAuth2::Events