#ifndef NETWORK_TWITCH_OAUTH2_EVENTS_AUTHORIZATION_REQUEST_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_EVENTS_AUTHORIZATION_REQUEST_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 AuthorizationRequestEvent Interface                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Events
{

/**
 * @brief Twitch OAuth2 Authorization Request event.
 *
 * This event is published when the Twitch OAuth2 service requires user authorization.
 * The user must navigate to the authorization request URL to authorize the application.
 *
 * @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#authorization-code-grant-flow.
 */
struct AuthorizationRequestEvent : public Network::Twitch::OAuth2::Event
{
    /**
     * @brief Constructor.
     *
     * @param pURL The Twitch OAuth2 authorization request URL.
     */
    AuthorizationRequestEvent(
        const std::string& pURL);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::OAuth2::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch OAuth2 authorization request URL.
     */
    const std::string URL;
};

} // Network::Twitch::OAuth2::Events

#endif // NETWORK_TWITCH_OAUTH2_EVENTS_AUTHORIZATION_REQUEST_EVENT_HPP_INCLUDED