///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 AuthorizationRequestEvent Implementation                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Events/AuthorizationRequestEvent.hpp>
#include <Network/Twitch/OAuth2/EventHandler.hpp>

namespace Network::Twitch::OAuth2::Events
{

AuthorizationRequestEvent::AuthorizationRequestEvent(
    const std::string& pURL) :
    URL(pURL)
{}

void AuthorizationRequestEvent::Dispatch(
    Network::Twitch::OAuth2::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
};

} // Network::Twitch::OAuth2::Events