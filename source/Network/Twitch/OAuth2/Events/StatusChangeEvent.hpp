#ifndef NETWORK_TWITCH_OAUTH2_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 StatusChangeEvent Interface                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Event.hpp>
#include <Network/Twitch/OAuth2/Status.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::OAuth2::Events
{

/**
 * @brief Twitch OAuth2 service Status Change event.
 *
 * This event is published when the Twitch OAuth2 service status changes.
 */
struct StatusChangeEvent : public Network::Twitch::OAuth2::Event
{
    /**
     * @brief Constructor.
     *
     * @param pMessage The Twitch OAuth2 service status message.
     * @param pCode The Twitch OAuth2 service status code.
     */
    StatusChangeEvent(
        Network::Twitch::OAuth2::Status pCode,
        const std::string& pMessage);

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::OAuth2::EventHandler& pHandler) const override;

    /**
     * @brief The Twitch OAuth2 service status code.
     */
    const Network::Twitch::OAuth2::Status Code;

    /**
     * @brief The Twitch OAuth2 service status message.
     */
    const std::string Message;
};

} // Network::Twitch::OAuth2::Events

#endif // NETWORK_TWITCH_OAUTH2_EVENTS_STATUS_CHANGE_EVENT_HPP_INCLUDED