#ifndef NETWORK_TWITCH_OAUTH2_SERVICE_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_SERVICE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Service Interface                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Logging.hpp>
#include <Network/Twitch/OAuth2/Workflows/AuthorizationCodeGrantFlow.hpp>
#include <Network/Twitch/OAuth2/Workflows/ClientCredentialsGrantFlow.hpp>
#include <Network/Twitch/OAuth2/CommandHandler.hpp>
#include <Network/Twitch/OAuth2/Status.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>
#include <boost/asio/steady_timer.hpp>
#include <boost/beast/core.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <mutex>
#include <string>
#include <thread>

namespace Network::Twitch::OAuth2
{

/**
 * @brief Twitch OAuth2 service.
 *
 * Maintain valid Twitch OAuth2 app and user access tokens.
 * The app access token is obtained using the Client Credentials grant flow.
 * The user access token is obtained using the Authorization Code grant flow.
 *
 * @see https://dev.twitch.tv/docs/authentication.
 */
class Service :
    public Network::Twitch::OAuth2::Workflows::ClientCredentialsGrantFlow,
    public Network::Twitch::OAuth2::Workflows::AuthorizationCodeGrantFlow,
    public Network::Twitch::OAuth2::CommandHandler
{
private:
    /**
     * @brief Constructor.
     *
     * Start receiving Twitch OAuth2 commands and processing Twitch OAuth2 sessions.
     */
    Service();

public:
    /**
     * @brief Destructor.
     *
     * Stop receiving Twitch OAuth2 commands and processing Twitch OAuth2 sessions.
     */
    ~Service();

    /**
     * @brief Deleted copy-constructor.
     */
    Service(const Service&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Service(Service&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Service& operator==(const Service&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Service& operator==(Service&&) = delete;

    /**
     * @brief Initialize the Twitch OAuth2 service.
     *
     * Start receiving Twitch OAuth2 commands and processing Twitch OAuth2 sessions.
     */
    static void Initialize();

private:
    /**
     * @brief Worker thread main loop.
     *
     * Invoke asynchronous Twitch OAuth2 HTTP session handlers.
     * The main loop is started upon construction, and stopped upon destruction.
     *
     * @param pStopToken Signal sent upon destruction to request the main loop to stop.
     */
    void Loop(
        std::stop_token pStopToken);

    /**
     * @brief Set the service status.
     *
     * @param pStatus The new service status.
     * @param pMessage The service status message.
     */
    void SetStatus(
        Network::Twitch::OAuth2::Status pStatus,
        const std::string& pMessage = std::string{});

    /**
     * @brief Get the service status.
     *
     * @return The service status.
     */
    Network::Twitch::OAuth2::Status GetStatus();

    /**
     * @brief Set the next heartbeat timeout.
     *
     * The new heartbeat timeout replaces any existing timeout set to expire later.
     * Trigger the validation of Twitch OAuth2 app and user access tokens.
     *
     * @param pTimeout The new timeout, in seconds.
     */
    void SetNextHeartbeatTimeout(
        std::uint64_t pTimeout);

    /**
     * @brief Prompt the user for authorization.
     *
     * Send an Authorization Request event containing the authorization URL.
     */
    void RequestUserAuthorization();

    /**
     * @brief Callback connect handler.
     *
     * This handler is invoked when an authorization request is received.
     *
     * @param pError The error details.
     * @param pSocket The TCP socket to communicate with the remote host.
     */
    void OnCallbackConnectHandler(
        boost::beast::error_code pError,
        boost::asio::ip::tcp::socket pSocket);

    /**
     * @brief Heartbeat timeout handler.
     *
     * This handler is invoked when the heartbeat timeout expires.
     * Trigger the validation of Twitch OAuth2 app and user access tokens.
     *
     * @param pError The error details.
     */
    void OnHeartbeatTimeout(
        boost::system::error_code pError);

    /**
     * @brief Client credentials grant flow session error handler.
     *
     * This handler is invoked when the app access token validation fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnValidateAppAccessTokenSessionError(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow response error handler.
     *
     * This handler is invoked when the app access token validation fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    void OnValidateAppAccessTokenResponseError(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow unauthorized error handler.
     *
     * This handler is invoked when the app access token validation fails due to an invalid or expired app access token.
     * The next step in the workflow should be to get a new app access token.
     *
     * @param pError The error details.
     */
    void OnValidateAppAccessTokenUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow success handler.
     *
     * This handler is invoked when the app access token validation succeeds.
     *
     * pAppAccessTokenTimeout The period of validity of the app access token, in seconds.
     */
    void OnValidateAppAccessTokenSuccess(
        std::int64_t pAppAccessTokenTimeout) override;

    /**
     * @brief Client credentials grant flow session error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnGetAppAccessTokenSessionError(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow response error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    void OnGetAppAccessTokenResponseError(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow unauthorized error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to authorization denied.
     *
     * @param pError The error details.
     */
    void OnGetAppAccessTokenUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Client credentials grant flow success handler.
     *
     * This handler is invoked when the app access token retrieval succeeds.
     *
     * @param pAppAccessToken The new app access token.
     * @param pAppAccessTokenTimeout The period of validity of the app access token, in seconds.
     */
    void OnGetAppAccessTokenSuccess(
        const std::string& pAppAccessToken,
        std::int64_t pAppAccessTokenTimeout) override;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token validation fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnValidateUserAccessTokenSessionError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token validation fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    void OnValidateUserAccessTokenResponseError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token validation fails due to an invalid or expired user access token.
     * The next step in the workflow should be to refresh the user access token.
     *
     * @param pError The error details.
     */
    void OnValidateUserAccessTokenUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token validation succeeds.
     *
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    void OnValidateUserAccessTokenSuccess(
        std::int64_t pUserAccessTokenTimeout) override;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnRefreshUserAccessTokenSessionError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    void OnRefreshUserAccessTokenResponseError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an invalid or expired user refresh token.
     * The next step in the workflow should be to request user authorization.
     *
     * @param pError The error details.
     */
    void OnRefreshUserAccessTokenUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token refresh succeeds.
     *
     * @param pUserAccessToken The new user access token.
     * @param pUserRefreshToken The new user refresh token.
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    void OnRefreshUserAccessTokenSuccess(
        const std::string& pUserAccessToken,
        const std::string& pUserRefreshToken,
        std::uint64_t pUserAccessTokenTimeout) override;

    /**
     * @brief Authorization code grant flow check state callback.
     *
     * This callback checks that the state parameter in the received user authorization request matches the initial request.
     *
     * @param pState The state parameter in the received user authorization request.
     * @return True if the state parameter in the received user authorization request matches the initial request.
     */
    bool OnGetAuthorizationCodeCheckState(
        const std::string& pState) override;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user authorization fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnGetAuthorizationCodeSessionError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow request error handler.
     *
     * This handler is invoked when the user authorization fails due to an HTTP request error.
     *
     * @param pError The error details.
     */
    void OnGetAuthorizationCodeRequestError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user authorization is denied.
     *
     * @param pError The error details.
     */
    void OnGetAuthorizationCodeUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user authorization succeeds.
     *
     * @param pAuthorizationCode The authorization code.
     */
    void OnGetAuthorizationCodeSuccess(
        const std::string& pAuthorizationCode) override;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    void OnGetUserAccessTokenSessionError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    void OnGetUserAccessTokenResponseError(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to invalid user credentials.
     *
     * @param pError The error details.
     */
    void OnGetUserAccessTokenUnauthorized(
        const std::string& pError) override;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token retrieval succeeds.
     *
     * @param pUserAccessToken The new user access token.
     * @param pUserRefreshToken The new user refresh token.
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    void OnGetUserAccessTokenSuccess(
        const std::string& pUserAccessToken,
        const std::string& pUserRefreshToken,
        std::uint64_t pUserAccessTokenTimeout) override;

    /**
     * @brief Twitch OAuth2 Validate Access Tokens command handler.
     *
     * Trigger the application and user access token grant flows.
     *
     * @param pCommand The command to handle.
     */
    void OnCommand(
        const Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand& pCommand) override;

private:
    /**
     * @brief The Twitch OAuth2 service log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief The service status.
     */
    Network::Twitch::OAuth2::Status mStatus{ Network::Twitch::OAuth2::Status::Busy };

    /**
     * @brief The IO context.
     *
     * The IO context is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, a new instance of IO context can be constructed to discard all waiting handlers.
     */
    std::unique_ptr<boost::asio::io_context> mIOContext;

    /**
     * @brief The HTTP callback listener.
     *
     * The HTTP callback listener is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, the IO context executor is reset, so a new instance of the HTTP callback listener must be constructed.
     */
    std::unique_ptr<boost::asio::ip::tcp::acceptor> mCallbackListener;

    /**
     * @brief The heartbeat timer to validate Twitch OAuth2 tokens on an hourly basis.
     *
     * The heartbeat timer is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, the IO context executor is reset, so a new instance of the heartbeat timer must be constructed.
     *
     * @see https://dev.twitch.tv/docs/authentication/validate-tokens/.
     */
    std::unique_ptr<boost::asio::steady_timer> mHeartbeatTimer;

    /**
     * @brief The service restart streak.
     */
    std::uint64_t mRestartCount{ 0ULL };

    /**
     * @brief The authorization request nonce.
     */
    std::string mNonce;

    /**
     * @brief Mutual exclusion to synchronize the worker thread, the message dispatcher thread and the service destruction.
     *
     * Protect the IO context pointer, the service status and the heartbeat timeout.
     */
    std::mutex mMutex;

    /**
     * @brief Worker thread.
     *
     * Invoke asynchronous Twitch OAuth2 HTTP session handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;

    /**
     * @brief Subscription to Twitch OAuth2 Command messages.
     */
    Network::Core::Dispatcher::Subscription mTwitchOAuth2CommandSubscription;
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_SERVICE_HPP_INCLUDED