#ifndef NETWORK_TWITCH_OAUTH2_COMMAND_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_COMMAND_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 CommandHandler Interface                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/OAuth2/Command.hpp>

namespace Network::Twitch::OAuth2
{

namespace Commands
{

struct ValidateAccessTokensCommand;

} // Commands

/**
 * @brief Twitch OAuth2 command handler.
 *
 * Provide Twitch OAuth2 command handler methods that can be invoked asynchronously.
 */
class CommandHandler : public Network::Core::Observer<Network::Twitch::OAuth2::Command>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the command message to the appropriate command handler method.
     *
     * @param pCommand The command message to handle.
     */
    void OnMessage(
        const Network::Twitch::OAuth2::Command& pCommand) override;

public:
    /**
     * @brief Twitch OAuth2 Validate Access Tokens command handler.
     *
     * @param pCommand The command to handle.
     */
    virtual void OnCommand(
        const Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand& pCommand);
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_COMMAND_HANDLER_HPP_INCLUDED