///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 GetUserAccessTokenSession Implementation                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/HTTPConstants.hpp>
#include <Network/Core/HTTPEncoding.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Utility.hpp>
#include <Network/Twitch/OAuth2/Sessions/GetUserAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Workflows/AuthorizationCodeGrantFlow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace Network::Twitch::OAuth2::Sessions
{

const std::string LOG_PREFIX{ "[GetUserAccessTokenSession] " };
constexpr std::uint64_t RETRY_DELAY_TOO_MANY_REQUESTS_SECONDS{ 60ULL };
constexpr std::uint64_t RETRY_TIMEOUT_SECONDS{ 300ULL };

GetUserAccessTokenSession::GetUserAccessTokenSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::OAuth2::Workflows::AuthorizationCodeGrantFlow& pWorkflow,
    boost::asio::io_context& pIOContext,
    const std::string& pAuthorizationCode) :
    Network::Core::HTTPSession(pLogger, pIOContext),
    mLogger(pLogger),
    mWorkflow(pWorkflow),
    mAuthorizationCode(pAuthorizationCode)
{
    SetRetryTimeout(RETRY_TIMEOUT_SECONDS);
}

boost::beast::http::request<boost::beast::http::string_body> GetUserAccessTokenSession::BuildHTTPRequest()
{
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    const std::vector<std::pair<std::string, std::string>> lHTTPRequestQuery{
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_CLIENT_ID, Network::Core::URLEncoded(lSettings.GetTwitchClientId()) },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_CLIENT_SECRET, Network::Core::URLEncoded(lSettings.GetTwitchClientSecret()) },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_CODE, mAuthorizationCode },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_GRANT_TYPE, Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_GRANT_TYPE_AUTHORIZATION_CODE },
        { Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_REDIRECT_URI, lSettings.GetTwitchOAuth2CallbackURI() }
    };

    boost::beast::http::request<boost::beast::http::string_body> lHTTPRequest = {};
    lHTTPRequest.method(boost::beast::http::verb::post);
    lHTTPRequest.target(Network::Twitch::OAuth2::Endpoints::TOKEN);
    lHTTPRequest.set(boost::beast::http::field::host, Network::Twitch::OAuth2::Endpoints::HOSTNAME);
    lHTTPRequest.set(boost::beast::http::field::content_type, Network::Core::HTTP_CONTENT_TYPE_APPLICATION_X_WWW_FORM_URLENCODED);
    lHTTPRequest.body() = Network::Core::BuildQueryString(lHTTPRequestQuery);
    lHTTPRequest.prepare_payload();
    return lHTTPRequest;
}

void GetUserAccessTokenSession::OnStartError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "aborted: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_ABORTED);
    }
}

void GetUserAccessTokenSession::OnResolveError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_RESOLVE);
    }
}

void GetUserAccessTokenSession::OnConnectError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_CONNECT);
    }
}

void GetUserAccessTokenSession::OnHandshakeError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "handshake error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_HANDSHAKE);
    }
}

void GetUserAccessTokenSession::OnWriteError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network write error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_WRITE);
    }
}

void GetUserAccessTokenSession::OnReadError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnGetUserAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_READ);
    }
}

bool GetUserAccessTokenSession::OnHTTPResponse(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "response: " << pHTTPResponse.result();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "response:\n" << pHTTPResponse;
    switch (pHTTPResponse.result())
    {
    case boost::beast::http::status::ok:
        return OnHTTPResponseOK(pHTTPResponse);
    case boost::beast::http::status::bad_request:
        return OnHTTPResponseBadRequest(pHTTPResponse);
    case boost::beast::http::status::unauthorized:
        return OnHTTPResponseUnauthorized(pHTTPResponse);
    case boost::beast::http::status::too_many_requests:
        return OnHTTPResponseTooManyRequests(pHTTPResponse);
    }
    return OnHTTPResponseError(pHTTPResponse);
}

bool GetUserAccessTokenSession::OnHTTPResponseOK(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    boost::property_tree::ptree lHTTPResponseJSON;
    try
    {
        std::stringstream lHTTPResponseBody{ pHTTPResponse.body() };
        boost::property_tree::read_json(lHTTPResponseBody, lHTTPResponseJSON);

        const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

        const auto lUserAccessToken{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_ACCESS_TOKEN) };
        if (!lUserAccessToken)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_ACCESS_TOKEN;
            mWorkflow.OnGetUserAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }

        const auto lUserAccessTokenTimeout{ lHTTPResponseJSON.get_optional<std::int64_t>(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_EXPIRES_IN) };
        if (!lUserAccessTokenTimeout)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_EXPIRES_IN;
            mWorkflow.OnGetUserAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }

        const auto lUserRefreshToken{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_REFRESH_TOKEN) };
        if (!lUserRefreshToken)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_REFRESH_TOKEN;
            mWorkflow.OnGetUserAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }

        if (const auto lScope{ lHTTPResponseJSON.get_child_optional(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_SCOPE) })
        {
            const std::set<std::string> lTokenScopes{ Network::Twitch::OAuth2::GetAllValues(lScope.get()) };
            for (const std::string& lRequiredScope : lSettings.GetTwitchUserAccessScopes())
            {
                if (lTokenScopes.find(lRequiredScope) == lTokenScopes.cend())
                {
                    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing required scope: " << lRequiredScope;
                    mWorkflow.OnGetUserAccessTokenUnauthorized(Network::Twitch::OAuth2::Errors::BAD_ACCESS_SCOPES);
                    return true;
                }
            }
        }
        else
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_SCOPE;
            mWorkflow.OnGetUserAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }

        mWorkflow.OnGetUserAccessTokenSuccess(lUserAccessToken.get(), lUserRefreshToken.get(), lUserAccessTokenTimeout.get());
        return true;
    }
    catch (boost::property_tree::json_parser_error& lException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "bad content: " << lException.what();
        mWorkflow.OnGetUserAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
        return true;
    }
}

bool GetUserAccessTokenSession::OnHTTPResponseBadRequest(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    mWorkflow.OnGetUserAccessTokenResponseError(
        Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    return true;
}

bool GetUserAccessTokenSession::OnHTTPResponseUnauthorized(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    mWorkflow.OnGetUserAccessTokenUnauthorized(
        Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    return true;
}

bool GetUserAccessTokenSession::OnHTTPResponseTooManyRequests(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the rate-limit bucket to be reset and retry.
    const bool lFatalError{ !HasRetryBudget() };
    SetRetryDelay(RETRY_DELAY_TOO_MANY_REQUESTS_SECONDS);
    if (lFatalError)
    {
        mWorkflow.OnGetUserAccessTokenResponseError(
            Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    }
    return lFatalError;
}

bool GetUserAccessTokenSession::OnHTTPResponseError(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the problem to magically disappear and retry.
    const bool lFatalError{ !HasRetryBudget() };
    if (lFatalError)
    {
        mWorkflow.OnGetUserAccessTokenResponseError(
            Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    }
    return lFatalError;
}

} // Network::Twitch::OAuth2::Sessions