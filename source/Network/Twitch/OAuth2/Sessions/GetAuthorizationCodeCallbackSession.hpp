#ifndef NETWORK_GET_AUTHORIZATION_CODE_CALLBACK_SESSION_HPP_INCLUDED
#define NETWORK_GET_AUTHORIZATION_CODE_CALLBACK_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 GetAuthorizationCodeCallbackSession Interface                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>
#include <Network/Core/HTTPCallbackSession.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/beast/http.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <memory>
#include <string>

namespace Network::Twitch::OAuth2::Workflows
{

class AuthorizationCodeGrantFlow;

} // Network::Twitch::OAuth2::Workflows

namespace Network::Twitch::OAuth2::Sessions
{

/**
 * @brief Twitch OAuth2 HTTP callback session to get a user authorization code using the authorization code grant flow.
 *
 * @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#authorization-code-grant-flow.
 */
class GetAuthorizationCodeCallbackSession : public Network::Core::HTTPCallbackSession
{
public:
    /**
     * @brief Constructor.
     *
     * This class must be allocated dynamically using std::make_shared().
     *
     * @param pLogger The log source.
     * @param pWorkflow The parent authentication workflow that will handle the result of the session.
     * @param pSocket The TCP socket to communicate with the remote host.
     */
    GetAuthorizationCodeCallbackSession(
        Network::Core::Logger& pLogger,
        Network::Twitch::OAuth2::Workflows::AuthorizationCodeGrantFlow& pWorkflow,
        boost::asio::ip::tcp::socket&& pSocket);

private:
    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the callback session fails to read the HTTP request from the remote host.
     *
     * @param pError The error message.
     */
    void OnReadError(
        const std::string& pError) override;

    /**
     * @brief HTTP request handler.
     *
     * This handler is invoked when the callback session receives the HTTP request from the remote host.
     *
     * Possible errors: missing mandatory field.
     *
     * @param pHTTPRequest The HTTP request.
     * @return The optional HTTP response.
     */
    std::optional<boost::beast::http::response<boost::beast::http::string_body>> OnHTTPRequest(
        const boost::beast::http::request<boost::beast::http::string_body>& pHTTPRequest) override;

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The parent authentication workflow that will handle the result of the session.
     */
    Network::Twitch::OAuth2::Workflows::AuthorizationCodeGrantFlow& mWorkflow;
};

} // Network::Twitch::OAuth2::Sessions

#endif // NETWORK_GET_AUTHORIZATION_CODE_CALLBACK_SESSION_HPP_INCLUDED