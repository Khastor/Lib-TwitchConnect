///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 ValidateAppAccessTokenSession Implementation                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/HTTPConstants.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Utility.hpp>
#include <Network/Twitch/OAuth2/Sessions/ValidateAppAccessTokenSession.hpp>
#include <Network/Twitch/OAuth2/Workflows/ClientCredentialsGrantFlow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace Network::Twitch::OAuth2::Sessions
{

const std::string LOG_PREFIX{ "[ValidateAppAccessTokenSession] " };
constexpr std::uint64_t RETRY_DELAY_TOO_MANY_REQUESTS_SECONDS{ 60ULL };
constexpr std::uint64_t RETRY_TIMEOUT_SECONDS{ 300ULL };

ValidateAppAccessTokenSession::ValidateAppAccessTokenSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::OAuth2::Workflows::ClientCredentialsGrantFlow& pWorkflow,
    boost::asio::io_context& pIOContext) :
    Network::Core::HTTPSession(pLogger, pIOContext),
    mLogger(pLogger),
    mWorkflow(pWorkflow)
{
    SetRetryTimeout(RETRY_TIMEOUT_SECONDS);
}

boost::beast::http::request<boost::beast::http::string_body> ValidateAppAccessTokenSession::BuildHTTPRequest()
{
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    boost::beast::http::request<boost::beast::http::string_body> lHTTPRequest = {};
    lHTTPRequest.method(boost::beast::http::verb::get);
    lHTTPRequest.target(Network::Twitch::OAuth2::Endpoints::VALIDATE);
    lHTTPRequest.set(boost::beast::http::field::host, Network::Twitch::OAuth2::Endpoints::HOSTNAME);
    lHTTPRequest.set(boost::beast::http::field::authorization, Network::Core::HTTP_AUTHORIZATION_BEARER + lSettings.GetTwitchAppAccessToken());
    lHTTPRequest.prepare_payload();
    return lHTTPRequest;
}

void ValidateAppAccessTokenSession::OnStartError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "aborted: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_ABORTED);
    }
}

void ValidateAppAccessTokenSession::OnResolveError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_RESOLVE);
    }
}

void ValidateAppAccessTokenSession::OnConnectError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_CONNECT);
    }
}

void ValidateAppAccessTokenSession::OnHandshakeError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "handshake error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_HANDSHAKE);
    }
}

void ValidateAppAccessTokenSession::OnWriteError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network write error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_WRITE);
    }
}

void ValidateAppAccessTokenSession::OnReadError(
    const std::string& pError,
    bool pFatal)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    if (pFatal)
    {
        mWorkflow.OnValidateAppAccessTokenSessionError(Network::Twitch::OAuth2::Errors::SESSION_READ);
    }
}

bool ValidateAppAccessTokenSession::OnHTTPResponse(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "response: " << pHTTPResponse.result();
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "response:\n" << pHTTPResponse;
    switch (pHTTPResponse.result())
    {
    case boost::beast::http::status::ok:
        return OnHTTPResponseOK(pHTTPResponse);
    case boost::beast::http::status::bad_request:
        return OnHTTPResponseBadRequest(pHTTPResponse);
    case boost::beast::http::status::unauthorized:
        return OnHTTPResponseUnauthorized(pHTTPResponse);
    case boost::beast::http::status::too_many_requests:
        return OnHTTPResponseTooManyRequests(pHTTPResponse);
    }
    return OnHTTPResponseError(pHTTPResponse);
}

bool ValidateAppAccessTokenSession::OnHTTPResponseOK(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    boost::property_tree::ptree lHTTPResponseJSON;
    try
    {
        std::stringstream lHTTPResponseBody{ pHTTPResponse.body() };
        boost::property_tree::read_json(lHTTPResponseBody, lHTTPResponseJSON);

        const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

        const auto lAppAccessTokenTimeout{ lHTTPResponseJSON.get_optional<std::int64_t>(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_EXPIRES_IN) };
        if (!lAppAccessTokenTimeout)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_EXPIRES_IN;
            mWorkflow.OnValidateAppAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }

        const auto lClientId{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_CONTENT_CLIENT_ID) };
        if (!lClientId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_CONTENT_CLIENT_ID;
            mWorkflow.OnValidateAppAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
            return true;
        }
        if (lClientId.get() != lSettings.GetTwitchClientId())
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "bad client id: " << lClientId.get();
            mWorkflow.OnValidateAppAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CLIENT_ID);
            return true;
        }

        mWorkflow.OnValidateAppAccessTokenSuccess(lAppAccessTokenTimeout.get());
        return true;
    }
    catch (boost::property_tree::json_parser_error& lException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "bad content: " << lException.what();
        mWorkflow.OnValidateAppAccessTokenResponseError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
        return true;
    }
}

bool ValidateAppAccessTokenSession::OnHTTPResponseBadRequest(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    mWorkflow.OnValidateAppAccessTokenResponseError(
        Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    return true;
}

bool ValidateAppAccessTokenSession::OnHTTPResponseUnauthorized(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    mWorkflow.OnValidateAppAccessTokenUnauthorized(
        Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    return true;
}

bool ValidateAppAccessTokenSession::OnHTTPResponseTooManyRequests(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the rate-limit bucket to be reset and retry.
    const bool lFatalError{ !HasRetryBudget() };
    SetRetryDelay(RETRY_DELAY_TOO_MANY_REQUESTS_SECONDS);
    if (lFatalError)
    {
        mWorkflow.OnValidateAppAccessTokenResponseError(
            Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    }
    return lFatalError;
}

bool ValidateAppAccessTokenSession::OnHTTPResponseError(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    // Wait for the problem to magically disappear and retry.
    const bool lFatalError{ !HasRetryBudget() };
    if (lFatalError)
    {
        mWorkflow.OnValidateAppAccessTokenResponseError(
            Network::Twitch::OAuth2::GetErrorMessage(pHTTPResponse).value_or(pHTTPResponse.reason()));
    }
    return lFatalError;
}

} // Network::Twitch::OAuth2::Sessions