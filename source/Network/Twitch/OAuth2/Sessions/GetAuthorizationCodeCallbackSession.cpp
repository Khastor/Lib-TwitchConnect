///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 GetAuthorizationCodeCallbackSession Implementation                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/HTTPEncoding.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Sessions/GetAuthorizationCodeCallbackSession.hpp>
#include <Network/Twitch/OAuth2/Workflows/AuthorizationCodeGrantFlow.hpp>

namespace Network::Twitch::OAuth2::Sessions
{

const std::string LOG_PREFIX{ "[GetAuthorizationCodeCallbackSession] " };

GetAuthorizationCodeCallbackSession::GetAuthorizationCodeCallbackSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::OAuth2::Workflows::AuthorizationCodeGrantFlow& pWorkflow,
    boost::asio::ip::tcp::socket&& pSocket) :
    HTTPCallbackSession(pLogger, std::move(pSocket)),
    mLogger(pLogger),
    mWorkflow(pWorkflow)
{}

void GetAuthorizationCodeCallbackSession::OnReadError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    mWorkflow.OnGetAuthorizationCodeSessionError(pError);
}

std::optional<boost::beast::http::response<boost::beast::http::string_body>> GetAuthorizationCodeCallbackSession::OnHTTPRequest(
    const boost::beast::http::request<boost::beast::http::string_body>& pHTTPRequest)
{
    NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "processing request";
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "request:\n" << pHTTPRequest;
    boost::beast::http::response<boost::beast::http::string_body> lHTTPResponse;
    if (pHTTPRequest.method() == boost::beast::http::verb::get)
    {
        const std::unordered_map<std::string, std::string> lParameters{ Network::Core::ParseQueryString(pHTTPRequest.target()) };
        if (const auto lStateIt{ lParameters.find(Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_STATE) }; lStateIt != lParameters.cend())
        {
            if (mWorkflow.OnGetAuthorizationCodeCheckState(lStateIt->second))
            {
                if (const auto lCodeIt{ lParameters.find(Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_CODE) }; lCodeIt != lParameters.cend())
                {
                    mWorkflow.OnGetAuthorizationCodeSuccess(lCodeIt->second);
                    lHTTPResponse = { boost::beast::http::status::no_content, pHTTPRequest.version() };
                }
                else
                {
                    if (const auto lErrorIt{ lParameters.find(Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_ERROR_DESCRIPTION) }; lErrorIt != lParameters.cend())
                    {
                        NETWORK_CORE_LOG_INFO(mLogger) << LOG_PREFIX << "error description: " << lErrorIt->second;
                        mWorkflow.OnGetAuthorizationCodeUnauthorized(Network::Core::URLDecoded(lErrorIt->second).value_or(lErrorIt->second));
                        lHTTPResponse = { boost::beast::http::status::no_content, pHTTPRequest.version() };
                    }
                    else
                    {
                        NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::OAuth2::Keys::HTTP_PARAMETER_ERROR_DESCRIPTION;
                        mWorkflow.OnGetAuthorizationCodeRequestError(Network::Twitch::OAuth2::Errors::BAD_CONTENT);
                        lHTTPResponse = { boost::beast::http::status::bad_request, pHTTPRequest.version() };
                    }
                }
            }
            else
            {
                NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "session dropped: unexpected state parameter in query string";
                lHTTPResponse = { boost::beast::http::status::bad_request, pHTTPRequest.version() };
            }
        }
        else
        {
            NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "session dropped: missing state parameter in query string";
            lHTTPResponse = { boost::beast::http::status::bad_request, pHTTPRequest.version() };
        }
    }
    else
    {
        NETWORK_CORE_LOG_WARNING(mLogger) << LOG_PREFIX << "session dropped: unexpected http method";
        lHTTPResponse = { boost::beast::http::status::method_not_allowed, pHTTPRequest.version() };
    }
    return lHTTPResponse;
}

} // Network::Twitch::OAuth2::Sessions