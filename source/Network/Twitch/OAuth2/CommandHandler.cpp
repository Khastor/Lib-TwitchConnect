///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 CommandHandler Implementation                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Command.hpp>
#include <Network/Twitch/OAuth2/CommandHandler.hpp>

namespace Network::Twitch::OAuth2
{

void CommandHandler::OnMessage(
    const Network::Twitch::OAuth2::Command& pCommand)
{
    pCommand.Dispatch(*this);
}

void CommandHandler::OnCommand(
    const Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand&)
{}

} // Network::Twitch::OAuth2