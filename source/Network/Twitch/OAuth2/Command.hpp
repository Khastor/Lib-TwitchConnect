#ifndef NETWORK_TWITCH_OAUTH2_COMMAND_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_COMMAND_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Command Interface                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::OAuth2
{

class CommandHandler;

/**
 * @brief Base class in the hierarchy of Twitch OAuth2 commands.
 */
struct Command : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Command() = default;

    /**
     * @brief Dispatch the command to the appropriate command handler method.
     *
     * @param pHandler The command handler.
     */
    virtual void Dispatch(
        CommandHandler& pHandler) const = 0;
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_COMMAND_HPP_INCLUDED