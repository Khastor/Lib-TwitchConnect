///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Utility Implementation                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Utility.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <iomanip>
#include <random>
#include <sstream>

namespace Network::Twitch::OAuth2
{

namespace
{

void GetAllValuesInternal(
    const boost::property_tree::ptree& pNode,
    std::set<std::string>& pValues)
{
    if (std::string lValue{ pNode.get_value<std::string>() }; !lValue.empty())
    {
        pValues.insert(lValue);
    }
    for (const auto& lChild : pNode)
    {
        GetAllValuesInternal(lChild.second, pValues);
    }
}

} // anonymous namespace

std::optional<std::string> GetErrorMessage(
    const boost::beast::http::response<boost::beast::http::string_body>& pHTTPResponse)
{
    boost::property_tree::ptree lHTTPResponseJSON;
    try
    {
        std::stringstream lHTTPResponseBody(pHTTPResponse.body());
        boost::property_tree::read_json(lHTTPResponseBody, lHTTPResponseJSON);
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_ERROR_CONTENT_MESSAGE) })
        {
            return lErrorMessage.value();
        }
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_ERROR_CONTENT_REASON) })
        {
            return lErrorMessage.value();
        }
        if (const auto lErrorMessage{ lHTTPResponseJSON.get_optional<std::string>(Network::Twitch::OAuth2::Keys::HTTP_ERROR_CONTENT_STATUS) })
        {
            return lErrorMessage.value();
        }
        return {};
    }
    catch (...)
    {
        return {};
    }
}

std::set<std::string> GetAllValues(
    const boost::property_tree::ptree& pNode)
{
    std::set<std::string> lValues;
    GetAllValuesInternal(pNode, lValues);
    return lValues;
}

std::string GenerateNonce()
{
    static std::random_device lDevice;
    static std::mt19937 lEngine{ lDevice() };

    const uint64_t lNonce{ std::uniform_int_distribution<uint64_t>{}(lEngine) };
    return (std::stringstream{} << std::setprecision(16) << std::setfill('0') << std::hex << lNonce).str();
}

} // Network::Twitch::OAuth2