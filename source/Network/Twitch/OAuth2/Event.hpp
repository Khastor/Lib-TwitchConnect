#ifndef NETWORK_TWITCH_OAUTH2_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 Event Interface                                                                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::OAuth2
{

class EventHandler;

/**
 * @brief Base class in the hierarchy of Twitch OAuth2 events.
 */
struct Event : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Event() = default;

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    virtual void Dispatch(
        EventHandler& pHandler) const = 0;
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_EVENT_HPP_INCLUDED