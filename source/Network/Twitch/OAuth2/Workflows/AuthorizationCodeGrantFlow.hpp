#ifndef NETWORK_TWITCH_OAUTH2_WORKFLOWS_AUTHORIZATION_CODE_GRANT_FLOW_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_WORKFLOWS_AUTHORIZATION_CODE_GRANT_FLOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 AuthorizationCodeGrantFlow Interface                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::OAuth2::Workflows
{

/**
 * @brief Twitch OAuth2 authorization code grant flow interface.
 *
 * @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#authorization-code-grant-flow.
 */
class AuthorizationCodeGrantFlow
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~AuthorizationCodeGrantFlow() = default;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token validation fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnValidateUserAccessTokenSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token validation fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    virtual void OnValidateUserAccessTokenResponseError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token validation fails due to an invalid or expired user access token.
     * The next step in the workflow should be to refresh the user access token.
     *
     * @param pError The error details.
     */
    virtual void OnValidateUserAccessTokenUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token validation succeeds.
     *
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    virtual void OnValidateUserAccessTokenSuccess(
        std::int64_t pUserAccessTokenTimeout) = 0;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnRefreshUserAccessTokenSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    virtual void OnRefreshUserAccessTokenResponseError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token refresh fails due to an invalid or expired user refresh token.
     * The next step in the workflow should be to request user authorization.
     *
     * @param pError The error details.
     */
    virtual void OnRefreshUserAccessTokenUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token refresh succeeds.
     *
     * @param pUserAccessToken The new user access token.
     * @param pUserRefreshToken The new user refresh token.
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    virtual void OnRefreshUserAccessTokenSuccess(
        const std::string& pUserAccessToken,
        const std::string& pUserRefreshToken,
        std::uint64_t pUserAccessTokenTimeout) = 0;

    /**
     * @brief Authorization code grant flow check state callback.
     *
     * This callback checks that the state parameter in the received user authorization request matches the initial request.
     *
     * @param pState The state parameter in the received user authorization request.
     * @return True if the state parameter in the received user authorization request matches the initial request.
     */
    virtual bool OnGetAuthorizationCodeCheckState(
        const std::string& pState) = 0;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user authorization fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnGetAuthorizationCodeSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow request error handler.
     *
     * This handler is invoked when the user authorization fails due to an HTTP request error.
     *
     * @param pError The error details.
     */
    virtual void OnGetAuthorizationCodeRequestError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user authorization is denied.
     *
     * @param pError The error details.
     */
    virtual void OnGetAuthorizationCodeUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user authorization succeeds.
     *
     * @param pAuthorizationCode The authorization code.
     */
    virtual void OnGetAuthorizationCodeSuccess(
        const std::string& pAuthorizationCode) = 0;

    /**
     * @brief Authorization code grant flow session error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnGetUserAccessTokenSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow response error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    virtual void OnGetUserAccessTokenResponseError(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow unauthorized error handler.
     *
     * This handler is invoked when the user access token retrieval fails due to invalid user credentials.
     *
     * @param pError The error details.
     */
    virtual void OnGetUserAccessTokenUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Authorization code grant flow success handler.
     *
     * This handler is invoked when the user access token retrieval succeeds.
     *
     * @param pUserAccessToken The new user access token.
     * @param pUserRefreshToken The new user refresh token.
     * @param pUserAccessTokenTimeout The period of validity of the user access token, in seconds.
     */
    virtual void OnGetUserAccessTokenSuccess(
        const std::string& pUserAccessToken,
        const std::string& pUserRefreshToken,
        std::uint64_t pUserAccessTokenTimeout) = 0;
};

} // Network::Twitch::OAuth2::Workflows

#endif // NETWORK_TWITCH_OAUTH2_WORKFLOWS_AUTHORIZATION_CODE_GRANT_FLOW_HPP_INCLUDED