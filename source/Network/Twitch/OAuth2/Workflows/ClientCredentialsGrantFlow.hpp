#ifndef NETWORK_TWITCH_OAUTH2_WORKFLOWS_CLIENT_CREDENTIALS_GRANT_FLOW_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_WORKFLOWS_CLIENT_CREDENTIALS_GRANT_FLOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 ClientCredentialsGrantFlow Interface                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::OAuth2::Workflows
{

/**
 * @brief Twitch OAuth2 client credentials grant flow interface.
 *
 * @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#client-credentials-grant-flow.
 */
class ClientCredentialsGrantFlow
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~ClientCredentialsGrantFlow() = default;

    /**
     * @brief Client credentials grant flow session error handler.
     *
     * This handler is invoked when the app access token validation fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnValidateAppAccessTokenSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow response error handler.
     *
     * This handler is invoked when the app access token validation fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    virtual void OnValidateAppAccessTokenResponseError(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow unauthorized error handler.
     *
     * This handler is invoked when the app access token validation fails due to an invalid or expired app access token.
     * The next step in the workflow should be to get a new app access token.
     *
     * @param pError The error details.
     */
    virtual void OnValidateAppAccessTokenUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow success handler.
     *
     * This handler is invoked when the app access token validation succeeds.
     *
     * pAppAccessTokenTimeout The period of validity of the app access token, in seconds.
     */
    virtual void OnValidateAppAccessTokenSuccess(
        std::int64_t pAppAccessTokenTimeout) = 0;

    /**
     * @brief Client credentials grant flow session error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to an HTTP session error.
     *
     * @param pError The error details.
     */
    virtual void OnGetAppAccessTokenSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow response error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to an HTTP response error.
     *
     * @param pError The error details.
     */
    virtual void OnGetAppAccessTokenResponseError(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow unauthorized error handler.
     *
     * This handler is invoked when the app access token retrieval fails due to authorization denied.
     *
     * @param pError The error details.
     */
    virtual void OnGetAppAccessTokenUnauthorized(
        const std::string& pError) = 0;

    /**
     * @brief Client credentials grant flow success handler.
     *
     * This handler is invoked when the app access token retrieval succeeds.
     *
     * @param pAppAccessToken The new app access token.
     * @param pAppAccessTokenTimeout The period of validity of the app access token, in seconds.
     */
    virtual void OnGetAppAccessTokenSuccess(
        const std::string& pAppAccessToken,
        std::int64_t pAppAccessTokenTimeout) = 0;
};

} // Network::Twitch::OAuth2::Workflows

#endif // NETWORK_TWITCH_OAUTH2_WORKFLOWS_CLIENT_CREDENTIALS_GRANT_FLOW_HPP_INCLUDED