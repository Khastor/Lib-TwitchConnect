#ifndef NETWORK_TWITCH_OAUTH2_EVENT_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_OAUTH2_EVENT_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch OAuth2 EventHandler Interface                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/OAuth2/Event.hpp>

namespace Network::Twitch::OAuth2
{

namespace Events
{

struct AuthorizationRequestEvent;
struct StatusChangeEvent;

} // Events

/**
 * @brief Twitch OAuth2 event handler.
 *
 * Provide Twitch OAuth2 event handler methods that can be invoked asynchronously.
 */
class EventHandler : public Network::Core::Observer<Network::Twitch::OAuth2::Event>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the event message to the appropriate event handler method.
     *
     * @param pEvent The event message to handle.
     */
    void OnMessage(
        const Network::Twitch::OAuth2::Event& pEvent) override;

public:
    /**
     * @brief Twitch OAuth2 Authorization Request event handler.
     *
     * This handler is invoked when the Twitch OAuth2 service requires user authorization.
     * The user must navigate to the authorization request URL to authorize the application.
     *
     * @param pEvent The event details.
     * @see https://dev.twitch.tv/docs/authentication/getting-tokens-oauth/#authorization-code-grant-flow.
     */
    virtual void OnEvent(
        const Network::Twitch::OAuth2::Events::AuthorizationRequestEvent& pEvent);

    /**
     * @brief Twitch OAuth2 Status Change event handler.
     *
     * This handler is invoked when the Twitch OAuth2 service status changes.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::OAuth2::Events::StatusChangeEvent& pEvent);
};

} // Network::Twitch::OAuth2

#endif // NETWORK_TWITCH_OAUTH2_EVENT_HANDLER_HPP_INCLUDED