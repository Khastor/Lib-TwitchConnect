#ifndef NETWORK_TWITCH_MODEL_COLOR_HPP_INCLUDED
#define NETWORK_TWITCH_MODEL_COLOR_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Color Definition                                                                              //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>

namespace Network::Twitch::Model
{

/**
 * @brief 24-bit RGB color.
 */
struct Color
{
    std::uint8_t R;
    std::uint8_t G;
    std::uint8_t B;
};

} // Network::Twitch::Model

#endif // NETWORK_TWITCH_MODEL_COLOR_HPP_INCLUDED