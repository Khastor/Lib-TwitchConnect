#ifndef NETWORK_TWITCH_EVENTSUB_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED
#define NETWORK_TWITCH_EVENTSUB_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub WebsocketWorkflow Interface                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/ptree.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::EventSub::Workflows
{

/**
 * @brief Twitch EventSub workflow interface.
 *
 * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/.
 */
class WebsocketWorkflow
{
public:
    /**
     * @brief Destructor.
     */
    virtual ~WebsocketWorkflow() = default;

    /**
     * @brief Twitch EventSub websocket session error handler.
     *
     * This handler is invoked when a websocket session fails due to a connection error.
     *
     * @param pError The error details.
     */
    virtual void OnEventSubWebsocketSessionError(
        const std::string& pError) = 0;

    /**
     * @brief Twitch EventSub websocket message error handler.
     *
     * This handler is invoked when a websocket session fails due to a websocket message error.
     *
     * @param pError The error details.
     */
    virtual void OnEventSubWebsocketMessageError(
        const std::string& pError) = 0;

    /**
     * @brief Twitch EventSub websocket welcome message handler.
     *
     * This handler is invoked when a websocket session receives a welcome message.
     *
     * @param pSessionId The websocket session id.
     * @param pKeepalive The keepalive timeout, in seconds.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#welcome-message.
     */
    virtual void OnEventSubWebsocketWelcomeMessage(
        const std::string& pSessionId,
        std::uint64_t pKeepalive) = 0;

    /**
     * @brief Twitch EventSub websocket keepalive message handler.
     *
     * This handler is invoked when a websocket session receives a keepalive message.
     *
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#keepalive-message.
     */
    virtual void OnEventSubWebsocketKeepaliveMessage() = 0;

    /**
     * @brief Twitch EventSub websocket notification message handler.
     *
     * This handler is invoked when a websocket session receives a notification message.
     *
     * @param pSubscriptionType The type of event sent in the notification message.
     * @param pEvent The event details.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#notification-message.
     */
    virtual void OnEventSubWebsocketNotificationMessage(
        const std::string& pSubscriptionType,
        const boost::property_tree::ptree& pEvent) = 0;

    /**
     * @brief Twitch EventSub websocket reconnect message handler.
     *
     * This handler is invoked when a websocket session receives a reconnect message.
     *
     * @param pSessionId The websocket session reconnect URL.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#reconnect-message.
     */
    virtual void OnEventSubWebsocketReconnectMessage(
        const std::string& pReconnectURL) = 0;

    /**
     * @brief Twitch EventSub websocket revocation message handler.
     *
     * This handler is invoked when a websocket session receives a revocation message.
     *
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#revocation-message.
     */
    virtual void OnEventSubWebsocketRevocationMessage() = 0;
};

} // Network::Twitch::EventSub::Workflows

#endif // NETWORK_TWITCH_EVENTSUB_WORKFLOWS_WEBSOCKET_WORKFLOW_HPP_INCLUDED