#ifndef NETWORK_TWITCH_EVENT_SUB_CONSTANTS_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_CONSTANTS_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Endpoint Definitions                                                          //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::EventSub::Endpoints
{

/**
 * @brief Twitch EventSub remote host name.
 */
inline const std::string HOSTNAME{ "eventsub.wss.twitch.tv" };

/**
 * @brief Twitch EventSub remote host port.
 */
inline const std::string PORT{ "443" };

/**
 * @brief Twitch EventSub websocket endpoint.
 */
inline const std::string WEBSOCKET{ "/ws" };

} // Network::Twitch::EventSub::Endpoints

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Subscriptions Definitions                                                     //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::EventSub::Subscriptions
{

/**
 * @brief Twitch EventSub subscription version 1.
 */
inline const std::string VERSION_1{ "1" };

/**
 * @brief Twitch EventSub Channel Ad Bread Begin subscription type.
 */
inline const std::string CHANNEL_AD_BREAK_BEGIN{ "channel.ad_break.begin" };

/**
 * @brief Twitch EventSub Channel Points Automatic Reward Redemption Add subscription type.
 */
inline const std::string CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD{ "channel.channel_points_automatic_reward_redemption.add" };

/**
 * @brief Twitch EventSub Channel Points Custom Reward Redemption Add subscription type.
 */
inline const std::string CHANNEL_POINTS_CUSTOM_REWARD_REDEMPTION_ADD{ "channel.channel_points_custom_reward_redemption.add" };

/**
 * @brief Twitch EventSub Channel Raid subscription type.
 */
inline const std::string CHANNEL_RAID{ "channel.raid" };

} // Network::Twitch::EventSub::Subscriptions

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Message Keys Definitions                                                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::EventSub::Keys
{

/**
 * @brief Twitch EventSub message key "metadata".
 */
inline const std::string MESSAGE_METADATA{ "metadata" };

/**
 * @brief Twitch EventSub message key "metadata.message_type".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE{ "message_type" };

/**
 * @brief Twitch EventSub message key "metadata.message_id".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_ID{ "message_id" };

/**
 * @brief Twitch EventSub message key "metadata.message_timestamp".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TIMESTAMP{ "message_timestamp" };

/**
 * @brief Twitch EventSub message key "metadata.subscription_type".
 */
inline const std::string MESSAGE_METADATA_SUBSCRIPTION_TYPE{ "subscription_type" };

/**
 * @brief Twitch EventSub message key "metadata.message_type" enumerator value "session_welcome".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE_SESSION_WELCOME{ "session_welcome" };

/**
 * @brief Twitch EventSub message key "metadata.message_type" enumerator value "session_keepalive".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE_SESSION_KEEPALIVE{ "session_keepalive" };

/**
 * @brief Twitch EventSub message key "metadata.message_type" enumerator value "session_reconnect".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE_SESSION_RECONNECT{ "session_reconnect" };

/**
 * @brief Twitch EventSub message key "metadata.message_type" enumerator value "revocation".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE_REVOCATION{ "revocation" };

/**
 * @brief Twitch EventSub message key "metadata.message_type" enumerator value "notification".
 */
inline const std::string MESSAGE_METADATA_MESSAGE_TYPE_NOTIFICATION{ "notification" };

/**
 * @brief Twitch EventSub message key "payload".
 */
inline const std::string MESSAGE_PAYLOAD{ "payload" };

/**
 * @brief Twitch EventSub message key "payload.event".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT{ "event" };

/**
 * @brief Twitch EventSub message key "payload.event.broadcaster_user_id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID{ "broadcaster_user_id" };

/**
 * @brief Twitch EventSub message key "payload.event.broadcaster_user_login".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN{ "broadcaster_user_login" };

/**
 * @brief Twitch EventSub message key "payload.event.broadcaster_user_name".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME{ "broadcaster_user_name" };

/**
 * @brief Twitch EventSub message key "payload.event.duration_seconds".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_DURATION_SECONDS{ "duration_seconds" };

/**
 * @brief Twitch EventSub message key "payload.event.from_broadcaster_user_id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_ID{ "from_broadcaster_user_id" };

/**
 * @brief Twitch EventSub message key "payload.event.from_broadcaster_user_login".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_LOGIN{ "from_broadcaster_user_login" };

/**
 * @brief Twitch EventSub message key "payload.event.from_broadcaster_user_name".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_NAME{ "from_broadcaster_user_name" };

/**
 * @brief Twitch EventSub message key "payload.event.id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_ID{ "id" };

/**
 * @brief Twitch EventSub message key "payload.event.is_automatic".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_IS_AUTOMATIC{ "is_automatic" };

/**
 * @brief Twitch EventSub message key "payload.event.requester_user_id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_ID{ "requester_user_id" };

/**
 * @brief Twitch EventSub message key "payload.event.requester_user_login".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_LOGIN{ "requester_user_login" };

/**
 * @brief Twitch EventSub message key "payload.event.requester_user_name".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_NAME{ "requester_user_name" };

/**
 * @brief Twitch EventSub message key "payload.event.reward".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD{ "reward" };

/**
 * @brief Twitch EventSub message key "payload.event.reward.cost".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD_COST{ "cost" };

/**
 * @brief Twitch EventSub message key "payload.event.reward.id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD_ID{ "id" };

/**
 * @brief Twitch EventSub message key "payload.event.reward.prompt".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD_PROMPT{ "prompt" };

/**
 * @brief Twitch EventSub message key "payload.event.reward.title".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD_TITLE{ "title" };

/**
 * @brief Twitch EventSub message key "payload.event.reward.type".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_REWARD_TYPE{ "type" };

/**
 * @brief Twitch EventSub message key "payload.event.started_at".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_STARTED_AT{ "started_at" };

/**
 * @brief Twitch EventSub message key "payload.event.status".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_STATUS{ "status" };

/**
 * @brief Twitch EventSub message key "payload.event.to_broadcaster_user_id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_ID{ "to_broadcaster_user_id" };

/**
 * @brief Twitch EventSub message key "payload.event.to_broadcaster_user_login".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_LOGIN{ "to_broadcaster_user_login" };

/**
 * @brief Twitch EventSub message key "payload.event.to_broadcaster_user_name".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_NAME{ "to_broadcaster_user_name" };

/**
 * @brief Twitch EventSub message key "payload.event.user_id".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_USER_ID{ "user_id" };

/**
 * @brief Twitch EventSub message key "payload.event.user_input".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_USER_INPUT{ "user_input" };

/**
 * @brief Twitch EventSub message key "payload.event.user_login".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_USER_LOGIN{ "user_login" };

/**
 * @brief Twitch EventSub message key "payload.event.user_name".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_USER_NAME{ "user_name" };

/**
 * @brief Twitch EventSub message key "payload.event.viewers".
 */
inline const std::string MESSAGE_PAYLOAD_EVENT_VIEWERS{ "viewers" };

/**
 * @brief Twitch EventSub message key "payload.session".
 */
inline const std::string MESSAGE_PAYLOAD_SESSION{ "session" };

/**
 * @brief Twitch EventSub message key "payload.session.id".
 */
inline const std::string MESSAGE_PAYLOAD_SESSION_ID{ "id" };

/**
 * @brief Twitch EventSub message key "payload.session.keepalive_timeout_seconds".
 */
inline const std::string MESSAGE_PAYLOAD_SESSION_KEEPALIVE{ "keepalive_timeout_seconds" };

/**
 * @brief Twitch EventSub message key "payload.session.reconnect_url".
 */
inline const std::string MESSAGE_PAYLOAD_SESSION_RECONNECT_URL{ "reconnect_url" };

} // Network::Twitch::EventSub::Keys

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Errors Definitions                                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::EventSub::Errors
{

/**
 * @brief Twitch EventSub error message for hostname resolution errors.
 */
inline const std::string SESSION_RESOLVE{ "Hostname resolution error." };

/**
 * @brief Twitch EventSub error message for connection errors.
 */
inline const std::string SESSION_CONNECT{ "Connection error." };

/**
 * @brief Twitch EventSub error message for SSL handshake errors.
 */
inline const std::string SESSION_SSL_HANDSHAKE{ "SSL handshake error." };

/**
 * @brief Twitch EventSub error message for websocket handshake errors.
 */
inline const std::string SESSION_WEBSOCKET_HANDSHAKE{ "Websocket handshake error." };

/**
 * @brief Twitch EventSub error message for network read errors.
 */
inline const std::string SESSION_READ{ "Network read error." };

/**
 * @brief Twitch EventSub error message for websocket message parsing errors.
 */
inline const std::string BAD_CONTENT{ "Bad content." };

} // Network::Twitch::EventSub::Errors

#endif // NETWORK_TWITCH_EVENT_SUB_CONSTANTS_HPP_INCLUDED