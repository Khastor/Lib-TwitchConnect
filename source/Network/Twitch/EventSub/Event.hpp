#ifndef NETWORK_TWITCH_EVENT_SUB_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Event Interface                                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Message.hpp>

namespace Network::Twitch::EventSub
{

class EventHandler;

/**
 * @brief Base class in the hierarchy of Twitch EventSub events.
 */
struct Event : public Network::Core::Message
{
    /**
     * @brief Destructor.
     */
    virtual ~Event() = default;

    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    virtual void Dispatch(
        EventHandler& pHandler) const = 0;
};

} // Network::Twitch::EventSub

#endif // NETWORK_TWITCH_EVENT_SUB_EVENT_HPP_INCLUDED