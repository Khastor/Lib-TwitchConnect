///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Service Implementation                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Commands/CreateEventSubSubscriptionCommand.hpp>
#include <Network/Twitch/API/Constants.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionError.hpp>
#include <Network/Twitch/API/Events/CreateEventSubSubscriptionEvent.hpp>
#include <Network/Twitch/EventSub/Constants.hpp>
#include <Network/Twitch/EventSub/Events/ChannelAdBreakBeginEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsAutomaticRewardRedemptionAddEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsCustomRewardRedemptionAddEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelRaidEvent.hpp>
#include <Network/Twitch/EventSub/Service.hpp>
#include <Network/Twitch/EventSub/Sessions/WebsocketSession.hpp>
#include <Network/Twitch/OAuth2/Commands/ValidateAccessTokensCommand.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/json.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <algorithm>
#include <array>
#include <utility>

namespace Network::Twitch::EventSub
{

const std::string LOG_CHANNEL{ "Twitch EventSub" };
const std::string LOG_FILENAME{ "TwitchEventSubServiceLogs.log" };

Service::Service() :
    mLogger(Network::Core::MakeLogger(LOG_CHANNEL, LOG_FILENAME)),
    mIOContext(std::make_unique<boost::asio::io_context>()),
    mWorker([this](std::stop_token pStopToken) { Loop(pStopToken); }),
    mTwitchAPIEventSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::API::Event>(this))
{}

Service::~Service()
{
    std::unique_lock<std::mutex> lLock(mMutex);
    mTwitchAPIEventSubscription.Revoke();
    mWorker.request_stop();
    mIOContext->stop();
}

void Service::Initialize()
{
    static Service sInstance;
}

void Service::Loop(
    std::stop_token pStopToken)
{
    NETWORK_CORE_LOG_INFO(mLogger) << "start";
    while (!pStopToken.stop_requested())
    {
        if (mRetryCount > 0)
        {
            const std::uint64_t lRetryDelay{ 1ULL << std::min(mRetryCount, 5ULL) };
            NETWORK_CORE_LOG_INFO(mLogger) << "connection retry #" << mRetryCount << " in " << lRetryDelay << "s";

            std::unique_lock<std::mutex> lLock{ mMutex };
            auto lStopCondition{ [pStopToken]() { return pStopToken.stop_requested(); } };
            if (mWaitCondition.wait_for(lLock, pStopToken, std::chrono::seconds(lRetryDelay), lStopCondition))
            {
                NETWORK_CORE_LOG_INFO(mLogger) << "connection aborted";
                continue;
            }
        }
        NETWORK_CORE_LOG_INFO(mLogger) << "connecting to " << Network::Twitch::EventSub::Endpoints::HOSTNAME << ":" << Network::Twitch::EventSub::Endpoints::PORT;
        mRetryCount++;

        std::make_shared<Network::Twitch::EventSub::Sessions::WebsocketSession>(
            mLogger, *this, *mIOContext)->Start();

        try
        {
            mIOContext->run();
        }
        catch (std::exception& lException)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << lException.what();
        }

        {
            std::unique_lock<std::mutex> lLock(mMutex);

            // Hard reset the IO context to discard all waiting handlers.
            mIOContext = std::make_unique<boost::asio::io_context>();
        }
    }
    NETWORK_CORE_LOG_INFO(mLogger) << "stop";
}

void Service::OnEventSubWebsocketSessionError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << pError;
    mIOContext->stop();
}

void Service::OnEventSubWebsocketMessageError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_WARNING(mLogger) << "parser error: " << pError;
}

void Service::OnEventSubWebsocketWelcomeMessage(
    const std::string& pSessionId,
    std::uint64_t pKeepalive)
{
    const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };

    // Handle the first welcome message since service start.
    if (std::exchange(mRetryCount, 0ULL) > 0ULL)
    {
        std::array lCreateEventSubSubscriptionCommands{
            std::make_unique<Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand>(
                pSessionId,
                Network::Twitch::EventSub::Subscriptions::CHANNEL_AD_BREAK_BEGIN,
                Network::Twitch::EventSub::Subscriptions::VERSION_1,
                boost::json::value{
                    { Network::Twitch::API::Keys::HTTP_CONTENT_CONDITION_BROADCASTER_USER_ID, lSettings.GetTwitchUserId() }
                },
                pKeepalive),
            std::make_unique<Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand>(
                pSessionId,
                Network::Twitch::EventSub::Subscriptions::CHANNEL_RAID,
                Network::Twitch::EventSub::Subscriptions::VERSION_1,
                boost::json::value{
                    { Network::Twitch::API::Keys::HTTP_CONTENT_CONDITION_TO_BROADCASTER_USER_ID, lSettings.GetTwitchUserId() }
                },
                pKeepalive),
            std::make_unique<Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand>(
                pSessionId,
                Network::Twitch::EventSub::Subscriptions::CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD,
                Network::Twitch::EventSub::Subscriptions::VERSION_1,
                boost::json::value{
                    { Network::Twitch::API::Keys::HTTP_CONTENT_CONDITION_BROADCASTER_USER_ID, lSettings.GetTwitchUserId() }
                },
                pKeepalive),
            std::make_unique<Network::Twitch::API::Commands::CreateEventSubSubscriptionCommand>(
                pSessionId,
                Network::Twitch::EventSub::Subscriptions::CHANNEL_POINTS_CUSTOM_REWARD_REDEMPTION_ADD,
                Network::Twitch::EventSub::Subscriptions::VERSION_1,
                boost::json::value{
                    { Network::Twitch::API::Keys::HTTP_CONTENT_CONDITION_BROADCASTER_USER_ID, lSettings.GetTwitchUserId() }
                },
                pKeepalive)
        };

        {
            std::unique_lock<std::mutex> lLock(mMutex);

            mEventSubSubscriptions.clear();
            for (auto&& lCreateEventSubSubscriptionCommand : lCreateEventSubSubscriptionCommands)
            {
                mEventSubSubscriptions.insert(std::make_pair(
                    lCreateEventSubSubscriptionCommand->CommandId,
                    lCreateEventSubSubscriptionCommand->SubscriptionType));
                Network::Core::Dispatcher::Instance().Publish(std::move(lCreateEventSubSubscriptionCommand));
            }
        }
    }
}

void Service::OnEventSubWebsocketKeepaliveMessage()
{
    NETWORK_CORE_LOG_TRACE(mLogger) << "keepalive";
}

void Service::OnEventSubWebsocketNotificationMessage(
    const std::string& pSubscriptionType,
    const boost::property_tree::ptree& pEvent)
{
    // Parse Channel Points Automatic Reward Redemption Add Event.
    // https://dev.twitch.tv/docs/eventsub/eventsub-reference/#channel-points-automatic-reward-redemption-add-event.
    if (pSubscriptionType == Network::Twitch::EventSub::Subscriptions::CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD)
    {
        auto lEvent{ std::make_unique<Network::Twitch::EventSub::Events::ChannelPointsAutomaticRewardRedemptionAddEvent>() };

        const auto lRedemptionId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_ID) };
        if (!lRedemptionId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RedemptionId = lRedemptionId.get();

        const auto lBroadcasterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID) };
        if (!lBroadcasterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserId = lBroadcasterUserId.get();

        const auto lBroadcasterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN) };
        if (!lBroadcasterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserLogin = lBroadcasterUserLogin.get();

        const auto lBroadcasterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME) };
        if (!lBroadcasterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserName = lBroadcasterUserName.get();

        const auto lUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_ID) };
        if (!lUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserId = lUserId.get();

        const auto lUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_LOGIN) };
        if (!lUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserLogin = lUserLogin.get();

        const auto lUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_NAME) };
        if (!lUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserName = lUserName.get();

        const auto lReward{ pEvent.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD) };
        if (!lReward)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }

        const auto lRewardType{ lReward.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_TYPE)};
        if (!lRewardType)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_TYPE;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RewardType = lRewardType.get();

        const auto lUserInput{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_INPUT) };
        if (!lUserInput)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_INPUT;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserInput = lUserInput.get();

        Network::Core::Dispatcher::Instance().Publish(std::move(lEvent));
        return;
    }

    // Parse Channel Points Custom Reward Redemption Add Event.
    // https://dev.twitch.tv/docs/eventsub/eventsub-reference/#channel-points-custom-reward-redemption-add-event.
    if (pSubscriptionType == Network::Twitch::EventSub::Subscriptions::CHANNEL_POINTS_CUSTOM_REWARD_REDEMPTION_ADD)
    {
        auto lEvent{ std::make_unique<Network::Twitch::EventSub::Events::ChannelPointsCustomRewardRedemptionAddEvent>() };

        const auto lRedemptionId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_ID) };
        if (!lRedemptionId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RedemptionId = lRedemptionId.get();

        const auto lRedemptionStatus{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_STATUS) };
        if (!lRedemptionStatus)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_STATUS;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RedemptionStatus = lRedemptionStatus.get();

        const auto lBroadcasterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID) };
        if (!lBroadcasterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserId = lBroadcasterUserId.get();

        const auto lBroadcasterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN) };
        if (!lBroadcasterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserLogin = lBroadcasterUserLogin.get();

        const auto lBroadcasterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME) };
        if (!lBroadcasterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserName = lBroadcasterUserName.get();

        const auto lUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_ID) };
        if (!lUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserId = lUserId.get();

        const auto lUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_LOGIN) };
        if (!lUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserLogin = lUserLogin.get();

        const auto lUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_NAME) };
        if (!lUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserName = lUserName.get();

        const auto lUserInput{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_INPUT) };
        if (!lUserInput)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_USER_INPUT;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->UserInput = lUserInput.get();

        const auto lReward{ pEvent.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD) };
        if (!lReward)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }

        const auto lRewardCost{ lReward.get().get_optional<std::uint64_t>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_COST) };
        if (!lRewardCost)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_COST;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RewardCost = lRewardCost.get();

        const auto lRewardId{ lReward.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_ID) };
        if (!lRewardId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RewardId = lRewardId.get();

        const auto lRewardPrompt{ lReward.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_PROMPT) };
        if (!lRewardPrompt)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_PROMPT;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RewardPrompt = lRewardPrompt.get();

        const auto lRewardTitle{ lReward.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_TITLE) };
        if (!lRewardTitle)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REWARD_TITLE;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RewardTitle = lRewardTitle.get();

        Network::Core::Dispatcher::Instance().Publish(std::move(lEvent));
        return;
    }

    // Parse Channel Ad Break Begin Event.
    // https://dev.twitch.tv/docs/eventsub/eventsub-reference/#channel-ad-break-begin-event.
    if (pSubscriptionType == Network::Twitch::EventSub::Subscriptions::CHANNEL_AD_BREAK_BEGIN)
    {
        auto lEvent{ std::make_unique<Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent>() };

        const auto lBroadcasterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID) };
        if (!lBroadcasterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserId = lBroadcasterUserId.get();

        const auto lBroadcasterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN) };
        if (!lBroadcasterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserLogin = lBroadcasterUserLogin.get();

        const auto lBroadcasterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME) };
        if (!lBroadcasterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_BROADCASTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->BroadcasterUserName = lBroadcasterUserName.get();

        const auto lRequesterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_ID) };
        if (!lRequesterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RequesterUserId = lRequesterUserId.get();

        const auto lRequesterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_LOGIN) };
        if (!lRequesterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RequesterUserLogin = lRequesterUserLogin.get();

        const auto lRequesterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_NAME) };
        if (!lRequesterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_REQUESTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->RequesterUserName = lRequesterUserName.get();

        const auto lDuration{ pEvent.get_optional<std::uint64_t>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_DURATION_SECONDS) };
        if (!lDuration)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_DURATION_SECONDS;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->Duration = lDuration.get();

        const auto lStartedAt{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_STARTED_AT) };
        if (!lStartedAt)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_STARTED_AT;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->StartedAt = lStartedAt.get();

        const auto lIsAutomatic{ pEvent.get_optional<bool>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_IS_AUTOMATIC) };
        if (!lIsAutomatic)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_IS_AUTOMATIC;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->IsAutomatic = lIsAutomatic.get();

        Network::Core::Dispatcher::Instance().Publish(std::move(lEvent));
        return;
    }

    // Parse Channel Raid Event.
    // https://dev.twitch.tv/docs/eventsub/eventsub-reference/#channel-raid-event.
    if (pSubscriptionType == Network::Twitch::EventSub::Subscriptions::CHANNEL_RAID)
    {
        auto lEvent{ std::make_unique<Network::Twitch::EventSub::Events::ChannelRaidEvent>() };

        const auto lFromBroadcasterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_ID) };
        if (!lFromBroadcasterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->FromBroadcasterUserId = lFromBroadcasterUserId.get();

        const auto lFromBroadcasterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_LOGIN) };
        if (!lFromBroadcasterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->FromBroadcasterUserLogin = lFromBroadcasterUserLogin.get();

        const auto lFromBroadcasterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_NAME) };
        if (!lFromBroadcasterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_FROM_BROADCASTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->FromBroadcasterUserName = lFromBroadcasterUserName.get();

        const auto lToBroadcasterUserId{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_ID) };
        if (!lToBroadcasterUserId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_ID;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->ToBroadcasterUserId = lToBroadcasterUserId.get();

        const auto lToBroadcasterUserLogin{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_LOGIN) };
        if (!lToBroadcasterUserLogin)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_LOGIN;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->ToBroadcasterUserLogin = lToBroadcasterUserLogin.get();

        const auto lToBroadcasterUserName{ pEvent.get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_NAME) };
        if (!lToBroadcasterUserName)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_TO_BROADCASTER_USER_NAME;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->ToBroadcasterUserName = lToBroadcasterUserName.get();

        const auto lViewers{ pEvent.get_optional<std::uint64_t>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_VIEWERS) };
        if (!lViewers)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT_VIEWERS;
            OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return;
        }
        lEvent->Viewers = lViewers.get();

        Network::Core::Dispatcher::Instance().Publish(std::move(lEvent));
        return;
    }
}

void Service::OnEventSubWebsocketReconnectMessage(
    const std::string& pReconnectURL)
{
    std::make_shared<Network::Twitch::EventSub::Sessions::WebsocketSession>(
        mLogger, *this, *mIOContext, pReconnectURL)->Start();
}

void Service::OnEventSubWebsocketRevocationMessage()
{
    mIOContext->stop();
    Network::Core::Dispatcher::Instance().Publish(
        std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
}

void Service::OnEvent(
    const Network::Twitch::API::Events::CreateEventSubSubscriptionEvent& pEvent)
{
    std::unique_lock<std::mutex> lLock(mMutex);
    if (auto lSubscription{ mEventSubSubscriptions.find(pEvent.CommandId) }; lSubscription != mEventSubSubscriptions.cend())
    {
        NETWORK_CORE_LOG_INFO(mLogger) << "subscription success: " << lSubscription->second;
    }
}

void Service::OnEvent(
    const Network::Twitch::API::Events::CreateEventSubSubscriptionError& pEvent)
{
    std::unique_lock<std::mutex> lLock(mMutex);
    if (auto lSubscription{ mEventSubSubscriptions.find(pEvent.CommandId) }; lSubscription != mEventSubSubscriptions.cend())
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << "subscription error: " << lSubscription->second;
        if (pEvent.Fatal)
        {
            mEventSubSubscriptions.clear();
            mIOContext->stop();
            Network::Core::Dispatcher::Instance().Publish(
                std::make_unique<Network::Twitch::OAuth2::Commands::ValidateAccessTokensCommand>());
        }
    }
}

} // Network::Twitch::EventSub