#ifndef NETWORK_TWITCH_EVENT_SUB_EVENT_HANDLER_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_EVENT_HANDLER_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub EventHandler Interface                                                        //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Observer.hpp>
#include <Network/Twitch/EventSub/Event.hpp>

namespace Network::Twitch::EventSub
{

namespace Events
{

struct ChannelAdBreakBeginEvent;
struct ChannelPointsAutomaticRewardRedemptionAddEvent;
struct ChannelPointsCustomRewardRedemptionAddEvent;
struct ChannelRaidEvent;

} // Events

/**
 * @brief Twitch EventSub event handler.
 *
 * Provide Twitch EventSub event handler methods that can be invoked asynchronously.
 */
class EventHandler : public Network::Core::Observer<Network::Twitch::EventSub::Event>
{
private:
    /**
     * @brief Message callback.
     *
     * The message callback is invoked asynchronously from the message dispatcher thread.
     * Dispatch the event message to the appropriate event handler method.
     *
     * @param pEvent The event message to handle.
     */
    void OnMessage(
        const Network::Twitch::EventSub::Event& pEvent) override;

public:
    /**
     * @brief Twitch EventSub Channel Ad Break Begin event handler.
     *
     * This handler is invoked when a user runs a midroll commercial break,
     * either manually or automatically via ads manager on the broadcaster's channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent& pEvent);

    /**
     * @brief Twitch EventSub Channel Points Automatic Reward Redemption Add event handler.
     *
     * This handler is invoked when a user redeems an automatic channel points reward on the broadcaster's channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelPointsAutomaticRewardRedemptionAddEvent& pEvent);

    /**
     * @brief Twitch EventSub Channel Points Custom Reward Redemption Add event handler.
     *
     * This handler is invoked when a user redeems a custom channel points reward on the broadcaster's channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelPointsCustomRewardRedemptionAddEvent& pEvent);

    /**
     * @brief Twitch EventSub Channel Raid event handler.
     *
     * This event is published when a broadcaster raids another broadcaster�s channel.
     *
     * @param pEvent The event details.
     */
    virtual void OnEvent(
        const Network::Twitch::EventSub::Events::ChannelRaidEvent& pEvent);
};

} // Network::Twitch::EventSub

#endif // NETWORK_TWITCH_EVENT_SUB_EVENT_HANDLER_HPP_INCLUDED