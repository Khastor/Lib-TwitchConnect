#ifndef NETWORK_TWITCH_EVENT_SUB_SESSIONS_WEBSOCKET_SESSION_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_SESSIONS_WEBSOCKET_SESSION_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub WebsocketSession Interface                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>
#include <Network/Core/WebsocketSession.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <memory>
#include <string>

namespace Network::Twitch::EventSub::Workflows
{

class WebsocketWorkflow;

} // Network::Twitch::OAuth2::Workflows

namespace Network::Twitch::EventSub::Sessions
{

/**
 * @brief Secure Twitch EventSub websocket session over SSL.
 *
 * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/.
 * @see https://dev.twitch.tv/docs/eventsub/websocket-reference/.
 */
class WebsocketSession : public Network::Core::WebsocketSession
{
public:
    /**
     * @brief Constructor.
     *
     * This class must be allocated dynamically using std::make_shared().
     *
     * @param pLogger The log source.
     * @param pWorkflow The parent workflow that will handle the session events.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     */
    WebsocketSession(
        Network::Core::Logger& pLogger,
        Network::Twitch::EventSub::Workflows::WebsocketWorkflow& pWorkflow,
        boost::asio::io_context& pIOContext);

    /**
     * @brief Constructor.
     *
     * This class must be allocated dynamically using std::make_shared().
     *
     * @param pLogger The log source.
     * @param pWorkflow The parent workflow that will handle the session events.
     * @param pIOContext The execution context that will invoke all the asynchrounous handlers, must run on a single thread.
     * @param pReconnectURL The reconnect URL.
     */
    WebsocketSession(
        Network::Core::Logger& pLogger,
        Network::Twitch::EventSub::Workflows::WebsocketWorkflow& pWorkflow,
        boost::asio::io_context& pIOContext,
        const std::string& pReconnectURL);

private:
    /**
     * @brief Resolve error handler.
     *
     * This handler is invoked when the session fails to find the remote host address.
     *
     * @param pError The error message.
     */
    void OnResolveError(
        const std::string& pError) override;

    /**
     * @brief Connect error handler.
     *
     * This handler is invoked when the session fails to connect to the remote host.
     *
     * @param pError The error message.
     */
    void OnConnectError(
        const std::string& pError) override;

    /**
     * @brief SSL handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate a secure connection with the remote host.
     * This can happen if the client cannot verify the SSL certificate chain of the remote host.
     *
     * @param pError The error message.
     */
    void OnSSLHandshakeError(
        const std::string& pError) override;

    /**
     * @brief Websocket handshake error handler.
     *
     * This handler is invoked when the session fails to negotiate the upgrade of the websocket connection with the remote host.
     *
     * @param pError The error message.
     */
    void OnWebsocketHandshakeError(
        const std::string& pError) override;

    /**
     * @brief Read error handler.
     *
     * This handler is invoked when the session fails to read a websocket message from the remote host.
     *
     * @param pError The error message.
     */
    void OnWebsocketReadError(
        const std::string& pError) override;

    /**
     * @brief Websocket message handler.
     *
     * This handler is invoked when the session receives a websocket message from the remote host.
     *
     * @param The websocket message.
     * @return True to keep the websocket connection alive, false to close the connection.
     */
    bool OnWebsocketMessage(
        const std::string& pMessage) override;

    /**
     * @brief The log source.
     */
    Network::Core::Logger& mLogger;

    /**
     * @brief The parent workflow that will handle the session events.
     */
    Network::Twitch::EventSub::Workflows::WebsocketWorkflow& mWorkflow;
};

} // Network::Twitch::EventSub::Sessions

#endif // NETWORK_TWITCH_EVENT_SUB_SESSIONS_WEBSOCKET_SESSION_HPP_INCLUDED