///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub WebsocketSession Implementation                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/EventSub/Constants.hpp>
#include <Network/Twitch/EventSub/Sessions/WebsocketSession.hpp>
#include <Network/Twitch/EventSub/Workflows/WebsocketWorkflow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>

namespace Network::Twitch::EventSub::Sessions
{

const std::string LOG_PREFIX{ "[WebsocketSession] " };
constexpr std::uint64_t WEBSOCKET_SESSION_TIMEOUT_SECONDS{ 30ULL };

WebsocketSession::WebsocketSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::EventSub::Workflows::WebsocketWorkflow& pWorkflow,
    boost::asio::io_context& pIOContext) :
    Network::Core::WebsocketSession(pLogger, pIOContext,
        Network::Twitch::EventSub::Endpoints::HOSTNAME,
        Network::Twitch::EventSub::Endpoints::PORT,
        Network::Twitch::EventSub::Endpoints::WEBSOCKET),
    mLogger(pLogger),
    mWorkflow(pWorkflow)
{
    SetIdleTimeout(WEBSOCKET_SESSION_TIMEOUT_SECONDS);
}

WebsocketSession::WebsocketSession(
    Network::Core::Logger& pLogger,
    Network::Twitch::EventSub::Workflows::WebsocketWorkflow& pWorkflow,
    boost::asio::io_context& pIOContext,
    const std::string& pReconnectURL) :
    Network::Core::WebsocketSession(pLogger, pIOContext,
        Network::Twitch::EventSub::Endpoints::HOSTNAME,
        Network::Twitch::EventSub::Endpoints::PORT,
        pReconnectURL),
    mLogger(pLogger),
    mWorkflow(pWorkflow)
{
    SetIdleTimeout(WEBSOCKET_SESSION_TIMEOUT_SECONDS);
}

void WebsocketSession::OnResolveError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "hostname resolution error: " << pError;
    mWorkflow.OnEventSubWebsocketSessionError(Network::Twitch::EventSub::Errors::SESSION_RESOLVE);
}

void WebsocketSession::OnConnectError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "connect error: " << pError;
    mWorkflow.OnEventSubWebsocketSessionError(Network::Twitch::EventSub::Errors::SESSION_CONNECT);
}

void WebsocketSession::OnSSLHandshakeError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "SSL handshake error: " << pError;
    mWorkflow.OnEventSubWebsocketSessionError(Network::Twitch::EventSub::Errors::SESSION_SSL_HANDSHAKE);
}

void WebsocketSession::OnWebsocketHandshakeError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "websocket handshake error: " << pError;
    mWorkflow.OnEventSubWebsocketSessionError(Network::Twitch::EventSub::Errors::SESSION_WEBSOCKET_HANDSHAKE);
}

void WebsocketSession::OnWebsocketReadError(
    const std::string& pError)
{
    NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "network read error: " << pError;
    mWorkflow.OnEventSubWebsocketSessionError(Network::Twitch::EventSub::Errors::SESSION_READ);
}

bool WebsocketSession::OnWebsocketMessage(
    const std::string& pMessage)
{
    NETWORK_CORE_LOG_TRACE(mLogger) << LOG_PREFIX << "message:\n" << pMessage;
    boost::property_tree::ptree lWebsocketMessageJSON;
    try
    {
        std::stringstream lWebsocketMessageBody{ pMessage };
        boost::property_tree::read_json(lWebsocketMessageBody, lWebsocketMessageJSON);

        const auto lMetadata{ lWebsocketMessageJSON.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_METADATA) };
        if (!lMetadata)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_METADATA;
            mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return true;
        }

        const auto lMessageType{ lMetadata.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE) };
        if (!lMessageType)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE;
            mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return true;
        }

        const auto lMessageId{ lMetadata.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_ID) };
        if (!lMessageId)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_ID;
            mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return true;
        }

        const auto lMessageTimestamp{ lMetadata.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TIMESTAMP) };
        if (!lMessageTimestamp)
        {
            NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TIMESTAMP;
            mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
            return true;
        }

        // TODO: check message id uniqueness + timestamp in workflow callback.

        // https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#welcome-message.
        if (lMessageType.get() == Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE_SESSION_WELCOME)
        {
            const auto lPayload{ lWebsocketMessageJSON.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD) };
            if (!lPayload)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lSession{ lPayload.get().get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION) };
            if (!lSession)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lSessionId{ lSession.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_ID) };
            if (!lSessionId)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_ID;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lSessionKeepalive{ lSession.get().get_optional<std::uint64_t>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_KEEPALIVE) };
            if (!lSessionKeepalive)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_KEEPALIVE;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            mWorkflow.OnEventSubWebsocketWelcomeMessage(lSessionId.get(), lSessionKeepalive.get());
            return true;
        }

        // https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#keepalive-message.
        if (lMessageType.get() == Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE_SESSION_KEEPALIVE)
        {
            mWorkflow.OnEventSubWebsocketKeepaliveMessage();
            return true;
        }

        // https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#notification-message.
        if (lMessageType.get() == Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE_NOTIFICATION)
        {
            const auto lSubscriptionType{ lMetadata.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_METADATA_SUBSCRIPTION_TYPE) };
            if (!lSubscriptionType)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_METADATA_SUBSCRIPTION_TYPE;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lPayload{ lWebsocketMessageJSON.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD) };
            if (!lPayload)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lEvent{ lPayload.get().get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT) };
            if (!lEvent)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_EVENT;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            mWorkflow.OnEventSubWebsocketNotificationMessage(lSubscriptionType.get(), lEvent.get());
            return true;
        }

        // https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#reconnect-message.
        if (lMessageType.get() == Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE_SESSION_RECONNECT)
        {
            const auto lPayload{ lWebsocketMessageJSON.get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD) };
            if (!lPayload)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lSession{ lPayload.get().get_child_optional(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION) };
            if (!lSession)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            const auto lReconnectURL{ lSession.get().get_optional<std::string>(Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_RECONNECT_URL) };
            if (!lReconnectURL)
            {
                NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "missing mandatory field: " << Network::Twitch::EventSub::Keys::MESSAGE_PAYLOAD_SESSION_RECONNECT_URL;
                mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
                return true;
            }

            mWorkflow.OnEventSubWebsocketReconnectMessage(lReconnectURL.get());
            return true;
        }

        // https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#revocation-message.
        if (lMessageType.get() == Network::Twitch::EventSub::Keys::MESSAGE_METADATA_MESSAGE_TYPE_REVOCATION)
        {
            mWorkflow.OnEventSubWebsocketRevocationMessage();
            return true;
        }
    }
    catch (boost::property_tree::json_parser_error& lException)
    {
        NETWORK_CORE_LOG_ERROR(mLogger) << LOG_PREFIX << "bad content: " << lException.what();
        mWorkflow.OnEventSubWebsocketMessageError(Network::Twitch::EventSub::Errors::BAD_CONTENT);
        return true;
    }
    return true;
}

} // Network::Twitch::EventSub::Sessions