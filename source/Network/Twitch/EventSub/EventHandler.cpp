///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub EventHandler Implementation                                                   //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/Event.hpp>
#include <Network/Twitch/EventSub/EventHandler.hpp>

namespace Network::Twitch::EventSub
{

void EventHandler::OnMessage(
    const Network::Twitch::EventSub::Event& pEvent)
{
    pEvent.Dispatch(*this);
}

void EventHandler::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelPointsAutomaticRewardRedemptionAddEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelPointsCustomRewardRedemptionAddEvent&)
{}

void EventHandler::OnEvent(
    const Network::Twitch::EventSub::Events::ChannelRaidEvent&)
{}

} // Network::Twitch::EventSub