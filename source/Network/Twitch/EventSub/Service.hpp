#ifndef NETWORK_TWITCH_EVENT_SUB_SERVICE_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_SERVICE_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub Service Interface                                                             //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Logging.hpp>
#include <Network/Twitch/API/EventHandler.hpp>
#include <Network/Twitch/EventSub/Workflows/WebsocketWorkflow.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/asio/io_context.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <condition_variable>
#include <cstddef>
#include <cstdint>
#include <memory>
#include <mutex>
#include <string>
#include <thread>
#include <unordered_map>

namespace Network::Twitch::EventSub
{

/**
 * @brief Twitch EventSub service.
 *
 * Receive Twitch EventSub websocket messages.
 */
class Service :
    public Network::Twitch::EventSub::Workflows::WebsocketWorkflow,
    public Network::Twitch::API::EventHandler
{
private:
    /**
     * @brief Constructor.
     *
     * Start receiving Twitch EventSub websocket messages.
     */
    Service();

public:
    /**
     * @brief Destructor.
     *
     * Stop receiving Twitch EventSub websocket messages.
     */
    ~Service();

    /**
     * @brief Deleted copy-constructor.
     */
    Service(const Service&) = delete;

    /**
     * @brief Deleted move-constructor.
     */
    Service(Service&&) = delete;

    /**
     * @brief Deleted copy-assignment operator.
     */
    Service& operator==(const Service&) = delete;

    /**
     * @brief Deleted move-assignment operator.
     */
    Service& operator==(Service&&) = delete;

    /**
     * @brief Initialize the Twitch EventSub service.
     *
     * Start receiving Twitch EventSub websocket messages.
     */
    static void Initialize();

private:
    /**
     * @brief Worker thread main loop.
     *
     * Invoke asynchronous Twitch EventSub websocket session handlers.
     * The main loop is started upon construction, and stopped upon destruction.
     *
     * @param pStopToken Signal sent upon destruction to request the main loop to stop.
     */
    void Loop(
        std::stop_token pStopToken);

    /**
     * @brief Twitch EventSub websocket session error handler.
     *
     * This handler is invoked when a websocket session fails due to a connection error.
     *
     * @param pError The error details.
     */
    void OnEventSubWebsocketSessionError(
        const std::string& pError) override;

    /**
     * @brief Twitch EventSub websocket message error handler.
     *
     * This handler is invoked when a websocket session fails due to a websocket message error.
     *
     * @param pError The error details.
     */
    void OnEventSubWebsocketMessageError(
        const std::string& pError) override;

    /**
     * @brief Twitch EventSub websocket welcome message handler.
     *
     * This handler is invoked when a websocket session receives a welcome message.
     *
     * @param pSessionId The websocket session id.
     * @param pKeepalive The keepalive timeout, in seconds.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#welcome-message.
     */
    void OnEventSubWebsocketWelcomeMessage(
        const std::string& pSessionId,
        std::uint64_t pKeepalive) override;

    /**
     * @brief Twitch EventSub websocket keepalive message handler.
     *
     * This handler is invoked when a websocket session receives a keepalive message.
     *
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#keepalive-message.
     */
    void OnEventSubWebsocketKeepaliveMessage() override;

    /**
     * @brief Twitch EventSub websocket notification message handler.
     *
     * This handler is invoked when a websocket session receives a notification message.
     *
     * @param pSubscriptionType The type of event sent in the notification message.
     * @param pEvent The event details.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#notification-message.
     */
    void OnEventSubWebsocketNotificationMessage(
        const std::string& pSubscriptionType,
        const boost::property_tree::ptree& pEvent) override;

    /**
     * @brief Twitch EventSub websocket reconnect message handler.
     *
     * This handler is invoked when a websocket session receives a reconnect message.
     *
     * @param pSessionId The websocket session reconnect URL.
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#reconnect-message.
     */
    void OnEventSubWebsocketReconnectMessage(
        const std::string& pReconnectURL) override;

    /**
     * @brief Twitch EventSub websocket revocation message handler.
     *
     * This handler is invoked when a websocket session receives a revocation message.
     *
     * @see https://dev.twitch.tv/docs/eventsub/handling-websocket-events/#revocation-message.
     */
    void OnEventSubWebsocketRevocationMessage() override;

    /**
     * @brief Twitch API Create EventSub Subscription event handler.
     *
     * This handler is invoked when the Twitch API Create EventSub Subscription command terminates successfully.
     *
     * @param pEvent The event details.
     */
    void OnEvent(
        const Network::Twitch::API::Events::CreateEventSubSubscriptionEvent& pEvent) override;

    /**
     * @brief Twitch API Create EventSub Subscription error handler.
     *
     * This handler is invoked when the Twitch API Create EventSub Subscription command produces an error.
     *
     * @param pEvent The event details.
     */
    void OnEvent(
        const Network::Twitch::API::Events::CreateEventSubSubscriptionError& pEvent) override;

    /**
     * @brief The Twitch EventSub service log source.
     */
    Network::Core::Logger mLogger;

    /**
     * @brief Track the status of the published Twitch API Create EventSub Subscription commands.
     *
     * The Twitch API Create EventSub Subscription command id is mapped to its subscription type.
     */
    std::unordered_map<std::uint64_t, std::string> mEventSubSubscriptions;

    /**
     * @brief The IO context.
     *
     * The IO context is wrapped inside a unique pointer to enable explicit lifetime management.
     * Upon restart, a new instance of IO context can be constructed to discard all waiting handlers.
     */
    std::unique_ptr<boost::asio::io_context> mIOContext;

    /**
     * @brief The current retry streak.
     */
    std::size_t mRetryCount{ 0ULL };

    /**
     * @brief Worker thread wait condition.
     *
     * Wake up the worker thread after waiting before a retry.
     */
    std::condition_variable_any mWaitCondition;

    /**
     * @brief Mutual exclusion to synchronize the worker thread, the message dispatcher thread and the service destruction.
     *
     * Protect the IO context pointer, the wait condition and the list of tracked EventSub subscription commands.
     */
    std::mutex mMutex;

    /**
     * @brief Worker thread.
     *
     * Invoke asynchronous Twitch EventSub websocket session handlers.
     * Start executing the main loop upon construction, and stop upon destruction.
     */
    std::jthread mWorker;

    /**
     * @brief Subscription to Twitch API Event messages.
     */
    Network::Core::Dispatcher::Subscription mTwitchAPIEventSubscription;
};

} // Network::Twitch::EventSub

#endif // NETWORK_TWITCH_EVENT_SUB_SERVICE_HPP_INCLUDED