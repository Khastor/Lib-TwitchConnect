///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelAdBreakBeginEvent Implementation                                       //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/EventHandler.hpp>
#include <Network/Twitch/EventSub/Events/ChannelAdBreakBeginEvent.hpp>

namespace Network::Twitch::EventSub::Events
{

void ChannelAdBreakBeginEvent::Dispatch(
    Network::Twitch::EventSub::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::EventSub::Events