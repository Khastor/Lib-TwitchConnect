#ifndef NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_AD_BREAK_BEGIN_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_AD_BREAK_BEGIN_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelAdBReakBeginEvent Interface                                            //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::EventSub::Events
{

/**
 * @brief Twitch EventSub Channel Ad Break Begin event.
 *
 * This event is published when a user runs a midroll commercial break,
 * either manually or automatically via ads manager on the broadcaster's channel.
 */
struct ChannelAdBreakBeginEvent : public Network::Twitch::EventSub::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::EventSub::EventHandler& pHandler) const override;

    /**
     * @brief The user id of the channel where the the ad break was run on.
     */
    std::string BroadcasterUserId;

    /**
     * @brief The user login of the channel where the the ad break was run on.
     */
    std::string BroadcasterUserLogin;

    /**
     * @brief The display name of the channel where the the ad break was run on.
     */
    std::string BroadcasterUserName;

    /**
     * @brief The user id of the user who requested the ad break.
     */
    std::string RequesterUserId;

    /**
     * @brief The user login of the user who requested the ad break.
     */
    std::string RequesterUserLogin;

    /**
     * @brief The display name of the user who requested the ad break.
     */
    std::string RequesterUserName;

    /**
     * @brief The length in seconds of the ad break requested.
     */
    std::uint64_t Duration;

    /**
     * @brief The UTC timestamp of when the ad break began, in RFC3339 format.
     */
    std::string StartedAt;

    /**
     * @brief Indicates if the ad break was automatically scheduled via ads manager.
     */
    bool IsAutomatic;
};

} // Network::Twitch::EventSub::Events

#endif // NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_AD_BREAK_BEGIN_EVENT_HPP_INCLUDED