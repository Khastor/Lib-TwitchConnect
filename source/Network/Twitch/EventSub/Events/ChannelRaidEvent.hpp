#ifndef NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_RAID_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_RAID_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelRaidEvent Interface                                                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <cstdint>
#include <string>

namespace Network::Twitch::EventSub::Events
{

/**
 * @brief Twitch EventSub Channel Raid event.
 *
 * This event is published when a broadcaster raids another broadcaster�s channel.
 */
struct ChannelRaidEvent: public Network::Twitch::EventSub::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::EventSub::EventHandler& pHandler) const override;

    /**
     * @brief The user id of the channel that created the raid.
     */
    std::string FromBroadcasterUserId;

    /**
     * @brief The user login of the channel that created the raid.
     */
    std::string FromBroadcasterUserLogin;

    /**
     * @brief The display name of the channel that created the raid.
     */
    std::string FromBroadcasterUserName;

    /**
     * @brief The user id of the channel that received the raid.
     */
    std::string ToBroadcasterUserId;

    /**
     * @brief The user login the channel that received the raid.
     */
    std::string ToBroadcasterUserLogin;

    /**
     * @brief The display name the channel that received the raid.
     */
    std::string ToBroadcasterUserName;

    /**
     * @brief The number of viewers in the raid.
     */
    std::uint64_t Viewers;
};

} // Network::Twitch::EventSub::Events

#endif // NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_RAID_EVENT_HPP_INCLUDED