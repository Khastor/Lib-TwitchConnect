///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelRaidEvent Implementation                                               //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/EventHandler.hpp>
#include <Network/Twitch/EventSub/Events/ChannelRaidEvent.hpp>

namespace Network::Twitch::EventSub::Events
{

void ChannelRaidEvent::Dispatch(
    Network::Twitch::EventSub::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::EventSub::Events