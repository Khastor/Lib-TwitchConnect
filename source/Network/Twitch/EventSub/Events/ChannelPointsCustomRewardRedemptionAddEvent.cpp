///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelPointsCustomRewardRedemptionAddEvent Implementation                    //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/EventHandler.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsCustomRewardRedemptionAddEvent.hpp>

namespace Network::Twitch::EventSub::Events
{

void ChannelPointsCustomRewardRedemptionAddEvent::Dispatch(
    Network::Twitch::EventSub::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::EventSub::Events