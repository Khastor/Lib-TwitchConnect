#ifndef NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD_EVENT_HPP_INCLUDED
#define NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD_EVENT_HPP_INCLUDED

///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelPointsAutomaticRewardRedemptionAddEvent Interface                      //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/Event.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <string>

namespace Network::Twitch::EventSub::Events
{

/**
 * @brief Twitch EventSub Channel Points Automatic Reward Redemption Add event.
 *
 * This event is published when a user redeems an automatic channel point reward on the broadcaster's channel.
 */
struct ChannelPointsAutomaticRewardRedemptionAddEvent : public Network::Twitch::EventSub::Event
{
    /**
     * @brief Dispatch the event to the appropriate event handler method.
     *
     * @param pHandler The event handler.
     */
    void Dispatch(
        Network::Twitch::EventSub::EventHandler& pHandler) const override;

    /**
     * @brief The id of the channel points reward redemption.
     */
    std::string RedemptionId;

    /**
     * @brief The user id of the channel where the reward was redeemed.
     */
    std::string BroadcasterUserId;

    /**
     * @brief The user login of the channel where the reward was redeemed.
     */
    std::string BroadcasterUserLogin;

    /**
     * @brief The display name of the channel where the reward was redeemed.
     */
    std::string BroadcasterUserName;

    /**
     * @brief The user id of the user who redeemed the channel points reward.
     */
    std::string UserId;

    /**
     * @brief The user login of the user who redeemed the channel points reward.
     */
    std::string UserLogin;

    /**
     * @brief The display name of the user who redeemed the channel points reward.
     */
    std::string UserName;

    /**
     * @brief The type of automatic channel points reward that was redeemed.
     */
    std::string RewardType;

    /**
     * @brief The message entered by the user who redeemed the channel points reward, if the reward requires input.
     */
    std::string UserInput;
};

} // Network::Twitch::EventSub::Events

#endif // NETWORK_TWITCH_EVENT_SUB_EVENTS_CHANNEL_POINTS_AUTOMATIC_REWARD_REDEMPTION_ADD_EVENT_HPP_INCLUDED