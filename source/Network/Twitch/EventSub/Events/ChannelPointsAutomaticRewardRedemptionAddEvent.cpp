///////////////////////////////////////////////////////////////////////////////////////////////////
// Twitch EventSub ChannelPointsAutomaticRewardRedemptionAddEvent Implementation                 //
///////////////////////////////////////////////////////////////////////////////////////////////////

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/EventSub/EventHandler.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsAutomaticRewardRedemptionAddEvent.hpp>

namespace Network::Twitch::EventSub::Events
{

void ChannelPointsAutomaticRewardRedemptionAddEvent::Dispatch(
    Network::Twitch::EventSub::EventHandler& pHandler) const
{
    pHandler.OnEvent(*this);
}

} // Network::Twitch::EventSub::Events