// -------------------------------------------------------------------------------- Project Headers
#include <Network/Core/Dispatcher.hpp>
#include <Network/Core/Settings.hpp>
#include <Network/Twitch/API/Commands/SendChatAnnouncementCommand.hpp>
#include <Network/Twitch/API/Service.hpp>
#include <Network/Twitch/EventSub/Events/ChannelAdBreakBeginEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelPointsAutomaticRewardRedemptionAddEvent.hpp>
#include <Network/Twitch/EventSub/Events/ChannelRaidEvent.hpp>
#include <Network/Twitch/EventSub/Service.hpp>
#include <Network/Twitch/Extensions/Heat/Service.hpp>
#include <Network/Twitch/Extensions/Heat/Events/UserClickEvent.hpp>
#include <Network/Twitch/IRC/Commands/SendChatMessageCommand.hpp>
#include <Network/Twitch/IRC/EventHandler.hpp>
#include <Network/Twitch/IRC/Events/UserJoinEvent.hpp>
#include <Network/Twitch/IRC/Events/UserMessageEvent.hpp>
#include <Network/Twitch/IRC/Events/UserPartEvent.hpp>
#include <Network/Twitch/IRC/Service.hpp>
#include <Network/Twitch/OAuth2/Constants.hpp>
#include <Network/Twitch/OAuth2/Service.hpp>

// --------------------------------------------------------------------------------- System Headers
#include <format>
#include <iostream>
#include <string>

/**
 * @brief Twitch IRC event observer example.
 *
 * Greet users joining the Twitch IRC channel.
 */
class HelloTwitchBot : public Network::Twitch::IRC::EventHandler
{
public:
    HelloTwitchBot() :
        mTwitchIRCEventSubscription(Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::IRC::Event>(this))
    {}

    ~HelloTwitchBot()
    {
        mTwitchIRCEventSubscription.Revoke();
    }

private:
    void OnEvent(
        const Network::Twitch::IRC::Events::StatusChangeEvent& pEvent) override
    {}

    void OnEvent(
        const Network::Twitch::IRC::Events::UserJoinEvent& pEvent) override
    {
        const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };
        if (pEvent.UserLogin != lSettings.GetTwitchUserLogin())
        {
            auto lCommand{ std::make_unique<Network::Twitch::IRC::Commands::SendChatMessageCommand>() };
            lCommand->Message = "Welcome @" + pEvent.UserLogin;
            Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
        }
    }

    void OnEvent(
        const Network::Twitch::IRC::Events::UserPartEvent& pEvent) override
    {}

    void OnEvent(
        const Network::Twitch::IRC::Events::UserMessageEvent& pEvent) override
    {}

    void OnEvent(
        const Network::Twitch::IRC::Events::UserWhisperEvent& pEvent) override
    {}

    Network::Core::Dispatcher::Subscription mTwitchIRCEventSubscription;
};

int main()
{
    // Load network settings and credentials.
    Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };
    lSettings.LoadFromFile("settings.xml");
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHANNEL_READ_ADS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHANNEL_READ_REDEMPTIONS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::MODERATOR_MANAGE_ANNOUNCEMENTS);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHAT_READ);
    lSettings.AddTwitchUserAccessScope(Network::Twitch::OAuth2::Scopes::CHAT_EDIT);

    // Twitch OAuth2 service: connect to Twitch, required by the Twitch API and IRC services.
    Network::Twitch::OAuth2::Service::Initialize();

    // Twitch API service: process Twitch API requests.
    Network::Twitch::API::Service::Initialize();

    // Twitch IRC service: read and write messages in the Twitch IRC channel.
    Network::Twitch::IRC::Service::Initialize();

    // Twitch EventSub service: receive Twitch EventSub notifications.
    Network::Twitch::EventSub::Service::Initialize();

    // Twitch Heat extension service: receive user clicks on the video player.
    Network::Twitch::Extensions::Heat::Service::Initialize();

    // Twitch IRC event callback example.
    auto lUserMessageSubscription{
        Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::IRC::Events::UserMessageEvent>(
            [](const Network::Twitch::IRC::Events::UserMessageEvent& pEvent)
            {
                const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };
                if (pEvent.UserLogin == lSettings.GetTwitchUserLogin())
                {
                    auto lCommand{ std::make_unique<Network::Twitch::API::Commands::SendChatAnnouncementCommand>() };
                    lCommand->Message = pEvent.Message;
                    lCommand->Color = "primary";
                    Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
                }
            })
    };

    // Twitch EventSub event callback example.
    auto lChannelPointsRedemptionSubscription{
        Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::EventSub::Events::ChannelPointsAutomaticRewardRedemptionAddEvent>(
            [](const Network::Twitch::EventSub::Events::ChannelPointsAutomaticRewardRedemptionAddEvent& pEvent)
            {
                const Network::Core::Settings& lSettings{ Network::Core::Settings::Instance() };
                if (pEvent.BroadcasterUserLogin == lSettings.GetTwitchUserLogin())
                {
                    auto lCommand{ std::make_unique<Network::Twitch::API::Commands::SendChatAnnouncementCommand>() };
                    lCommand->Message = pEvent.UserName + " redeemed " + pEvent.RewardType;
                    lCommand->Color = "primary";
                    Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
                }
            })
    };

    // Twitch EventSub event callback example.
    auto lTwitchAdBreakSubscription{
        Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent>(
            [](const Network::Twitch::EventSub::Events::ChannelAdBreakBeginEvent& pEvent)
            {
                auto lCommand{ std::make_unique<Network::Twitch::API::Commands::SendChatAnnouncementCommand>() };
                lCommand->Message = std::format("Ad break started ({}s)", pEvent.Duration);
                lCommand->Color = "primary";
                Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
            })
    };

    // Twitch EventSub event callback example.
    auto lTwitchRaidSubscription{
        Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::EventSub::Events::ChannelRaidEvent>(
            [](const Network::Twitch::EventSub::Events::ChannelRaidEvent& pEvent)
            {
                auto lCommand{ std::make_unique<Network::Twitch::API::Commands::SendChatAnnouncementCommand>() };
                lCommand->Message = std::format("{} is raiding with a party of {} viewers!", pEvent.FromBroadcasterUserName, pEvent.Viewers);
                lCommand->Color = "primary";
                Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
            })
    };

    // Twitch Heat extension event callback example.
    auto lTwitchHeatUserClickSubscription{
        Network::Core::Dispatcher::Instance().Subscribe<Network::Twitch::Extensions::Heat::Events::UserClickEvent>(
            [](const Network::Twitch::Extensions::Heat::Events::UserClickEvent& pEvent)
            {
                auto lCommand{ std::make_unique<Network::Twitch::IRC::Commands::SendChatMessageCommand>() };
                lCommand->Message = std::format("User click: ({},{})", pEvent.CursorX, pEvent.CursorY);
                Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
            })
    };

    HelloTwitchBot lBot;

    std::string lMessage;
    while (std::getline(std::cin, lMessage))
    {
        if (lMessage.empty())
        {
            break;
        }

        auto lCommand{ std::make_unique<Network::Twitch::IRC::Commands::SendChatMessageCommand>() };
        lCommand->Message = lMessage;
        Network::Core::Dispatcher::Instance().Publish(std::move(lCommand));
    }
}