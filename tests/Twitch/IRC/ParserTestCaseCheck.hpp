#ifndef NETWORK_TWITCH_IRC_PARSER_TEST_CASE_CHECK_HPP_INCLUDED
#define NETWORK_TWITCH_IRC_PARSER_TEST_CASE_CHECK_HPP_INCLUDED

// -------------------------------------------------------------------------------- Project Headers
#include <Network/Twitch/IRC/Parser.hpp>

class ParserTestCaseCheck : public Network::Twitch::IRC::Parser
{
protected:
    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_YOURHOST& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_CREATED& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MYINFO& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTDSTART& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTD& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFMOTD& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFNAMES& pMessageData) override;

    void OnParseMessage(
        const Network::Twitch::IRC::Parser::NUMERIC_ERR_UNKNOWNCOMMAND& pMessageData) override;
};

#endif // NETWORK_TWITCH_IRC_PARSER_TEST_CASE_CHECK_HPP_INCLUDED
