// -------------------------------------------------------------------------------- Project Headers
#include <Twitch/IRC/ParserTestCaseCheck.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#include <boost/test/unit_test.hpp>

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_YOURHOST& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_CREATED& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MYINFO& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTDSTART& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTD& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFMOTD& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFNAMES& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}

void ParserTestCaseCheck::OnParseMessage(
    const Network::Twitch::IRC::Parser::NUMERIC_ERR_UNKNOWNCOMMAND& pMessageData)
{
    BOOST_ERROR("Unexpected message data type");
}
