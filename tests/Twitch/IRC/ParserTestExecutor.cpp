// -------------------------------------------------------------------------------- Project Headers
#include <Twitch/IRC/ParserTestCaseCheck.hpp>

// ---------------------------------------------------------------------------------- Boost Headers
#define BOOST_TEST_MODULE Twitch IRC Parser
#include <boost/test/included/unit_test.hpp>

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandCapAck)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.HasCommandsCap, true);
            BOOST_CHECK_EQUAL(pMessageData.HasMembershipCap, true);
            BOOST_CHECK_EQUAL(pMessageData.HasTagsCap, true);
        }
    } lCheckCommandWithSupportedCapabilities;
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * ACK :twitch.tv/commands twitch.tv/membership twitch.tv/tags\r\n"));
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * ACK :twitch.tv/membership twitch.tv/tags twitch.tv/commands\r\n"));
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * ACK :twitch.tv/tags twitch.tv/example twitch.tv/membership twitch.tv/commands\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CAP_ACK& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.HasCommandsCap, false);
            BOOST_CHECK_EQUAL(pMessageData.HasMembershipCap, false);
            BOOST_CHECK_EQUAL(pMessageData.HasTagsCap, false);
        }
    } lCheckCommandWithoutSupportedCapabilities;
    BOOST_CHECK(lCheckCommandWithoutSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * ACK :twitch.tv/example\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandCapNak)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.HasCommandsCap, true);
            BOOST_CHECK_EQUAL(pMessageData.HasMembershipCap, true);
            BOOST_CHECK_EQUAL(pMessageData.HasTagsCap, true);
        }
    } lCheckCommandWithSupportedCapabilities;
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * NAK :twitch.tv/commands twitch.tv/membership twitch.tv/tags\r\n"));
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * NAK :twitch.tv/membership twitch.tv/tags twitch.tv/commands\r\n"));
    BOOST_CHECK(lCheckCommandWithSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * NAK :twitch.tv/tags twitch.tv/example twitch.tv/membership twitch.tv/commands\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CAP_NAK& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.HasCommandsCap, false);
            BOOST_CHECK_EQUAL(pMessageData.HasMembershipCap, false);
            BOOST_CHECK_EQUAL(pMessageData.HasTagsCap, false);
        }
    } lCheckCommandWithoutSupportedCapabilities;
    BOOST_CHECK(lCheckCommandWithoutSupportedCapabilities.Dispatch(":tmi.twitch.tv CAP * NAK :twitch.tv/example\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandClearChat)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK(!pMessageData.UserLogin.has_value());
            BOOST_CHECK(!pMessageData.Timeout.has_value());
            BOOST_CHECK(!pMessageData.Timestamp.has_value());
        }
    } lCheckCommandClearChatMessagesWithoutTags;
    BOOST_CHECK(lCheckCommandClearChatMessagesWithoutTags.Dispatch(":tmi.twitch.tv CLEARCHAT #dallas\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("ronni"));
            BOOST_CHECK(!pMessageData.Timeout.has_value());
            BOOST_CHECK(!pMessageData.Timestamp.has_value());

        }
    } lCheckCommandClearUserMessagesWithoutTags;
    BOOST_CHECK(lCheckCommandClearUserMessagesWithoutTags.Dispatch(":tmi.twitch.tv CLEARCHAT #dallas :ronni\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK(!pMessageData.UserLogin.has_value());
            BOOST_CHECK(!pMessageData.Timeout.has_value());
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1642715695392);
        }
    } lCheckCommandClearChatMessagesWithTags;
    BOOST_CHECK(lCheckCommandClearChatMessagesWithTags.Dispatch("@room-id=12345678;tmi-sent-ts=1642715695392 :tmi.twitch.tv CLEARCHAT #dallas\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("87654321"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("ronni"));
            BOOST_CHECK(!pMessageData.Timeout.has_value());
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1642715756806);
        }
    } lCheckCommandBanUserWithTags;
    BOOST_CHECK(lCheckCommandBanUserWithTags.Dispatch("@room-id=12345678;target-user-id=87654321;tmi-sent-ts=1642715756806 :tmi.twitch.tv CLEARCHAT #dallas :ronni\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARCHAT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("87654321"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.Timeout.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timeout.value(), 350);
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1642719320727LL);
        }
    } lCheckCommandTimeoutUserWithTags;
    BOOST_CHECK(lCheckCommandTimeoutUserWithTags.Dispatch("@ban-duration=350;room-id=12345678;target-user-id=87654321;tmi-sent-ts=1642719320727 :tmi.twitch.tv CLEARCHAT #dallas :ronni\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandClearMsg)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("HeyGuys"));
            BOOST_CHECK(!pMessageData.MessageId.has_value());
            BOOST_CHECK(!pMessageData.Timestamp.has_value());
        }
    } lCheckCommandClearMessageWithoutTags;
    BOOST_CHECK(lCheckCommandClearMessageWithoutTags.Dispatch(":tmi.twitch.tv CLEARMSG #dallas :HeyGuys\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_CLEARMSG& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("ronni"));
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("HeyGuys"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("abc-123-def"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1642720582342LL);
        }
    } lCheckCommandClearMessageWithTags;
    BOOST_CHECK(lCheckCommandClearMessageWithTags.Dispatch("@login=ronni;room-id=;target-msg-id=abc-123-def;tmi-sent-ts=1642720582342 :tmi.twitch.tv CLEARMSG #dallas :HeyGuys\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandGlobalUserState)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK(!pMessageData.UserName.has_value());
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(!pMessageData.UserColor.has_value());
        }
    } lCheckCommandGlobalUserStateWithoutTags;
    BOOST_CHECK(lCheckCommandGlobalUserStateWithoutTags.Dispatch(":tmi.twitch.tv GLOBALUSERSTATE\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_GLOBALUSERSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("dallas"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("admin"));
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x0D);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x42);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0x00);
        }
    } lCheckCommandGlobalUserStateWithTags;
    BOOST_CHECK(lCheckCommandGlobalUserStateWithTags.Dispatch("@badge-info=subscriber/8;badges=subscriber/6;color=#0D4200;display-name=dallas;emote-sets=0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239;turbo=0;user-id=12345678;user-type=admin :tmi.twitch.tv GLOBALUSERSTATE\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandHostTarget)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.HostedChannel, std::string("xyz"));
            BOOST_CHECK_EQUAL(pMessageData.HostingChannel, std::string("abc"));
            BOOST_CHECK_EQUAL(pMessageData.ViewerCount, 10);
        }
    } lCheckCommandHostTargetStartHostingChannel;
    BOOST_CHECK(lCheckCommandHostTargetStartHostingChannel.Dispatch(":tmi.twitch.tv HOSTTARGET #abc :xyz 10\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_HOSTTARGET& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(pMessageData.HostedChannel.empty());
            BOOST_CHECK_EQUAL(pMessageData.HostingChannel, std::string("abc"));
            BOOST_CHECK_EQUAL(pMessageData.ViewerCount, 10);
        }
    } lCheckCommandHostTargetStopHostingChannel;
    BOOST_CHECK(lCheckCommandHostTargetStopHostingChannel.Dispatch(":tmi.twitch.tv HOSTTARGET #abc :- 10\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandJoin)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_JOIN& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("twitchdev"));
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("bar"));
        }
    } lCheckCommandJoin;
    BOOST_CHECK(lCheckCommandJoin.Dispatch(":bar!bar@bar.tmi.twitch.tv JOIN #twitchdev\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandNotice)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(pMessageData.Channel.empty());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("Login authentication failed"));
            BOOST_CHECK(!pMessageData.NoticeType.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
        }
    } lCheckCommandNoticeLoginAuthFailed;
    BOOST_CHECK(lCheckCommandNoticeLoginAuthFailed.Dispatch(":tmi.twitch.tv NOTICE * :Login authentication failed\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(pMessageData.Channel.empty());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("Improperly formatted auth"));
            BOOST_CHECK(!pMessageData.NoticeType.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
        }
    } lCheckCommandNoticeImproperlyFormattedAuth;
    BOOST_CHECK(lCheckCommandNoticeImproperlyFormattedAuth.Dispatch(":tmi.twitch.tv NOTICE * :Improperly formatted auth\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("bar"));
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("The message from foo is now deleted."));
            BOOST_CHECK(!pMessageData.NoticeType.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
        }
    } lCheckCommandNoticeWithoutTags;
    BOOST_CHECK(lCheckCommandNoticeWithoutTags.Dispatch(":tmi.twitch.tv NOTICE #bar :The message from foo is now deleted.\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("bar"));
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("The message from foo is now deleted."));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("delete_message_success"));
            BOOST_CHECK(!pMessageData.UserId.has_value());
        }
    } lCheckCommandNoticeDeleteMessageSuccess;
    BOOST_CHECK(lCheckCommandNoticeDeleteMessageSuccess.Dispatch("@msg-id=delete_message_success :tmi.twitch.tv NOTICE #bar :The message from foo is now deleted.\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_NOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("bar"));
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("Your settings prevent you from sending this whisper."));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("whisper_restricted"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("12345678"));
        }
    } lCheckCommandNoticeWhisperRestricted;
    BOOST_CHECK(lCheckCommandNoticeWhisperRestricted.Dispatch("@msg-id=whisper_restricted;target-user-id=12345678 :tmi.twitch.tv NOTICE #bar :Your settings prevent you from sending this whisper.\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandPart)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_PART& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("twitchdev"));
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("bar"));
        }
    } lCheckCommandPart;
    BOOST_CHECK(lCheckCommandPart.Dispatch(":bar!bar@bar.tmi.twitch.tv PART #twitchdev\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandPing)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_PING& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Payload, std::string(":tmi.twitch.tv"));
        }
    } lCheckCommandPing;
    BOOST_CHECK(lCheckCommandPing.Dispatch("PING :tmi.twitch.tv\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandPrivMsg)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("ronni"));
            BOOST_CHECK(!pMessageData.UserName.has_value());
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(!pMessageData.UserIsModerator.has_value());
            BOOST_CHECK(!pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK(!pMessageData.UserIsVIP.has_value());
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("bleedPurple"));
            BOOST_CHECK(!pMessageData.MessageId.has_value());
            BOOST_CHECK(!pMessageData.Timestamp.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserId.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserLogin.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserName.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessage.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessageId.has_value());
            BOOST_CHECK(!pMessageData.Bits.has_value());
        }
    } lCheckCommandPrivMsgWithoutTags;
    BOOST_CHECK(lCheckCommandPrivMsgWithoutTags.Dispatch(":ronni!ronni@ronni.tmi.twitch.tv PRIVMSG #dallas :bleedPurple\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("ronni"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("1337"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("1337"));
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("ronni"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("global_mod"));
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), false);
            BOOST_CHECK(!pMessageData.UserIsVIP.has_value());
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x0D);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x42);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0x00);
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("Kappa Keepo Kappa"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("b34ccfc7-4977-403a-8a94-33c6bac34fb8"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1507246572675);
            BOOST_CHECK(!pMessageData.ReplyParentUserId.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserLogin.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserName.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessage.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessageId.has_value());
            BOOST_CHECK(!pMessageData.Bits.has_value());
        }
    } lCheckCommandPrivMsgWithTags;
    BOOST_CHECK(lCheckCommandPrivMsgWithTags.Dispatch("@badge-info=;badges=turbo/1;color=#0D4200;display-name=ronni;emotes=25:0-4,12-16/1902:6-10;id=b34ccfc7-4977-403a-8a94-33c6bac34fb8;mod=0;room-id=1337;subscriber=0;tmi-sent-ts=1507246572675;turbo=1;user-id=1337;user-type=global_mod :ronni!ronni@ronni.tmi.twitch.tv PRIVMSG #ronni :Kappa Keepo Kappa\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_PRIVMSG& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("ronni"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("12345678"));
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("ronni"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("staff"));
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), false);
            BOOST_CHECK(pMessageData.UserIsVIP.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsVIP.value(), true);
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("cheer100"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("b34ccfc7-4977-403a-8a94-33c6bac34fb8"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1507246572675);
            BOOST_CHECK(!pMessageData.ReplyParentUserId.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserLogin.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentUserName.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessage.has_value());
            BOOST_CHECK(!pMessageData.ReplyParentMessageId.has_value());
            BOOST_CHECK(pMessageData.Bits.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Bits.value(), 100);
        }
    } lCheckCommandPrivMsgWithCheerBits;
    BOOST_CHECK(lCheckCommandPrivMsgWithCheerBits.Dispatch("@badge-info=;badges=staff/1,bits/1000;bits=100;color=;display-name=ronni;emotes=;id=b34ccfc7-4977-403a-8a94-33c6bac34fb8;mod=0;room-id=12345678;subscriber=0;tmi-sent-ts=1507246572675;turbo=1;user-id=12345678;user-type=staff;vip=1 :ronni!ronni@ronni.tmi.twitch.tv PRIVMSG #ronni :cheer100\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandReconnect)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_RECONNECT& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckCommandReconnect;
    BOOST_CHECK(lCheckCommandReconnect.Dispatch(":tmi.twitch.tv RECONNECT\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandRoomState)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("bar"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.EmoteOnly.has_value());
            BOOST_CHECK(!pMessageData.FollowersOnly.has_value());
            BOOST_CHECK(!pMessageData.SubscribersOnly.has_value());
            BOOST_CHECK(!pMessageData.UniqueMessages.has_value());
            BOOST_CHECK(!pMessageData.MessageCooldown.has_value());
        }
    } lCheckCommandRoomStateWithoutTags;
    BOOST_CHECK(lCheckCommandRoomStateWithoutTags.Dispatch(":tmi.twitch.tv ROOMSTATE #bar\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.EmoteOnly.has_value());
            BOOST_CHECK_EQUAL(pMessageData.EmoteOnly.value(), false);
            BOOST_CHECK(pMessageData.FollowersOnly.has_value());
            BOOST_CHECK_EQUAL(pMessageData.FollowersOnly.value(), -1);
            BOOST_CHECK(pMessageData.SubscribersOnly.has_value());
            BOOST_CHECK_EQUAL(pMessageData.SubscribersOnly.value(), false);
            BOOST_CHECK(pMessageData.UniqueMessages.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UniqueMessages.value(), false);
            BOOST_CHECK(pMessageData.MessageCooldown.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageCooldown.value(), 0);
        }
    } lCheckCommandRoomStateUserJoinedChannel;
    BOOST_CHECK(lCheckCommandRoomStateUserJoinedChannel.Dispatch("@emote-only=0;followers-only=-1;r9k=0;room-id=12345678;slow=0;subs-only=0 :tmi.twitch.tv ROOMSTATE #dallas\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("713936733"));
            BOOST_CHECK(!pMessageData.EmoteOnly.has_value());
            BOOST_CHECK(!pMessageData.FollowersOnly.has_value());
            BOOST_CHECK(!pMessageData.SubscribersOnly.has_value());
            BOOST_CHECK(pMessageData.UniqueMessages.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UniqueMessages.value(), true);
            BOOST_CHECK(!pMessageData.MessageCooldown.has_value());
        }
    } lCheckCommandRoomStateEnableUniqueMessages;
    BOOST_CHECK(lCheckCommandRoomStateEnableUniqueMessages.Dispatch("@r9k=1;room-id=713936733 :tmi.twitch.tv ROOMSTATE #dallas\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_ROOMSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.EmoteOnly.has_value());
            BOOST_CHECK(!pMessageData.FollowersOnly.has_value());
            BOOST_CHECK(!pMessageData.SubscribersOnly.has_value());
            BOOST_CHECK(!pMessageData.UniqueMessages.has_value());
            BOOST_CHECK(pMessageData.MessageCooldown.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageCooldown.value(), 10);
        }
    } lCheckCommandRoomStateEnableSlowMode;
    BOOST_CHECK(lCheckCommandRoomStateEnableSlowMode.Dispatch("@slow=10 :tmi.twitch.tv ROOMSTATE #dallas\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandUserNotice)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.ChannelId.has_value());
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK(!pMessageData.UserLogin.has_value());
            BOOST_CHECK(!pMessageData.UserName.has_value());
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(!pMessageData.UserIsModerator.has_value());
            BOOST_CHECK(!pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK(pMessageData.Message.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message.value(), std::string("Great stream -- keep it up!"));
            BOOST_CHECK(!pMessageData.MessageId.has_value());
            BOOST_CHECK(!pMessageData.NoticeType.has_value());
            BOOST_CHECK(!pMessageData.NoticeMessage.has_value());
            BOOST_CHECK(!pMessageData.Timestamp.has_value());
            BOOST_CHECK(!pMessageData.MessageParamCumulativeMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoGiftTotal.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserId.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamShouldShareStreak.has_value());
            BOOST_CHECK(!pMessageData.MessageParamStreakMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlan.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlanName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamViewerCount.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRitualName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamThreshold.has_value());
            BOOST_CHECK(!pMessageData.MessageParamGiftMonths.has_value());
        }
    } lCommandUserNoticeWithoutTags;
    BOOST_CHECK(lCommandUserNoticeWithoutTags.Dispatch(":tmi.twitch.tv USERNOTICE #dallas :Great stream -- keep it up!\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("87654321"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("staff"));
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), true);
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x00);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x80);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0x00);
            BOOST_CHECK(pMessageData.Message.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message.value(), std::string("Great stream -- keep it up!"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("db25007f-7a18-43eb-9379-80131e44d633"));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("resub"));
            BOOST_CHECK(pMessageData.NoticeMessage.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeMessage.value(), std::string("ronni has subscribed for 6 months!"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1507246572675);
            BOOST_CHECK(pMessageData.MessageParamCumulativeMonths.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamCumulativeMonths.value(), 6);
            BOOST_CHECK(!pMessageData.MessageParamUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoGiftTotal.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserId.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserName.has_value());
            BOOST_CHECK(pMessageData.MessageParamShouldShareStreak.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamShouldShareStreak.value(), true);
            BOOST_CHECK(pMessageData.MessageParamStreakMonths.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamStreakMonths.value(), 2);
            BOOST_CHECK(pMessageData.MessageParamSubPlan.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamSubPlan.value(), std::string("Prime"));
            BOOST_CHECK(pMessageData.MessageParamSubPlanName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamSubPlanName.value(), std::string("Prime"));
            BOOST_CHECK(!pMessageData.MessageParamViewerCount.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRitualName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamThreshold.has_value());
            BOOST_CHECK(!pMessageData.MessageParamGiftMonths.has_value());
        }
    } lCommandUserNoticeResub;
    BOOST_CHECK(lCommandUserNoticeResub.Dispatch("@badge-info=;badges=staff/1,broadcaster/1,turbo/1;color=#008000;display-name=ronni;emotes=;id=db25007f-7a18-43eb-9379-80131e44d633;login=ronni;mod=0;msg-id=resub;msg-param-cumulative-months=6;msg-param-streak-months=2;msg-param-should-share-streak=1;msg-param-sub-plan=Prime;msg-param-sub-plan-name=Prime;room-id=12345678;subscriber=1;system-msg=ronni\\shas\\ssubscribed\\sfor\\s6\\smonths!;tmi-sent-ts=1507246572675;turbo=1;user-id=87654321;user-type=staff :tmi.twitch.tv USERNOTICE #dallas :Great stream -- keep it up!\r\n"));
    
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("forstycup"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("12345678"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("87654321"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("tww2"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("TWW2"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("staff"));
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), false);
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x00);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x00);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0xFF);
            BOOST_CHECK(!pMessageData.Message.has_value());
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("e9176cd8-5e22-4684-ad40-ce53c2561c5e"));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("subgift"));
            BOOST_CHECK(pMessageData.NoticeMessage.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeMessage.value(), std::string("TWW2 gifted a Tier 1 sub to Mr_Woodchuck!"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1521159445153);
            BOOST_CHECK(!pMessageData.MessageParamCumulativeMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserLogin.has_value());
            BOOST_CHECK(pMessageData.MessageParamMonths.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamMonths.value(), 1);
            BOOST_CHECK(!pMessageData.MessageParamPromoGiftTotal.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoName.has_value());
            BOOST_CHECK(pMessageData.MessageParamRecipientUserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamRecipientUserId.value(), std::string("55554444"));
            BOOST_CHECK(pMessageData.MessageParamRecipientUserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamRecipientUserLogin.value(), std::string("mr_woodchuck"));
            BOOST_CHECK(pMessageData.MessageParamRecipientUserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamRecipientUserName.value(), std::string("Mr_Woodchuck"));
            BOOST_CHECK(!pMessageData.MessageParamSenderUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamShouldShareStreak.has_value());
            BOOST_CHECK(!pMessageData.MessageParamStreakMonths.has_value());
            BOOST_CHECK(pMessageData.MessageParamSubPlan.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamSubPlan.value(), std::string("1000"));
            BOOST_CHECK(pMessageData.MessageParamSubPlanName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamSubPlanName.value(), std::string("House of Nyoro~n"));
            BOOST_CHECK(!pMessageData.MessageParamViewerCount.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRitualName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamThreshold.has_value());
            BOOST_CHECK(!pMessageData.MessageParamGiftMonths.has_value());
        }
    } lCommandUserNoticeSubGift;
    BOOST_CHECK(lCommandUserNoticeSubGift.Dispatch("@badge-info=;badges=staff/1,premium/1;color=#0000FF;display-name=TWW2;emotes=;id=e9176cd8-5e22-4684-ad40-ce53c2561c5e;login=tww2;mod=0;msg-id=subgift;msg-param-months=1;msg-param-recipient-display-name=Mr_Woodchuck;msg-param-recipient-id=55554444;msg-param-recipient-user-name=mr_woodchuck;msg-param-sub-plan-name=House\\sof\\sNyoro~n;msg-param-sub-plan=1000;room-id=12345678;subscriber=0;system-msg=TWW2\\sgifted\\sa\\sTier\\s1\\ssub\\sto\\sMr_Woodchuck!;tmi-sent-ts=1521159445153;turbo=0;user-id=87654321;user-type=staff :tmi.twitch.tv USERNOTICE #forstycup\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("othertestchannel"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("33332222"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("123456"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("testchannel"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("TestChannel"));
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), false);
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x9A);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0xCD);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0x32);
            BOOST_CHECK(!pMessageData.Message.has_value());
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("3d830f12-795c-447d-af3c-ea05e40fbddb"));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("raid"));
            BOOST_CHECK(pMessageData.NoticeMessage.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeMessage.value(), std::string("15 raiders from TestChannel have joined\n!"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1507246572675);
            BOOST_CHECK(!pMessageData.MessageParamCumulativeMonths.has_value());
            BOOST_CHECK(pMessageData.MessageParamUserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamUserName.value(), std::string("TestChannel"));
            BOOST_CHECK(pMessageData.MessageParamUserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamUserLogin.value(), std::string("testchannel"));
            BOOST_CHECK(!pMessageData.MessageParamMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoGiftTotal.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserId.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamShouldShareStreak.has_value());
            BOOST_CHECK(!pMessageData.MessageParamStreakMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlan.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlanName.has_value());
            BOOST_CHECK(pMessageData.MessageParamViewerCount.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamViewerCount.value(), 15);
            BOOST_CHECK(!pMessageData.MessageParamRitualName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamThreshold.has_value());
            BOOST_CHECK(!pMessageData.MessageParamGiftMonths.has_value());
        }
    } lCommandUserNoticeRaid;
    BOOST_CHECK(lCommandUserNoticeRaid.Dispatch("@badge-info=;badges=turbo/1;color=#9ACD32;display-name=TestChannel;emotes=;id=3d830f12-795c-447d-af3c-ea05e40fbddb;login=testchannel;mod=0;msg-id=raid;msg-param-displayName=TestChannel;msg-param-login=testchannel;msg-param-viewerCount=15;room-id=33332222;subscriber=0;system-msg=15\\sraiders\\sfrom\\sTestChannel\\shave\\sjoined\\n!;tmi-sent-ts=1507246572675;turbo=1;user-id=123456;user-type= :tmi.twitch.tv USERNOTICE #othertestchannel\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERNOTICE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("seventoes"));
            BOOST_CHECK(pMessageData.ChannelId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ChannelId.value(), std::string("87654321"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("77776666"));
            BOOST_CHECK(pMessageData.UserLogin.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin.value(), std::string("seventest1"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("SevenTest1"));
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), false);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), false);
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK(pMessageData.Message.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message.value(), std::string("HeyGuys"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("37feed0f-b9c7-4c3a-b475-21c6c6d21c3d"));
            BOOST_CHECK(pMessageData.NoticeType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeType.value(), std::string("ritual"));
            BOOST_CHECK(pMessageData.NoticeMessage.has_value());
            BOOST_CHECK_EQUAL(pMessageData.NoticeMessage.value(), std::string("Seventoes is new here!"));
            BOOST_CHECK(pMessageData.Timestamp.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Timestamp.value(), 1508363903826);
            BOOST_CHECK(!pMessageData.MessageParamCumulativeMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoGiftTotal.has_value());
            BOOST_CHECK(!pMessageData.MessageParamPromoName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserId.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamRecipientUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserLogin.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSenderUserName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamShouldShareStreak.has_value());
            BOOST_CHECK(!pMessageData.MessageParamStreakMonths.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlan.has_value());
            BOOST_CHECK(!pMessageData.MessageParamSubPlanName.has_value());
            BOOST_CHECK(!pMessageData.MessageParamViewerCount.has_value());
            BOOST_CHECK(pMessageData.MessageParamRitualName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageParamRitualName.value(), std::string("new_chatter"));
            BOOST_CHECK(!pMessageData.MessageParamThreshold.has_value());
            BOOST_CHECK(!pMessageData.MessageParamGiftMonths.has_value());
        }
    } lCommandUserNoticeNewChatter;
    BOOST_CHECK(lCommandUserNoticeNewChatter.Dispatch("@badge-info=;badges=;color=;display-name=SevenTest1;emotes=30259:0-6;id=37feed0f-b9c7-4c3a-b475-21c6c6d21c3d;login=seventest1;mod=0;msg-id=ritual;msg-param-ritual-name=new_chatter;room-id=87654321;subscriber=0;system-msg=Seventoes\\sis\\snew\\shere!;tmi-sent-ts=1508363903826;turbo=0;user-id=77776666;user-type= :tmi.twitch.tv USERNOTICE #seventoes :HeyGuys\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandUserState)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(!pMessageData.UserName.has_value());
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(!pMessageData.UserIsModerator.has_value());
            BOOST_CHECK(!pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK(!pMessageData.MessageId.has_value());
        }
    } lCheckCommandUserStateWithoutTags;
    BOOST_CHECK(lCheckCommandUserStateWithoutTags.Dispatch(":tmi.twitch.tv USERSTATE #dallas\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_USERSTATE& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK_EQUAL(pMessageData.Channel, std::string("dallas"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("ronni"));
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x0D);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x42);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0x00);
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("staff"));
            BOOST_CHECK(pMessageData.UserIsModerator.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsModerator.value(), true);
            BOOST_CHECK(pMessageData.UserIsSubscriber.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserIsSubscriber.value(), true);
            BOOST_CHECK(!pMessageData.MessageId.has_value());
        }
    } lCheckCommandUserStateWithTags;
    BOOST_CHECK(lCheckCommandUserStateWithTags.Dispatch("@badge-info=;badges=staff/1;color=#0D4200;display-name=ronni;emote-sets=0,33,50,237,793,2126,3517,4578,5569,9400,10337,12239;mod=1;subscriber=1;turbo=1;user-type=staff :tmi.twitch.tv USERSTATE #dallas\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchCommandWhisper)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(!pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("foo"));
            BOOST_CHECK(!pMessageData.UserName.has_value());
            BOOST_CHECK(!pMessageData.UserType.has_value());
            BOOST_CHECK(!pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("hello"));
            BOOST_CHECK(!pMessageData.MessageId.has_value());
            BOOST_CHECK(!pMessageData.ThreadId.has_value());
        }
    } lCheckCommandWhisperWithoutTags;
    BOOST_CHECK(lCheckCommandWhisperWithoutTags.Dispatch(":petsgomoo!petsgomoo@petsgomoo.tmi.twitch.tv WHISPER foo :hello\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::COMMAND_WHISPER& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
            BOOST_CHECK(pMessageData.UserId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserId.value(), std::string("87654321"));
            BOOST_CHECK_EQUAL(pMessageData.UserLogin, std::string("foo"));
            BOOST_CHECK(pMessageData.UserName.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserName.value(), std::string("FOO"));
            BOOST_CHECK(pMessageData.UserType.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserType.value(), std::string("staff"));
            BOOST_CHECK(pMessageData.UserColor.has_value());
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().R, 0x8A);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().G, 0x2B);
            BOOST_CHECK_EQUAL(pMessageData.UserColor.value().B, 0xE2);
            BOOST_CHECK_EQUAL(pMessageData.Message, std::string("hello"));
            BOOST_CHECK(pMessageData.MessageId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.MessageId.value(), std::string("306"));
            BOOST_CHECK(pMessageData.ThreadId.has_value());
            BOOST_CHECK_EQUAL(pMessageData.ThreadId.value(), std::string("12345678_87654321"));
        }
    } lCheckCommandWhisperWithTags;
    BOOST_CHECK(lCheckCommandWhisperWithTags.Dispatch("@badges=staff/1,bits-charity/1;color=#8A2BE2;display-name=FOO;emotes=;message-id=306;thread-id=12345678_87654321;turbo=0;user-id=87654321;user-type=staff :petsgomoo!petsgomoo@petsgomoo.tmi.twitch.tv WHISPER foo :hello\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplWelcome)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_WELCOME& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyWelcome;
    BOOST_CHECK(lCheckNumericReplyWelcome.Dispatch(":tmi.twitch.tv 001 ronni :Welcome, GLHF!\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplYourHost)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_YOURHOST& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyYourHost;
    BOOST_CHECK(lCheckNumericReplyYourHost.Dispatch(":tmi.twitch.tv 002 ronni :Your host is tmi.twitch.tv\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplCreated)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_CREATED& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyCreated;
    BOOST_CHECK(lCheckNumericReplyCreated.Dispatch(":tmi.twitch.tv 003 ronni :This server is rather new\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplMyInfo)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_MYINFO& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyMyInfo;
    BOOST_CHECK(lCheckNumericReplyMyInfo.Dispatch(":tmi.twitch.tv 004 ronni :-\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplMotdStart)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTDSTART& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyMessageOfTheDayStart;
    BOOST_CHECK(lCheckNumericReplyMessageOfTheDayStart.Dispatch(":tmi.twitch.tv 375 ronni :-\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplMotd)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_MOTD& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyMessageOfTheDay;
    BOOST_CHECK(lCheckNumericReplyMessageOfTheDay.Dispatch(":tmi.twitch.tv 372 ronni :You are in a maze of twisty passages.\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplEndOfMotd)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFMOTD& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericReplyEndOfMessageOfTheDay;
    BOOST_CHECK(lCheckNumericReplyEndOfMessageOfTheDay.Dispatch(":tmi.twitch.tv 376 ronni :>\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplNamReply, * boost::unit_test::expected_failures(2))
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv")); // TODO: Fix expected failure
        }
    } lCheckNumericReplyNameReply;
    BOOST_CHECK(lCheckNumericReplyNameReply.Dispatch(":ronni.tmi.twitch.tv 353 ronni = #dallas :ronni\r\n"));

    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_NAMREPLY& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv")); // TODO: Fix expected failure
        }
    } lCheckNumericReplyNameReplyWithOtherUsers;
    BOOST_CHECK(lCheckNumericReplyNameReplyWithOtherUsers.Dispatch(":ronni.tmi.twitch.tv 353 ronni = #dallas :xhipgamer blushyface mauerbac_bot moobot\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericRplEndOfNames, * boost::unit_test::expected_failures(1))
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_RPL_ENDOFNAMES& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv")); // TODO: Fix expected failure
        }
    } lCheckNumericReplyEndOfNames;
    BOOST_CHECK(lCheckNumericReplyEndOfNames.Dispatch(":ronni.tmi.twitch.tv 366 ronni #dallas :End of /NAMES list\r\n"));
}

BOOST_AUTO_TEST_CASE(ParserTestDispatchNumericErrUnknownCommand)
{
    class : public ParserTestCaseCheck
    {
    protected:
        void OnParseMessage(
            const Network::Twitch::IRC::Parser::NUMERIC_ERR_UNKNOWNCOMMAND& pMessageData) override
        {
            BOOST_CHECK_EQUAL(pMessageData.Host, std::string("tmi.twitch.tv"));
        }
    } lCheckNumericErrorUnknownComand;
    BOOST_CHECK(lCheckNumericErrorUnknownComand.Dispatch(":tmi.twitch.tv 421 ronni WHO :Unknown command\r\n"));
}
